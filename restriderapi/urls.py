from rest_framework_simplejwt.views import TokenRefreshView
from django.urls import path
from .views import *

app_name = "restriderapi"

urlpatterns = [
    path("login/", RiderGetTokenPairView.as_view(),
         name="login"),
    path('refresh-token/', TokenRefreshView.as_view(),
         name='riderrefreshtoken'),
    path('profile/', RiderProfileAPIView.as_view(), name="riderprofile"),
    path('delivery-tasks/', RiderDeliveryTaskListAPIView.as_view()),
    path("all-riders", RiderListView.as_view(), name="riderlist")

]
