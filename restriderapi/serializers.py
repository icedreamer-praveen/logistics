from rest_framework import serializers
from accapp.models import DeliveryPerson
from lmsapp.models import DeliveryTask


class RiderLoginSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField()


class RiderProfileSerializer(serializers.ModelSerializer):
    email = serializers.SerializerMethodField()
    address_city = serializers.StringRelatedField()
    logistics_branch = serializers.StringRelatedField()
    image = serializers.SerializerMethodField()

    class Meta:
        model = DeliveryPerson
        fields = ["logistics_branch", "email", "full_name", "address_city", "mobile", "image"]

    def get_email(self, obj):
        return obj.user.username

    def get_image(self, obj):
        if obj.image:
            return obj.image.url
        else:
            return 'No Image'


class DeliveryTaskListSerializer(serializers.ModelSerializer):
    tracking_code = serializers.SerializerMethodField()
    sender = serializers.SerializerMethodField()
    sender_contact = serializers.SerializerMethodField()
    receiver_name = serializers.SerializerMethodField()
    receiver_contact = serializers.SerializerMethodField()
    receiver_city = serializers.SerializerMethodField()
    receiver_address = serializers.SerializerMethodField()
    cod_value = serializers.SerializerMethodField()

    class Meta:
        model = DeliveryTask
        fields = ['id', 'tracking_code', 'sender', 'sender_contact', 'receiver_name', 'receiver_contact', 'receiver_city', 'receiver_address', 'cod_value', 'task_type', 'task_status', 'task_details']
        
    def get_tracking_code(self, obj):
        return obj.shipment.tracking_code

    def get_sender(self, obj):
        return obj.shipment.customer.company_name

    def get_sender_contact(self, obj):
        return obj.shipment.customer.contact_number
    
    def get_receiver_name(self, obj):
        return obj.shipment.receiver_name
    
    def get_receiver_contact(self, obj):
        return obj.shipment.receiver_contact_number
    
    def get_receiver_city(self, obj):
        return obj.shipment.receiver_city.name
    
    def get_receiver_address(self, obj):
        return obj.shipment.receiver_address
    
    def get_cod_value(self, obj):
        return obj.shipment.cod_value
        
        
class DeliveryPersonSerializer(serializers.ModelSerializer):
    class Meta:
        model = DeliveryPerson
        fields = ['id', 'full_name', 'image']