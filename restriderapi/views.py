from django.contrib.auth import authenticate, login, logout
from django.db.models import query
from rest_framework.pagination import PageNumberPagination
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from lmsapp.models import DeliveryTask
from .serializers import *
from .utils import *


class RiderGetTokenPairView(APIView):
    def post(self, request):
        serializer = RiderLoginSerializer(data=request.data)
        serializer.is_valid()
        email = serializer.validated_data.get("email")
        password = serializer.validated_data.get("password")
        user = authenticate(username=email, password=password)
        try:
            rider = user.deliveryperson
            if rider.status == "Active" and rider.logistics_branch.status == "Active":
                data = get_tokens_for_user(user)
                resp = {
                    "status": "success",
                    "data": data
                }
            else:
                resp = {
                    "status": "error",
                    "message": "Pending or Suspended account.."
                }
        except Exception as e:
            print(e)
            resp = {
                "status": "error",
                
                "message": "Invalid credentials of the Rider.."
            }
        print(resp)
        return Response(resp)


class RiderProfileAPIView(APIView):
    permission_classes = [RiderOnlyPermission]

    def get(self, request):
        serializer = RiderProfileSerializer(request.user.deliveryperson)
        resp = {
            "status": "success",
            "data": serializer.data
        }
        return Response(resp)


class GeneralPaginationClass(PageNumberPagination):
    page_size = 50
    page_size_query_param = 'page_size'
    max_page_size = 1000


class RiderDeliveryTaskListAPIView(ListAPIView):
    queryset = DeliveryTask.objects.all()
    serializer_class = DeliveryTaskListSerializer
    pagination_class = GeneralPaginationClass
    permission_classes = [RiderOnlyPermission]

    def get_queryset(self):
        queryset = super().get_queryset().order_by("-id")
        type = self.request.GET.get("type")
        status = self.request.GET.get("status")
        delivery_person = self.request.user.deliveryperson
        queryset = queryset.filter(delivery_person=delivery_person)
        if type == "dropoff":
            queryset = queryset.filter(task_type="Drop Off")
        elif type == "pickup":
            queryset = queryset.filter(task_type="Pick Up")
        elif type == "return":
            queryset = queryset.filter(task_type="Return")
            
        if status == "assigned":
            status_list = ["Assigned", "Started"]
        elif status == "attempted":
            status_list = ["Doing"]
        elif status == "completed":
            status_list = ["Completed Success", "Completed Fail"]
        else:
            status_list = ["Assigned", "Started"]
        queryset = queryset.filter(task_status__in=status_list)
        return queryset
    
    
class RiderListView(ListAPIView):
    queryset = DeliveryPerson.objects.all()
    serializer_class = DeliveryPersonSerializer
    pagination_class = GeneralPaginationClass
    
    def list(self, request):
        queryset = self.get_queryset()
        serializer = DeliveryPersonSerializer(queryset, many=True)
        return Response(serializer.data)