from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework.response import Response
from django.contrib.auth import authenticate
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from django.contrib.contenttypes.models import ContentType
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.utils.encoding import smart_str, force_str, smart_bytes, DjangoUnicodeDecodeError
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.contrib.sites.shortcuts import get_current_site
from rest_framework.pagination import PageNumberPagination
from collections import OrderedDict
from django.urls import reverse
from django.db.models import Q, F, Max, Avg, Count, Sum
from django.utils import timezone
from .serializers import *
from .utils import *
from lmsapp.models import DeliveryTask


class OperatorGetTokenPairView(APIView):
    def post(self, request):
        serializer = OperatorLoginSerializer(data=request.data)
        serializer.is_valid()
        email = serializer.validated_data.get("email")
        password = serializer.validated_data.get("password")
        user = authenticate(username=email, password=password)
        try:
            operator = user.branchoperator
            if operator.status == "Active" and operator.logistics_branch.status == "Active":
                resp = get_tokens_for_user(user)
            else:
                resp = {
                    "message": "Pending or Suspended account.."
                }
        except Exception as e:
            print(e)
            resp = {
                "message": "Invalid credentials of the operator.."
            }
        return Response(resp)


class OperatorProfileAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def get(self, request):
        serializer = OperatorProfileSerializer(request.user.branchoperator)
        resp = {
            "status": "success",
            "data": serializer.data
        }
        return Response(resp)


class OperatorProfileUpdateAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def patch(self, request):
        op_id = self.request.user.branchoperator.id
        op = BranchOperator.objects.get(id=op_id)
        serializer = OperatorProfileUpdateSerializer(op, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            res = {
                'status' : 'success',
                'data' : serializer.data
            }
        else:
            res = {
                'message' : serializer.errors
            }
        return Response(res)


class CompanyRegistrationApiView(APIView):
    permission_classes = [AllowAny]

    def post(self, request):
        serializer = CompanyRegistrationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            res = {
                'status' : 'success',
                'data' : serializer.data
            }
        else:
            res = {
                'message' : serializer.errors
            }
        return Response(res)


class OperatorPasswordChangeAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def patch(self, request):
        self.user = request.user
        seriliazer = ChangeOperatorPasswordSerializer(data=request.data)
        if seriliazer.is_valid():
            if not self.user.check_password(seriliazer.data.get('old_password')):
                res = {
                    'message' : 'wrong password'
                }
                return Response(res)
            self.user.set_password(seriliazer.data.get('new_password'))
            self.user.save()
            res = {
                'status' : 'success'
            }
            return Response(res)
        else:
            res={
                'message' : seriliazer.errors
            }
            return Response(res)


class OperatorRequestPasswordResetEmail(APIView):
    permission_classes = [AllowAny]

    def post(self, request):
        serializer = OperatorResetPasswordEmailRequestSerializer(data=request.data)
        if User.objects.filter(email=request.data.get('email')).exists():
            user = User.objects.get(email=request.data.get('email'))
            uidb64 = urlsafe_base64_encode(smart_bytes(user.id))
            token = PasswordResetTokenGenerator().make_token(user)
            current_site = get_current_site(
                request=request).domain
            relativeLink = reverse(
                'lcapiapp:token-check', kwargs={'uidb64': uidb64, 'token': token})
            absurl = 'http://'+current_site + relativeLink
            email_body = 'Hello, \n Use link below to reset your password  \n' + \
                absurl
            data = {'email_body': email_body, 'to_email': user.email,
                    'email_subject': 'Reset your passsword'}
            Util.send_email(data)
            resp = {
                "status" : "success",
                "message" : 'We have sent you a link to reset your password'
            }
            return Response(resp)
        
        #Fake response for fake user
        resp = {
                "status" : "success",
                "message" : 'We have sent you a link to reset your password'
            }
        return Response(resp)


class OperatorPasswordTokenCheckAPI(APIView):
    def get(self, request, uidb64, token):
        try:
            id = smart_str(urlsafe_base64_decode(uidb64))
            user = User.objects.get(id=id)

            if not PasswordResetTokenGenerator().check_token(user, token):
                resp = {
                    "message" : "Token in not valid please request new one"
                }
                return Response(resp)
            resp = {
                "message" : "Token is Valid"
            }
            return Response(resp)

        except DjangoUnicodeDecodeError as identifier:
            if not PasswordResetTokenGenerator().check_token(user):
                resp = {
                    "message" : "Invalid Token"
                }
                return Response(resp)


class OperatorSetNewPasswordAPIView(APIView):
    def patch(self, request):
        serializer = OperatorSetNewPasswordSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response({'status': "success", 'message': 'Password reset success'})


class CustomerInfoListAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def get(self, request):
        branch = request.user.branchoperator.logistics_branch
        customer_list = LogisticsCustomer.objects.filter(connected_logistics=branch)
        serializer = CustomerListSerializer(customer_list, many=True)
        res = {
            'data' : serializer.data
        }
        return Response(res)


class CustomerInforDetailAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def get(self, request, pk=None):
        try:
            branch = request.user.branchoperator.logistics_branch
            customer = LogisticsCustomer.objects.get(id=pk, connected_logistics=branch)
            serializer = CustomerDetailSerializer(customer)
            res = {
                'data' : serializer.data
            }
        except Exception as e:
            res = {
                'message' : 'Not found'
            }
        return Response(res)


class CustomerCreateFromOperatorAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def post(self, request):
        branch = request.user.branchoperator.logistics_branch
        serializer = CustomerCreateSerializer(data=request.data)
        if serializer.is_valid():
            full_name = serializer.data['full_name']
            company_name = serializer.data['company_name']
            contact_number = serializer.data['contact_number']
            city_id = serializer.data['city']
            city = City.objects.get(id=city_id)
            address = serializer.data['address']
            email = serializer.data['email']
            password = serializer.data['password']
            user = User.objects.create_user(username=email, email=email, password=password)
            if user is not None:
                cust = LogisticsCustomer.objects.create(user=user, full_name=full_name, company_name=company_name, contact_number=contact_number, \
                city=city, address=address, status='Active', creator_branch=branch)
                cust.connected_logistics.add(branch)
                res = {
                    'status' : 'success',
                    'data' : serializer.data
                }
            else:
                res = {
                    'message' : 'user is none'
                }
        else:
            res = {
                'message' : serializer.errors
            }
        return Response(res)


class CustomerUpdateFromOperatorAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def patch(self, request, pk=None):
        try:
            branch = request.user.branchoperator.logistics_branch
            customer = LogisticsCustomer.objects.get(id=pk)
            serializer = CustomerUpdateSerializer(customer, data=request.data, partial=True)
            if serializer.is_valid():
                if customer.creator_branch == branch:
                    serializer.save()
                    res = {
                        'status' : 'success',
                        'data' : serializer.data
                    }
                else:
                    res = {
                        'message' : 'unauthorized'
                    }
            else:
                res = {
                    'message' : serializer.errors
                }
            return Response(res)
        except Exception as e:
            print(e)
            res = {
                'message' : 'Not Found'
            }
            return Response(res)



class CustomerPasswordChangeByCreatorBranchAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def patch(self, request, pk=None):
        try:
            branch = request.user.branchoperator.logistics_branch
            customer = LogisticsCustomer.objects.get(id=pk)
            user = customer.user
            serializer = CustomerPasswordChangeByCreatorBranchSerializer(data=request.data)
            if serializer.is_valid():
                if customer.creator_branch == branch:
                    user.set_password(serializer.data['new_password'])
                    user.save()
                    res = {
                        'status' : 'success',
                        'data' : serializer.data
                    }
                else:
                    res = {
                        'message' : 'unauthorized'
                    }
            else:
                res = {
                    'message' : serializer.errors,
                }
            return Response(res)
        except Exception as e:
            print(e)
            res = {
                'message' : 'Not Found'
            }
            return Response(res)


class OperatorStaffListAPIView(APIView, PageNumberPagination):
    permission_class = [OperatorOnlyPermission]

    def get_paginated_response(self, data, page, page_num):
        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('current', page),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('page_size', page_num),
            ('result', data),
        ]))
    
    def get_queryset(self,request):
        branch = request.user.branchoperator.logistics_branch
        keyword = self.request.GET.get('keyword')
        if keyword is not None:
            operator_obj = BranchOperator.objects.filter(Q(full_name__icontains=keyword) | Q(address_city__name__icontains=keyword)).filter(logistics_branch=branch, operator_type='SuperAdmin')
        else:
            operator_obj = BranchOperator.objects.filter(logistics_branch=branch, operator_type='SuperAdmin')
        return self.paginate_queryset(operator_obj, self.request)

    def get(self, request):
        page = self.request.GET.get('page', 1)
        page_size = self.request.GET.get('page_size', 1000)
        operator_list = self.get_queryset(request)
        serializer = OperatorStaffListSerializer(operator_list, many=True)
        return self.get_paginated_response(serializer.data, page, page_size)


class OperatorStaffDetailAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def get(self, request,pk=None):
        try:
            branch = request.user.branchoperator.logistics_branch
            operator = BranchOperator.objects.get(id=pk, logistics_branch=branch)
            serializer = OperatorStaffDetailSerializer(operator)
            res = {
                'status' : 'success',
                'data' : serializer.data
            }
            return Response(res)
        except Exception as e:
            print(e)
            res = {
                'message' : 'not found'
            }
            return Response(res)


class OperatorCreateStaffAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def post(self, request):
        branch = request.user.branchoperator.logistics_branch
        serializer = OperatorCreateStaffSerializer(data=request.data)
        if serializer.is_valid():
            full_name = serializer.data['full_name']
            image = serializer.data['image']
            mobile = serializer.data['mobile']
            alt_mobile = serializer.data['alt_mobile']
            document1 = serializer.data['document1']
            document2 = serializer.data['document2']
            document3 = serializer.data['document3']
            address_city = serializer.data['address_city']
            email = serializer.data['email']
            password = serializer.data['password']
            operator_type = serializer.data['operator_type']
            city = City.objects.get(id=address_city)
            user = User.objects.create_user(username=email, email=email, password=password)
            if user is not None:
                BranchOperator.objects.create(
                status = 'Active', user = user, full_name=full_name, image=image, mobile=mobile, alt_mobile=alt_mobile, address_city=city, \
                document1=document1, document2=document2, document3=document3, operator_type=operator_type, logistics_branch=branch
            )
                res = {
                    'status' : 'success',
                    'data' : serializer.data
                }
            else:
                res = {
                    'message' : 'user is none'
                }
        else:
            res = {
                'message' : serializer.errors
            }
        return Response(res)


class OperatorPasswordChangeBySuperAdminAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def patch(self, request, pk=None):
        try:
            operator = request.user.branchoperator
            sub_operator = BranchOperator.objects.get(id=pk)
            if operator.operator_type == 'SuperAdmin' and operator.logistics_branch == sub_operator.logistics_branch:
                serializer = OperatorpasswordChangeBySuperAdminSerializer(data=request.data, partial=True)
                if serializer.is_valid():
                    user = sub_operator.user
                    user.set_password(serializer.data['new_password'])
                    user.save()
                    res = {
                        'status' : 'success',
                        'data' : serializer.data
                    }
                else:
                    res = {
                        'message' : serializer.errors,
                    }
            else:
                res = {
                    'message' : 'Unauthorized'
                }
            return Response(res)
        except Exception as e:
            print(e)
            res = {
                'message' : 'Not Found'
            }
            return Response(res)


class BranchOperatorUpdateBySuperAdminAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def patch(self, request, pk=None):
        try:
            operator = request.user.branchoperator
            sub_operator = BranchOperator.objects.get(id=pk)
            if operator.operator_type == "SuperAdmin" and operator.logistics_branch == sub_operator.logistics_branch:
                serializer = OperatorUpdateBySuperAdminSerializer(sub_operator, data=request.data, partial=True)
                if serializer.is_valid():
                    serializer.save()
                    res = {
                        'status' : 'Success',
                        'data' : serializer.data
                    }
                else:
                    res = {
                        'message' : serializer.errors
                    }
            else:
                res = {
                    'message' : 'unauthorized'
                }
        except Exception as e:
            print(e)
            res = {
                'message' : 'not found'
            }
        return Response(res)


class DeliveryTypeCreateAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]
    
    def post(self, request):
        branch = request.user.branchoperator.logistics_branch
        delivery_type_list = DeliveryType.objects.filter(logistics_branch=branch)
        serializer = DeliveryTypeCreateSerializer(data=request.data)
        if serializer.is_valid():
            delivery_type = serializer.data['delivery_type']
            image = serializer.data['image']
            dt = DeliveryType.objects.create(status='Active',logistics_branch=branch, delivery_type=delivery_type, image = image)
            shiping = LogisticsBranchShippingZone.objects.filter(logistics_branch=branch)
            for ship in shiping:
                ShippingZoneDeliveryType.objects.create(status='Active', shipping_zone=ship, delivery_type=dt, shipping_charge_per_kg=0, \
                    additional_shipping_charge_per_kg=0, cod_handling_charge=0, rejection_handling_charge=0)
            res = {
                'status' : 'success',
                'res' : serializer.data
            }
        else:
            res = {
                'message' : serializer.errors
            }
        return Response(res)


class DeliveryTypeUpdateAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def patch(self, request, pk=None):
        try:
            branch = request.user.branchoperator.logistics_branch
            delivery_type = DeliveryType.objects.get(id=pk, logistics_branch=branch)
            serializer = DeliveryTypeCreateSerializer(delivery_type, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.save()
                res = {
                    'status' : 'success',
                    'data' : serializer.data
                }
            else:
                res = {
                    'message' : serializer.errors
                }
        except Exception as e:
            print(e)
            res = {
                'message' : 'not found'
            }
        return Response(res)

class ShippingZoneDeliveryTypeUpdateAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def patch(self, request, pk):
        try:
            branch = self.request.user.branchoperator.logistics_branch
            shippingzone_delivery = ShippingZoneDeliveryType.objects.get(id=pk)
            serializer = ShippingZoneDeliveryTypeUpdateSerializer(shippingzone_delivery, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.save()
                res = {
                    'status' : 'success',
                    'data' : serializer.data
                }
            else:
                res = {
                    'message' : serializer.errors
                }
        except Exception as e:
            print(e)
            res = {
                'message' : 'not found'
            }
        return Response(res)



class branchshippingzonecreateAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def post(self, request):
        branch = self.request.user.branchoperator.logistics_branch
        serializer = branchshippingzonecreateSerializer(data=request.data)
        if serializer.is_valid():
            cities = request.data['cities']
            shiping = LogisticsBranchShippingZone.objects.filter(logistics_branch=branch, cities__in=cities)
            if shiping:
                res = {
                    'mesage' : 'city already exists'
                }
            else:
                shipping_zone = serializer.save(logistics_branch=branch, status='Active')
                delivery_types = DeliveryType.objects.filter(logistics_branch=branch)
                for dt in delivery_types:
                    ShippingZoneDeliveryType.objects.create(shipping_zone=shipping_zone, delivery_type=dt, shipping_charge_per_kg=0, additional_shipping_charge_per_kg=0, \
                    cod_handling_charge=0, rejection_handling_charge=0, status = 'Active')
                res = {
                    'status' : 'success',
                    'data' : serializer.data
                }
        else:
            res = {
                'message' : serializer.errors
            }
        return Response(res)


class ShippingZoneUpdateAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def patch(self, request, pk):
        try:
            branch = self.request.user.branchoperator.logistics_branch
            shiping_zone = LogisticsBranchShippingZone.objects.get(id=pk, logistics_branch=branch)
            serializer = ShippingZoneUpdateSerializer(shiping_zone, data=request.data, partial=True)
            if serializer.is_valid():
                cities = request.data['cities']
                shiping = LogisticsBranchShippingZone.objects.filter(logistics_branch=branch, cities__in=cities)
                if shiping:
                    res = {
                        'mesage' : 'city already exists'
                    }
                else:
                    serializer.save()
                    res = {
                        'status' : 'success',
                        'data' : serializer.data
                    }
            else:
                res = {
                    'message' : serializer.errors
                }
        except Exception as e:
            print(e)
            res = {
                'message' : 'not found'
            }
        return Response(res)


class ShippingZoneListAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def get(self, request):
        branch = request.user.branchoperator.logistics_branch
        shipping_zone_list = LogisticsBranchShippingZone.objects.filter(logistics_branch=branch)
        serializer = ShippingZoneListSerializer(shipping_zone_list, many=True)
        res = {
            'status' : 'success',
            'data' : serializer.data
        }
        return Response(res)


class ShippingZoneDetailAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def get(self, request,pk=None):
        try:
            branch = request.user.branchoperator.logistics_branch
            shipping_zone = LogisticsBranchShippingZone.objects.get(id=pk,logistics_branch=branch)
            serializer = ShippingZoneDetailSerializer(shipping_zone)
            res = {
                'status' : 'success',
                'data' : serializer.data
            }
        except Exception as e:
            print(e)
            res = {
                'message' : 'not found'
            }
        return Response(res)


class CustomerSupportTicketListAPIView(APIView, PageNumberPagination):
    permission_classes = [OperatorOnlyPermission]

    def get_paginated_response(self, data, page, page_num):
        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('current', page),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('page_size', page_num),
            ('result', data),
        ]))
    
    def get_queryset(self,request):
        branch = request.user.branchoperator.logistics_branch
        ticket_status = self.request.GET.get('ticket_status')
        keyword = self.request.GET.get('keyword', '')
        new_queryset = CustomerSupportTicket.objects.filter(raised_to=branch) if ticket_status == 'all' or ticket_status == "" or ticket_status is None\
            else CustomerSupportTicket.objects.filter(raised_to=branch, ticket_status=ticket_status)
        new_queryset = new_queryset.filter(Q(raised_by__full_name__icontains=keyword) | Q(
            issue_detail__icontains=keyword)) if keyword is not None and keyword != "" else new_queryset
        return self.paginate_queryset(new_queryset, self.request)

    def get(self, request):
        page = self.request.GET.get('page', 1)
        page_size = self.request.GET.get('page_size', 1000)
        ticket_list = self.get_queryset(request)
        serializer = CustomerSupportTicketListSerializer(ticket_list, many=True)
        return self.get_paginated_response(serializer.data, page, page_size)


class CustomerSupportTicketDetailAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def get(self, request, pk=None):
        try:
            branch = request.user.branchoperator.logistics_branch
            ticket = CustomerSupportTicket.objects.get(id=pk, raised_to=branch)
            serializer = CustomerSupportTicketDetailSerializer(ticket)
            print(serializer.data)
            res = {
                'status' : 'success',
                'data' : serializer.data
            }
        except Exception as e:
            print(e)
            res = {
                'message' : 'not found'
            }
        return Response(res)


class CustomerTicketFollowupAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def post(self, request, pk=None):
        try:
            branch = request.user.branchoperator.logistics_branch
            ticket = CustomerSupportTicket.objects.get(id=pk)
            serializer = CustomerTicketFollowupCreateSerializer(data=request.data)
            if serializer.is_valid():
                content_type = ContentType.objects.get(
                        app_label='umapp', model='logisticscustomer'
                    )
                branch_id = branch.id
                message = serializer.data['followup_message']
                CustomerTicketFollowup.objects.create(ticket=ticket, followup_by_type=content_type, \
                    followup_by_id=branch_id, followup_message=message, status='Pending')
                res = {
                    "status" : "success",
                    "data" : serializer.data 
                }
            else:
                res = {
                    'message' : serializer.errors
                }
        except Exception as e:
            print(e)
            res = {
                'message' : 'not found'
            }
        return Response(res)


class LogisticsNoticeListAPIView(APIView, PageNumberPagination):
    permission_classes = [OperatorOnlyPermission]

    def get_paginated_response(self, data, page, page_num):
        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('current', page),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('page_size', page_num),
            ('result', data),
        ]))
    
    def get_queryset(self,request):
        branch = request.user.branchoperator.logistics_branch
        new_queryset = LogisticsNotice.objects.filter(logistics_branch=branch).order_by('-created_at')
        return self.paginate_queryset(new_queryset, self.request)

    def get(self, request):
        page = self.request.GET.get('page', 1)
        page_size = self.request.GET.get('page_size', 1000)
        notice_list = self.get_queryset(request)
        serializer = LogisticsOperatorNoticeListSerializer(notice_list, many=True)
        return self.get_paginated_response(serializer.data, page, page_size)


class LogisticsNoticeDetailAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def get(self, request, pk=None):
        try:
            branch = request.user.branchoperator.logistics_branch
            operator = request.user.branchoperator
            notice = LogisticsNotice.objects.get(id=pk, logistics_branch=branch)
            serializer = LogisticsOperatorNoticeDetailSerializer(notice)
            res = {
                'status' : 'success',
                'data' : serializer.data
            }
        except Exception as e:
            print(e)
            res = {
                'message' : 'not found'
            }
        return Response(res)


class LogisticsNoticeCreateAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def post(self, request):
        company = request.user.branchoperator.logistics_branch.logistics_company
        branch = request.user.branchoperator.logistics_branch
        operator = request.user.branchoperator
        serializer = LogisticsOperatorNoticeCreateSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(status="Active", logistics_company=company, logistics_branch=branch, created_by=operator)
            res = {
                'status' : 'success',
                'data' : serializer.data
            }
        else:
            res = {
                'message' : serializer.errors
            }
        return Response(res)


class LogisticsNoticeUpdateAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def patch(self, request, pk=None):
        try:
            branch = request.user.branchoperator.logistics_branch
            notice = LogisticsNotice.objects.get(id=pk, logistics_branch=branch)
            serializer = LogisticsOperatorNoticeCreateSerializer(notice, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.save()
                res = {
                    'status' : 'success',
                    'data' : serializer.data
                }
            else:
                res = {
                    'message' : serializer.errors
                }
        except Exception as e:
            print(e)
            res = {
                'message' : 'not found'
            }
        return Response(res)


class LogisticsContractRequestedByCustomerListAPIView(APIView, PageNumberPagination):
    permission_classes = [OperatorOnlyPermission]

    def get_paginated_response(self, data, page, page_num):
        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('current', page),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('page_size', page_num),
            ('result', data),
        ]))
    
    def get_queryset(self, request):
        branch = request.user.branchoperator.logistics_branch
        keyword = self.request.GET.get('keyword')
        new_queryset = LogisticsCustomerContract.objects.filter(first_party=branch).order_by('-created_at')
        new_queryset = LogisticsCustomerContract.objects.filter(Q(second_party__full_name__icontains=keyword) | Q(second_party__company_name__icontains=keyword) | Q(second_party__city__name__icontains=keyword))\
             if keyword is not None and keyword != "" else new_queryset
        return self.paginate_queryset(new_queryset, request)
        
    def get(self, request):
        page = self.request.GET.get('page', 1)
        page_size = self.request.GET.get('page_size', 1000)
        contract_list = self.get_queryset(request)
        serializer = LogisticsContractRequestedByCustomerSerializer(contract_list, many=True)
        return self.get_paginated_response(serializer.data, page, page_size)


class LogisticAdminShipmenListAPIView(APIView, PageNumberPagination):
    permission_classes = [OperatorOnlyPermission]

    def get_paginated_response(self, data, page, page_num):
        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('current', page),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('page_size', page_num),
            ('result', data),
        ]))
    
    def get_queryset(self,request):
        keyword = self.request.GET.get('keyword')
        status = self.request.GET.get('status')
        city = self.request.GET.get('cityId')
        branch = request.user.branchoperator.logistics_branch
        if status == 'staff-assigned':
            shipmentlist = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(
                    parcel_origin=branch, shipmentactivity__id=F('latest'), shipmentactivity__shipment_status='Rider Assigned for Pickup').order_by('-created_at')
            shipment_list = Shipment.objects.filter(Q(tracking_code__icontains=keyword) | Q(parcel_origin__logistics_company__company_name__icontains=keyword) | Q(customer__full_name__icontains=keyword) | Q(sender_name__icontains=keyword) | Q(sender_city__name=keyword) | Q(sender_address__icontains=keyword) | Q(
                    sender_contact_number__icontains=keyword) | Q(sender_email__icontains=keyword) | Q(receiver_name__icontains=keyword) | Q(receiver_city__name__icontains=keyword) | Q(receiver_address__icontains=keyword) | Q(receiver_contact_number__icontains=keyword)).filter(parcel_origin=branch).order_by('-created_at') if keyword is not None and keyword != "" else shipmentlist
        elif status == 'staff-picked-up':
            shipmentlist = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(
                parcel_origin=branch, shipmentactivity__id=F('latest'), shipmentactivity__shipment_status='Rider Picked up the Shipment')
            shipmentlist = shipmentlist.filter(Q(tracking_code__icontains=keyword) | Q(parcel_origin__logistics_company__company_name__icontains=keyword) | Q(customer__full_name__icontains=keyword) | Q(sender_name__icontains=keyword) | Q(sender_city__name=keyword) | Q(sender_address__icontains=keyword) | Q(
                sender_contact_number__icontains=keyword) | Q(sender_email__icontains=keyword) | Q(receiver_name__icontains=keyword) | Q(receiver_city__name__icontains=keyword) | Q(receiver_address__icontains=keyword) | Q(receiver_contact_number__icontains=keyword)) if keyword is not None and keyword != "" else shipmentlist
        elif status == 'at-warehouse':
            shipmentlist = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(Q(shipmentactivity__shipment_status='Shipment Arrived at Warehouse') | Q(
                shipmentactivity__shipment_status='Shipment Returned to Logistics'), parcel_origin=branch, shipmentbundleshipment=None, shipmentactivity__id=F('latest')).order_by('-created_at')
            shipmentlist = shipmentlist.filter(Q(tracking_code__icontains=keyword) | Q(parcel_origin__logistics_company__company_name__icontains=keyword) | Q(customer__full_name__icontains=keyword) | Q(sender_name__icontains=keyword) | Q(sender_city__name=keyword) | Q(sender_address__icontains=keyword) | Q(
                sender_contact_number__icontains=keyword) | Q(sender_email__icontains=keyword) | Q(receiver_name__icontains=keyword) | Q(receiver_city__name__icontains=keyword) | Q(receiver_address__icontains=keyword) | Q(receiver_contact_number__icontains=keyword)).order_by('-created_at') if keyword is not None and keyword != "" else shipmentlist
            citylist = City.objects.filter(
                id__in=shipmentlist.values('receiver_city'))
            shipmentlist = shipmentlist.filter(
                receiver_city__id=city) if city is not None and city != "" else shipmentlist
        elif status == 'staff-assigned-for-delivery':
            shipmentlist = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(parcel_origin=branch, shipmentactivity__id=F(
                'latest'), shipmentactivity__shipment_status='Rider Assigned for Dispatch to the Receiver').order_by('-created_at')
            shipmentlist = shipmentlist.filter(Q(tracking_code__icontains=keyword) | Q(parcel_origin__logistics_company__company_name__icontains=keyword) | Q(customer__full_name__icontains=keyword) | Q(sender_name__icontains=keyword) | Q(sender_city__name=keyword) | Q(sender_address__icontains=keyword) | Q(
                sender_contact_number__icontains=keyword) | Q(sender_email__icontains=keyword) | Q(receiver_name__icontains=keyword) | Q(receiver_city__name__icontains=keyword) | Q(receiver_address__icontains=keyword) | Q(receiver_contact_number__icontains=keyword)).order_by('-created_at') if keyword is not None and keyword != "" else shipmentlist
        elif status == 'pickedup-for-delivery':
            shipmentlist = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(parcel_origin=branch, shipmentactivity__id=F(
                'latest'), shipmentactivity__shipment_status='Rider Picked up the Shipment for Dispatch').order_by('-created_at')
            shipmentlist = shipmentlist.filter(Q(tracking_code__icontains=keyword) | Q(parcel_origin__logistics_company__company_name__icontains=keyword) | Q(customer__full_name__icontains=keyword) | Q(sender_name__icontains=keyword) | Q(sender_city__name=keyword) | Q(sender_address__icontains=keyword) | Q(
                sender_contact_number__icontains=keyword) | Q(sender_email__icontains=keyword) | Q(receiver_name__icontains=keyword) | Q(receiver_city__name__icontains=keyword) | Q(receiver_address__icontains=keyword) | Q(receiver_contact_number__icontains=keyword)).order_by('-created_at') if keyword is not None and keyword != "" else shipmentlist
        elif status == 'shipmentdelivered':
            shipmentlist = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(
                parcel_origin=branch, shipmentactivity__id=F('latest'), shipmentactivity__shipment_status='Shipment Delivered to the Receiver').order_by('-created_at')
            shipmentlist = shipmentlist.filter(Q(tracking_code__icontains=keyword) | Q(parcel_origin__logistics_company__company_name__icontains=keyword) | Q(customer__full_name__icontains=keyword) | Q(sender_name__icontains=keyword) | Q(sender_city__name=keyword) | Q(sender_address__icontains=keyword) | Q(
                sender_contact_number__icontains=keyword) | Q(sender_email__icontains=keyword) | Q(receiver_name__icontains=keyword) | Q(receiver_city__name__icontains=keyword) | Q(receiver_address__icontains=keyword) | Q(receiver_contact_number__icontains=keyword)).order_by('-created_at') if keyword is not None and keyword != "" else shipmentlist
        elif status == 'canceled':
            shipmentlist = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(
                parcel_origin=branch, shipmentactivity__id=F('latest'), shipmentactivity__shipment_status='Shipment Canceled').order_by('-created_at')
            shipmentlist = shipmentlist.filter(Q(tracking_code__icontains=keyword) | Q(parcel_origin__logistics_company__company_name__icontains=keyword) | Q(customer__full_name__icontains=keyword) | Q(sender_name__icontains=keyword) | Q(sender_city__name=keyword) | Q(sender_address__icontains=keyword) | Q(
                sender_contact_number__icontains=keyword) | Q(sender_email__icontains=keyword) | Q(receiver_name__icontains=keyword) | Q(receiver_city__name__icontains=keyword) | Q(receiver_address__icontains=keyword) | Q(receiver_contact_number__icontains=keyword)).order_by('-created_at') if keyword is not None and keyword != "" else shipmentlist
        elif status == 'shipment-rejected':
            shipmentlist = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(
                parcel_origin=self.branch, shipmentactivity__id=F('latest'), shipmentactivity__shipment_status='Shipment Rejected')
            shipmentlist = shipmentlist.filter(Q(tracking_code__icontains=keyword) | Q(parcel_origin__logistics_company__company_name__icontains=keyword) | Q(customer__full_name__icontains=keyword) | Q(sender_name__icontains=keyword) | Q(sender_city__name=keyword) | Q(sender_address__icontains=keyword) | Q(
                sender_contact_number__icontains=keyword) | Q(sender_email__icontains=keyword) | Q(receiver_name__icontains=keyword) | Q(receiver_city__name__icontains=keyword) | Q(receiver_address__icontains=keyword) | Q(receiver_contact_number__icontains=keyword)).order_by('-created_at') if keyword is not None and keyword != "" else shipmentlist
        elif status == 'ready-to-return':
            shipmentlist = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(
                parcel_origin=branch, shipmentactivity__id=F('latest'), shipmentactivity__shipment_status='Ready to Return to Sender').order_by('-created_at')
            shipmentlist = shipmentlist.filter(Q(tracking_code__icontains=keyword) | Q(parcel_origin__logistics_company__company_name__icontains=keyword) | Q(customer__full_name__icontains=keyword) | Q(sender_name__icontains=keyword) | Q(sender_city__name=keyword) | Q(sender_address__icontains=keyword) | Q(
                sender_contact_number__icontains=keyword) | Q(sender_email__icontains=keyword) | Q(receiver_name__icontains=keyword) | Q(receiver_city__name__icontains=keyword) | Q(receiver_address__icontains=keyword) | Q(receiver_contact_number__icontains=keyword)).order_by('-created_at') if keyword is not None and keyword != "" else shipmentlist
            citylist = City.objects.filter(
                id__in=shipmentlist.values('sender_city'))
            shipmentlist = shipmentlist.filter(
                receiver_city__id=city) if city is not None and city != "" else shipmentlist
        elif status == 'assign-staff-for-return':
            shipmentlist = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(parcel_origin=branch, shipmentactivity__id=F(
                'latest'), shipmentactivity__shipment_status='Rider Assigned for Returning to the Sender').order_by('-created_at')
            shipmentlist = shipmentlist.filter(Q(tracking_code__icontains=keyword) | Q(parcel_origin__logistics_company__company_name__icontains=keyword) | Q(customer__full_name__icontains=keyword) | Q(sender_name__icontains=keyword) | Q(sender_city__name=keyword) | Q(sender_address__icontains=keyword) | Q(
                sender_contact_number__icontains=keyword) | Q(sender_email__icontains=keyword) | Q(receiver_name__icontains=keyword) | Q(receiver_city__name__icontains=keyword) | Q(receiver_address__icontains=keyword) | Q(receiver_contact_number__icontains=keyword)).order_by('-created_at') if keyword is not None and keyword != "" else shipmentlist
        elif status == 'pickedup-for-return':
            shipmentlist = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(parcel_origin=branch, shipmentactivity__id=F(
                'latest'), shipmentactivity__shipment_status='Rider Picked up the Shipment for Return').order_by('-created_at')
            shipmentlist = shipmentlist.filter(Q(tracking_code__icontains=keyword) | Q(parcel_origin__logistics_company__company_name__icontains=keyword) | Q(customer__full_name__icontains=keyword) | Q(sender_name__icontains=keyword) | Q(sender_city__name=keyword) | Q(sender_address__icontains=keyword) | Q(
                sender_contact_number__icontains=keyword) | Q(sender_email__icontains=keyword) | Q(receiver_name__icontains=keyword) | Q(receiver_city__name__icontains=keyword) | Q(receiver_address__icontains=keyword) | Q(receiver_contact_number__icontains=keyword)).order_by('-created_at') if keyword is not None and keyword != "" else shipmentlist
        elif status == 'shipment-returned':
            shipmentlist = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(
                parcel_origin=branch, shipmentactivity__id=F('latest'), shipmentactivity__shipment_status='Shipment Returned to the Sender').order_by('-created_at')
            shipmentlist = shipmentlist.filter(Q(tracking_code__icontains=keyword) | Q(parcel_origin__logistics_company__company_name__icontains=keyword) | Q(customer__full_name__icontains=keyword) | Q(sender_name__icontains=keyword) | Q(sender_city__name=keyword) | Q(sender_address__icontains=keyword) | Q(
                sender_contact_number__icontains=keyword) | Q(sender_email__icontains=keyword) | Q(receiver_name__icontains=keyword) | Q(receiver_city__name__icontains=keyword) | Q(receiver_address__icontains=keyword) | Q(receiver_contact_number__icontains=keyword)).order_by('-created_at') if keyword is not None and keyword != "" else shipmentlist
        else:
            shipmentlist = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(
                parcel_origin=branch, shipmentactivity__id=F('latest'), shipmentactivity__shipment_status='Shipment Requested').order_by('-created_at')
            shipmentlist = shipmentlist.filter(Q(tracking_code__icontains=keyword) | Q(parcel_origin__logistics_company__company_name__icontains=keyword) | Q(customer__full_name__icontains=keyword) | Q(sender_name__icontains=keyword) | Q(sender_city__name=keyword) | Q(sender_address__icontains=keyword) | Q(
                sender_contact_number__icontains=keyword) | Q(sender_email__icontains=keyword) | Q(receiver_name__icontains=keyword) | Q(receiver_city__name__icontains=keyword) | Q(receiver_address__icontains=keyword) | Q(receiver_contact_number__icontains=keyword)).order_by('-created_at') if keyword is not None and keyword != "" else shipmentlist
        return self.paginate_queryset(shipmentlist, request)
    

    def get(self, request):
        page = self.request.GET.get('page', 1)
        page_size = self.request.GET.get('page_size', 1000)
        shipment_list = self.get_queryset(request)
        serializer = LogisticsAdminShipmentListSerializer(shipment_list, many=True)
        return self.get_paginated_response(serializer.data, page, page_size)


class LogisticsAdminShipmentDetailAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def get(self, request, pk=None):
        try:
            branch = self.request.user.branchoperator.logistics_branch
            shipment = Shipment.objects.get(id=pk, parcel_origin=branch)
            serializer = LogisticsAdminShipmentDetailSerializer(shipment)
            res = { 
                'status' : 'success',
                'data' : {
                    'shipment_status' : serializer.data['shipment'][-1],
                    'sender_name' : serializer.data['sender_name'],
                    'sender_city' : serializer.data['sender_city'],
                    'sender_address' : serializer.data['sender_address'],
                    'sender_contact_number' : serializer.data['sender_contact_number'],
                    'sender_email' : serializer.data['sender_email'],
                    'receiver_name' : serializer.data['receiver_name'],
                    'receiver_city' : serializer.data['receiver_city'],
                    'receiver_address' : serializer.data['receiver_address'],
                    'receiver_contact_number' : serializer.data['receiver_contact_number'],
                    'tracking_code' : serializer.data['tracking_code'],
                    'created_at' : serializer.data['created_at'],
                    'weight' : serializer.data['weight'],
                    'length' : serializer.data['length'],
                    'breadth' : serializer.data['breadth'],
                    'height' : serializer.data['height'],
                    'parcel_type' : serializer.data['parcel_type'],
                    'cod_value' : serializer.data['cod_value'],
                    'pickup_charge' : serializer.data['pickup_charge'],
                    'shipping_charge' : serializer.data['shipping_charge'],
                    'cod_handling_charge' : serializer.data['cod_handling_charge'],
                    'rejection_handling_charge' : serializer.data['rejection_handling_charge'],
                    'extra_charge' : serializer.data['extra_charge'],
                    'remarks' : serializer.data['shipment_remarks']
                }
            }
        except Exception as e:
            print(e)
            res = {
                'message' : 'not found'
            }
        return Response(res)


class LogisticsAdminShipmentUpdateAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def put(self, request, pk=None):
        try:
            branch = self.request.user.branchoperator.logistics_branch
            shipment = Shipment.objects.get(id=pk, parcel_origin=branch)
            serializer = LogisticsAdminShipmentUpdateSerializer(data=request.data)
            if serializer.is_valid():
                sender_city = request.data.get('sender_city')
                receiver_city = request.data.get('receiver_city')
                city = City.objects.get(id=sender_city)
                cit1y = City.objects.get(id=receiver_city)
                shipment.sender_name = serializer.data.get('sender_name', shipment.sender_name)
                shipment.sender_city = serializer.data.get(city, shipment.sender_city)
                shipment.sender_address = serializer.data.get('sender_address', shipment.sender_address)
                shipment.sender_contact_number = serializer.data.get('sender_contact_number', shipment.sender_contact_number)
                shipment.sender_email = serializer.data.get('sender_email', shipment.sender_email)
                shipment.receiver_name = serializer.data.get('receiver_name', shipment.receiver_name)
                shipment.receiver_city = serializer.data.get(cit1y, shipment.receiver_city)
                shipment.receiver_address = serializer.data.get('receiver_address', shipment.receiver_address)
                shipment.receiver_contact_number = serializer.data.get('receiver_contact_number', shipment.receiver_contact_number)
                shipment.weight = serializer.data.get('weight', shipment.weight)
                shipment.length = serializer.data.get('length', shipment.length)
                shipment.breadth = serializer.data.get('breadth', shipment.breadth)
                shipment.height = serializer.data.get('height', shipment.height)
                shipment.parcel_type = serializer.data.get('parcel_type', shipment.parcel_type)
                shipment.cod_value = serializer.data.get('cod_value', shipment.cod_value)
                shipment.pickup_charge = serializer.data.get('pickup_charge', shipment.pickup_charge)
                shipment.shipping_charge = serializer.data.get('shipping_charge', shipment.shipping_charge)
                shipment.cod_handling_charge = serializer.data.get('cod_handling_charge', shipment.cod_handling_charge)
                shipment.rejection_handling_charge = serializer.data.get('rejection_handling_charge', shipment.rejection_handling_charge)
                shipment.extra_charge = serializer.data.get('extra_charge', shipment.extra_charge)
                shipment_status = serializer.data.get('shipment_status')
                if shipment_status == ShipmentActivity.objects.filter(shipment=shipment).last().shipment_status:
                    shipment.shipmentactivity_set.last()
                    shipment.save()
                else:
                    shipment_activity = ShipmentActivity.objects.create(shipment=shipment, shipment_status=shipment_status, status='Active')
                    shipment.save()
                res = {
                    'status':'success',
                    'data' : serializer.data
                }
            else:
                res = {
                    'message' : serializer.errors
                }
            return Response(res)
        except Exception as e:
            print(e)
            res = {
                'message' : 'not found'
            }
            return Response(res)


class LogisticsAdminShipmentRemarksCreateAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def post(self, request, pk=None):
        try:
            branch = self.request.user.branchoperator.logistics_branch
            op_id = self.request.user.branchoperator.id
            shipment = Shipment.objects.get(id=pk, parcel_origin=branch)
            serializer = LogisticsAdminShipmentRemarksSerializer(data=request.data)
            if serializer.is_valid():
                remarks = serializer.data['remarks']
                content_type = ContentType.objects.get(app_label='umapp', model='branchoperator')
                ShipmentRemark.objects.create(status='Active', shipment=shipment ,remarks=remarks, remarks_by_type=content_type, remarks_by_id=op_id)
                res = {
                    'status' : 'success',
                    'data' : serializer.data
                }
            else:
                res = {
                    'message' : serializer.errors
                }
        except Exception as e:
            print(e)
            res = {
                'message' : 'not found'
            }
        return Response(res)


class LogisticsAdminShipmentCreateAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def post(self, request):
        branch = self.request.user.branchoperator.logistics_branch
        serializer = LogisticsAdminShipmentCreateSerializer(data=request.data)
        if serializer.is_valid():
            shipment = serializer.save(parcel_origin=branch)
            unit = serializer.data.get('length_unit')
            shipment.breadth_unit = unit
            shipment.height_unit = unit
            shipment.status = 'Pending'
            shipment.tracking_code = 'tracking-' + str(shipment.id)
            shipment.save()
            activity_by = self.request.user.branchoperator
            ShipmentActivity.objects.create(status='Active', shipment=shipment, shipment_status='Shipment Requested',activity='Shipment Created by Logistics Branch', activity_by=activity_by)
            res = {
                'status' : 'success',
                'data' : serializer.data
            }
        else:
            res = {
                'message' : serializer.errors
            }
        return Response(res)


class StaffListAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def get(self, request):
        branch = self.request.user.branchoperator.logistics_branch
        staff_list = DeliveryPerson.objects.filter(logistics_branch=branch)
        serializer = StaffListSerializer(staff_list, many=True)
        res = {
            'status' : 'success',
            'data' : serializer.data
        }
        return Response(res)


class LogisticsAdminShipmentChangeStatusAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def post(self, request, *args, **kwargs):
        shipment_ids = request.GET.get("shipment_ids")
        print(shipment_ids)
        shipmentlist = Shipment.objects.filter(id=shipment_ids)
        print(shipmentlist)
        status = request.GET.get("status")
        time_now = timezone.localtime(timezone.now())
        if status == 'assignstaff':
            staff = request.GET.get("staff")
            rider = DeliveryPerson.objects.get(id=staff)
            [ShipmentActivity.objects.create(shipment=shipment, rider=rider, shipment_status='Rider Assigned for Pickup', status="Active") and DeliveryTask.objects.create(shipment=shipment, delivery_person=rider, task_type="Pick Up", task_status="Assigned", scheduled_date=time_now, status="Active") for shipment in shipmentlist]
            return Response({"status": "success"})
        elif status == 'riderpickedupshipment':
            for shipment in shipmentlist:
                ShipmentActivity.objects.create(
                    shipment=shipment, shipment_status='Rider Picked up the Shipment')
            return Response({"status": "success"})

        elif status == 'cancelshipment':
            for shipment in shipmentlist:
                ShipmentActivity.objects.create(
                    shipment=shipment, shipment_status='Shipment Canceled', status="Active")
            return Response({"status": "success"})

        elif status == 'pickedupshipment':
            DeliveryTask.objects.filter(shipment__id__in=shipment_ids, task_type="Pick Up").update(
                task_status='Completed Success', completed_date=time_now)
            for shipment in shipmentlist:
                ShipmentActivity.objects.create(
                    shipment=shipment, shipment_status='Shipment Arrived at Warehouse', status="Active")
            return Response({"status": "success"})

        elif status == 'assignstafffordelivery':
            staff = request.GET.get("staff")
            rider = DeliveryPerson.objects.get(id=staff)
            [ShipmentActivity.objects.create(shipment=shipment, rider=rider, shipment_status='Rider Assigned for Dispatch to the Receiver', status="Active") and DeliveryTask.objects.create(
                shipment=shipment, delivery_person=rider, task_type="Drop Off", task_status="Assigned", scheduled_date=time_now, status="Active") for shipment in shipmentlist]
            return Response({"status": "success"})

        elif status == 'pickedupfordelivery':
            DeliveryTask.objects.filter(
                shipment__id__in=shipment_ids, task_type="Drop Off").update(task_status='Doing')
            for shipment in shipmentlist:
                ShipmentActivity.objects.create(
                    shipment=shipment, shipment_status='Rider Picked up the Shipment for Dispatch', status="Active")
            return Response({"status": "success"})

        elif status == 'confirmdelivered':
            DeliveryTask.objects.filter(shipment__id__in=shipment_ids, task_type="Drop Off").update(
                task_status='Completed Success', completed_date=time_now)
            for shipment in shipmentlist:
                ShipmentActivity.objects.create(
                    shipment=shipment, shipment_status='Shipment Delivered to the Receiver', status="Active")
            return Response({"status": "success"})

        elif status == 'returnedtowarehouse':
            for shipment in shipmentlist:
                ShipmentActivity.objects.create(
                    shipment=shipment, shipment_status='Ready to Return to Sender', status="Active")
            return Response({"status": "success"})

        elif status == 'reasignshipmentfordelivery':
            staff = request.GET.get("staff")
            rider = DeliveryPerson.objects.get(id=staff)
            for shipment in shipmentlist:
                ShipmentActivity.objects.create(
                    shipment=shipment, shipment_status='Shipment Returned to Logistics', status="Active")
            return Response({"status": "success"})

        elif status == 'assignshipmentforreturn':
            staff = request.GET.get("staff")
            rider = DeliveryPerson.objects.get(id=staff)
            for shipment in shipmentlist:
                ShipmentActivity.objects.create(
                    shipment=shipment, rider=rider, shipment_status='Rider Assigned for Returning to the Sender', status="Active")
            return Response({"status": "success"})

        elif status == 'pickedupshipmentforreturn':
            for shipment in shipmentlist:
                ShipmentActivity.objects.create(
                    shipment=shipment, shipment_status='Rider Picked up the Shipment for Return', status="Active")
            return Response({"status": "success"})
        else:
            return Response({"status": "error"})


class DeliveryPersonListAPIView(APIView, PageNumberPagination):
    permission_classes = [OperatorOnlyPermission]
    def get_paginated_response(self, data, page, page_num):
        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('current', page),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('page_size', page_num),
            ('result', data),
        ]))
    
    def get_queryset(self, request):
        branch = request.user.branchoperator.logistics_branch
        keyword = self.request.GET.get('keyword')
        new_queryset = DeliveryPerson.objects.filter(logistics_branch=branch).order_by('-created_at')
        new_queryset = DeliveryPerson.objects.filter(Q(full_name__icontains=keyword) | Q(mobile__exact=keyword) | Q(alt_mobile__exact=keyword))\
             if keyword is not None and keyword != "" else new_queryset
        return self.paginate_queryset(new_queryset, request)
        
    def get(self, request):
        page = self.request.GET.get('page', 1)
        page_size = self.request.GET.get('page_size', 1000)
        delivery_person_list = self.get_queryset(request)
        serializer = DeliveryPersonListSerializer(delivery_person_list, many=True)
        return self.get_paginated_response(serializer.data, page, page_size)


class DeliveryPersonDetailAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def get(self, request,pk=None):
        try:
            branch = self.request.user.branchoperator.logistics_branch
            delivery_person = DeliveryPerson.objects.get(id=pk, logistics_branch=branch)
            serializer = DeliveryPersonDetailSerializer(delivery_person)
            res = {
                'status' : 'success',
                'data' : serializer.data
            }
        except Exception as e:
            print(e)
            res = {
                'message' : 'not found'
            }
        return Response(res)


class DeliveryPersonCreateAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def post(self, request):
        branch = self.request.user.branchoperator.logistics_branch
        serializer = DeliveryPersonCreateSerializer(data=request.data)
        if serializer.is_valid():
            email = serializer.data['email']
            password = serializer.data['password']
            image = serializer.data['image']
            full_name = serializer.data['full_name']
            mobile = serializer.data['mobile']
            address_city = serializer.data['address_city']
            city = City.objects.get(id=address_city)
            user = User.objects.create_user(username=email, email=email, password=password)
            if user is not None:
                delivery_person = DeliveryPerson.objects.create(status='Active',user=user, logistics_branch=branch, full_name=full_name, image=image, mobile=mobile, address_city=city)
                delivery_person.connected_branches.add(branch)
                res = {
                    'status' : 'success',
                    'data' : serializer.data
                }
            else:
                res = {
                    'message' : 'user is none'
                }
        else:
            res = {
                'message' : serializer.errors
            }
        return Response(res)


class DeliveryPersonUpdateAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def patch(self, request, pk=None):
        try:
            branch = self.request.user.branchoperator.logistics_branch
            delivery_person = DeliveryPerson.objects.get(id=pk, logistics_branch=branch)
            serializer = DeliveryPersonUpdateSerializer(delivery_person, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.save()
                res = {
                    'status' : 'success',
                    'data' : serializer.data
                }
            else:
                res = {
                    'message' : serializer.errors
                }
        except Exception as e:
            print(e)
            res = {
                'message' : 'not found'
            }
        return Response(res)


class DeliveryPersonPasswordChangeByOperatorAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def patch(self, request, pk=None):
        try:
            branch = self.request.user.branchoperator.logistics_branch
            delivery_person = DeliveryPerson.objects.get(id=pk, logistics_branch=branch)
            user = delivery_person.user
            serializer = DeliveryPersonPasswordChangeSerializer(data=request.data)
            if serializer.is_valid():
                if delivery_person.logistics_branch == branch:
                    user.set_password(serializer.validated_data['new_password'])
                    user.save()
                    res = {
                        'status' : 'success',
                        'data' : serializer.data
                    }
                else:
                    res = {
                        'message' : 'authorization failed'
                    }
            else:
                res = {
                    'message' : serializer.errors
                }
        except Exception as e:
            print(e)
            res = {
                'message' : 'not found'
            }
        return Response(res)


class CityListAPIView(APIView, PageNumberPagination):
    permission_classes = [OperatorOnlyPermission]

    def get_paginated_response(self, data, page, page_num):
        return Response(OrderedDict([
        ('count', self.page.paginator.count),
        ('current', page),
        ('next', self.get_next_link()),
        ('previous', self.get_previous_link()),
        ('page_size', page_num),
        ('result', data)
        ]))

    
    def get_queryset(self, request):
        keyword = self.request.GET.get('keyword')
        new_queryset = City.objects.filter(name__icontains=keyword) if keyword is not None and keyword != '' else \
            City.objects.all()
        return self.paginate_queryset(new_queryset, request)

    def get(self, request):
        page = self.request.GET.get('page', 1)
        page_size = self.request.GET.get('page_size', 1000)
        city_list = self.get_queryset(request)
        serializer = CitySerializer(city_list, many=True)
        return self.get_paginated_response(serializer.data, page, page_size)


class CityCreateAPIView(APIView):
    permission_classes = [OperatorOnlyPermission]

    def post(self, request):
        serializer = CityCreateSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            res = {
                'status' : 'success',
                'data' : serializer.data
            }
        else:
            res = {
                'message' : serializer.errors
            }
        return Response(res)