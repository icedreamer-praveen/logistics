from datetime import timedelta, timezone
from django.conf import settings
from django.core.mail import send_mail
from django.db import reset_queries
from django.http.response import Http404
from django.template.loader import render_to_string
from django.utils import encoding
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from rest_framework.views import APIView
from django.contrib.auth import authenticate
from rest_framework.response import Response
from rest_framework import pagination, status

from accapp.constants import customer_processing_sl, customer_pending_sl, \
        customer_dispatched__sl, customer_rejected_sl, customer_delivered_sl, customer_returned_sl, customer_canceled_sl, \
        customer_completed_charge_sl
from lmsapp.models import ShipmentActivity
from siteapp.models import LogisticsOrganizationalInformation, RiderApplication
from .mypaginations import CustomPagination

from accapp.models import City, LogisticsBranch
from lbcapp.models import LogisticsBranchShippingZone
from lmsapp.utils import get_current_balance, get_incomplete_charge, get_payment, password_reset_token
from .serializers import *
from lmsapp.models import Shipment, ShipmentRemark
from django.contrib.contenttypes.models import ContentType
from .utils import *
from django.utils import timezone
from django.utils.timezone import localtime


class CustomerLoginAPIView(APIView):
    def post(self, request):
        serializer = CustomerLoginSerializer(data=request.data)
        if serializer.is_valid():
            email = serializer.validated_data.get("email")
            password = serializer.validated_data.get("password")
            user = authenticate(username=email, password=password)
            try:
                customer = user.logisticscustomer
                if customer.status == "Active":
                    resp = get_tokens_for_user(user)
                else:
                    resp = {
                        "message": "Pending or Suspended account.."
                    }
            except Exception as e:
                print(e)
                resp = {
                    "message": "Invalid credentials of the Customer.."
                }
        else:
            resp = {
                "message": "Missing information"
            }
        return Response(resp)

            


# Dashboard Shipment Overview
class CustomerDashbaordShipmentOverviewAPIView(APIView):
    permission_classes = [CustomerOnlyPermission]

    def get(self, request):
        customer = request.user.logisticscustomer
        shipment = Shipment.objects.filter(customer = customer)

        requested = shipment.filter(shipment_status = "Shipment Requested")
        processing = shipment.filter(shipment_status__in = customer_processing_sl)
        dispatched = shipment.filter(shipment_status__in = customer_dispatched__sl)
        rejected = shipment.filter(shipment_status__in = customer_rejected_sl)
        delivered = shipment.filter(shipment_status__in = customer_delivered_sl)
        returned = shipment.filter(shipment_status__in = customer_returned_sl)
        cancel = shipment.filter(shipment_status__in = customer_canceled_sl)
        all_shipments = shipment

        resp = {
            "status": "success",
            "requested": requested.count(),
            "processing": processing.count(),
            "dispatched": dispatched.count(),
            "rejected": rejected.count(),
            "delivered": delivered.count(),
            "returned": returned.count(),
            "cancel": cancel.count(),
            "all_shipments": all_shipments.count(),
        }
        return Response(resp)

#dashboard payment overview
class CustomerDashboardPaymentOverviewAPIView(APIView):
    permission_classes = [CustomerOnlyPermission]

    def get(self, request):
        customer = request.user.logisticscustomer

        current_balance = get_current_balance(customer)
        incomplete_cod_amount = get_incomplete_charge(customer)[0]
        incomplete_charge = get_incomplete_charge(customer)[1]
        total_cod_received = get_payment(customer)[0]
        total_charge_paid = get_payment(customer)[1]
        net_paid_or_received = get_payment(customer)[2]

        resp = {
            "status": "success",
            "current_balance": current_balance,
            "incomplete_cod_amount": incomplete_cod_amount,
            "incomplete_charge": incomplete_charge,
            "total_cod_received": total_cod_received,
            "total_charge_paid": total_charge_paid,
            "net_paid_or_received": net_paid_or_received
        }
        return Response(resp)

# dashboard remarks        
class CustomerDashboardRemarksListAPIView(APIView):
    permission_classes = [CustomerOnlyPermission]

    def get(self, request):
        customer = request.user.logisticscustomer 

        # remarks 
        latest_remarks = ShipmentRemark.objects.filter(status="Active", is_private=False, shipment__customer=customer).order_by("-created_at")[:10]
        serializer = CustomerDashboardRemarksSerializer(latest_remarks, many=True)
        resp = {
            "status": "success",
            "message": "Remarks created",
            "remarks": serializer.data
        }
        return Response(resp)


# dashbaord new orders
class CustomerDashboardNewOrdersAPIView(APIView):
    permission_classes = [CustomerOnlyPermission]

    def get(self, request):
        customer = request.user.logisticscustomer
        days = []
        data = []
        today = timezone.localtime(timezone.now()).date()
        start_date = today - timedelta(days=6)
        for i in range(7):
            date = start_date + timedelta(days=i)
            shipments = Shipment.objects.filter(customer=customer, created_at__date=date).count()
            days.append(date)
            data.append(shipments)
            resp = {
                "status": "success",
                "days":date,
                "data": data
            }
            return Response(resp)
    
# # Customer profile view
class CustomerProfileAPIView(APIView):
    permission_classes = [CustomerOnlyPermission]


    def get(self, request):
        customer = request.user.logisticscustomer
        serializer = CustomerProfileSerializer(customer)
        connected_ligistics = customer.connected_logistics
        resp = {
            "status": "success",
            'data': serializer.data,
            "connected_logistics": connected_ligistics.count()
        }
        return Response(resp)


# customer profile update
class CustomerProfileUpdateAPIView(APIView):
    permission_classes = [CustomerOnlyPermission]

    def patch(self, request):
        cust_id = self.request.user.logisticscustomer.id
        customer = LogisticsCustomer.objects.get(id=cust_id)
        profile_data = CustomerProfileUpdateSerializer(customer, data=request.data, partial=True)
        if profile_data.is_valid():
            profile_data.save()
            resp = {
                "status": "success",
                "message": "Profile updated",
                "data": profile_data.data
            }
        else:
            resp = {
                "status": "failure",
                "message": profile_data.errors
            }
        return Response(resp)


# customer forgot password
class CustomerForgotPasswordAPIView(APIView):

    def post(self, request):
        serializer = CustomerForgotPasswordSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            email = serializer.validated_data.get("email")
            user = User.objects.get(email=email)
            domain = 'localhost:3000'
            url = 'http://' + domain
            html_content = render_to_string(
            'customertemplates/customerapipasswordresetemail.html',
            {
                'domain': url, 'email': email,
                'token': password_reset_token.make_token(user),
                'uid': urlsafe_base64_encode(
                    encoding.force_bytes(user.pk)),
                'user': user,
                'organization': LogisticsOrganizationalInformation.objects.filter(status="Active").first()
            })
            send_mail(
                'Password Reset from ' + str(domain),
                html_content,
                settings.EMAIL_HOST_USER,
                [email],
                fail_silently=False
            )
            resp = {
                "status": "success",
                "message": "Password Reset Link has been send to your email. Please check your email."
            }
            return Response(resp)
        else:
            resp = {
                "status": "failure",
                "message": serializer.errors
            }
            return Response(resp)


#customer password reset
class CustomerResetPasswordAPIView(APIView):

    def get(self, request, uidb64, token):
        try:
            uid = encoding.force_text(urlsafe_base64_decode(uidb64))
            user = User.objects.get(id=uid)
            if user is not None and password_reset_token.check_token(user, token):
                resp = {"status": "success", "message": 'Token is valid.', "validlink": True}
            else:
                resp = {"status": "failure", "message": 'Token is not valid, please request new one.'}
        except Exception as e:
            print(e)
            resp = {
                "status": "failure",
                "message": "Invalid token",
                "validlink": False
            }
        return Response(resp)

    def post(self, request, *args, **kwargs):
        password = CustomerResetPasswordSerializer(data=request.data)
        if password.is_valid(raise_exception=True):
            try:
                token = self.kwargs.get("token")
                uidb64 = self.kwargs.get("uidb64")
                uid = encoding.force_text(urlsafe_base64_decode(uidb64))
                user = User.objects.get(id=uid)
                if user is not None and password_reset_token.check_token(user, token):
                    new_password2 = password.validated_data.get("new_password2")
                    user.set_password(new_password2)
                    user.save()
                    return Response({"status": "success", "message": "Password reset success."})
            except Exception as e:
                print(e)
                resp = {
                    "status": "failure",
                    "message": "Password did not match!"
                }
            return Response(resp)
        else:
            resp = {
                "status": "failure",
                "message": password.errors
            }
            return Response(resp)


# Change password
class CustomerPasswordChangeAPIView(APIView):
    permission_classes = [CustomerOnlyPermission]

    def post(self, request, *args, **kwargs):
        password_data = CustomerChangePasswordSerializer(data=request.data)
        if password_data.is_valid(raise_exception=True):
            email = password_data.validated_data.get("email")
            user = User.objects.get(email=email)
            password = password_data.validated_data.get("password")
            user.set_password(password)
            user.save()
            resp = {
                "status": "success",
                "message": "Password Change Successfully."
            }
            return Response(resp)
        else:
            resp = {
                "status": "failure",
                "message": password_data.errors
            }
            return Response(resp)



# Customer shipment list view
class CustomerShipmentListAPIView(APIView):
    permission_classes = [CustomerOnlyPermission]
    pagination_class = CustomPagination()

    def get(self, request, format=None):
        customer = request.user.logisticscustomer
        shipments = Shipment.objects.filter(customer=customer)
        
        pending_shipment = shipments.filter(shipment_status="Shipment Requested")
        processing_shipment = shipments.filter(shipment_status__in=customer_processing_sl)
        dispatched_shipment = shipments.filter(shipment_status__in=["Rider Picked up the Shipment for Dispatch"])
        rejected_shipment = shipments.filter(shipment_status__in=["Shipment Rejected", "Shipment Returned to Logistics"])
        delivered_shipment = shipments.filter(shipment_status__in=["Shipment Delivered to the Receiver"])
        returned_shipment = shipments.filter(shipment_status__in=["Shipment Returned to the Sender"])

        resp = {
            "status": "success",
            "pending_shipment": pending_shipment.count(),
            "processing_shipment": processing_shipment.count(),
            "dispatched_shipment": dispatched_shipment.count(),
            "rejected_shipment": rejected_shipment.count(),
            "delivered_shipment": delivered_shipment.count(),
            "returned_shipment": returned_shipment.count()
        }
       


        #for pagination
        page = self.pagination_class.paginate_queryset(queryset=shipments, request=request)

        serializer = CustomerShipmentListSerializer(page, many=True)
        resp["data"] = serializer.data
        return self.pagination_class.get_paginated_response(resp)

# Customer shipment dettail view
class CustomerShipmentDetailAPIView(APIView):
    permission_classes = [CustomerOnlyPermission]

    def get(self, request, *args, **kwargs):
        shipment = Shipment.objects.get(tracking_code=self.kwargs.get("tracking_code"))
        serializer = CustomerShipmentDetailSerializer(shipment)
        resp = {
            "status": "success",
            "data": serializer.data
        }
        return Response(resp)


# Customer remarks create view
class CustomerRemarksCreateAPIView(APIView):

    def post(self, request, pk):
        customer = request.user.logisticscustomer
        remarks_data = CustomerRemarksSerializer(data=request.data)
        if remarks_data.is_valid():
            try:
                shipment = Shipment.objects.get(id=self.kwargs.get("pk"), customer=customer)
                remarks = remarks_data.validated_data.get("remarks")

                new_remarks = ShipmentRemark.objects.create(status="Active", shipment=shipment, remarks=remarks, remarks_by=customer,
                    is_private=False)

                new_remarks.save()
                resp = {
                    "status": "success",
                    "message": "Shipment Remarks Created."
                }
                return Response(resp)
            except Exception as e:
                print(e)
                return Response({"status": "failure", "message": "Invalid Data"})
        else:
            resp = {
                "status": "failure",
                "message": remarks_data.errors
            }
            return Response(resp)


# reamrks list
class CustomerRemarksListAPIView(APIView):
    permission_classes = [CustomerOnlyPermission]

    def get(self, request, pk):
        customer = request.user.logisticscustomer
        shipment = Shipment.objects.get(id=pk, customer=customer)
        remarks = ShipmentRemark.objects.filter(shipment=shipment)
        serializer = CustomerRemarksSerializer(remarks, many=True)
        resp = {
            "status": "success",
            "data": serializer.data
        }
        return Response(resp)

# customer shipment city list View
class CustomerShipmentCityListAPIView(APIView):
    permission_classes = [CustomerOnlyPermission]

    def get(self, request, format=None):
        city = City.objects.all()
        
        serializer = CustomerCityListSerializer(city, many=True)
        resp = {
            "status": "success",
            "data": serializer.data
        }
        return Response(resp)

# Customer shipment zone view
class CustomerShipmentZonesAPIView(APIView):
    permission_classes = [CustomerOnlyPermission]

    def get(self, request, pk):
        customer = request.user.logisticscustomer
        
        city_obj = City.objects.get(id=pk)
        
        branches = customer.connected_logistics.filter(status="Active")
        shipping_zones = LogisticsBranchShippingZone.objects.filter(
            logistics_branch__branch_coverage=customer.city, logistics_branch__in=branches, cities__id=city_obj.id).distinct()

        serializer = CustomerChargeSerializer(shipping_zones, many=True)
        resp = {
            "status": "success",
            "data": serializer.data
        }
        return Response(resp)

# Customer place new shipment view
class CustomerPlaceNewShipmentCreateAPIView(APIView):
    permission_classes = [CustomerOnlyPermission]

    def post(self, request):
        customer = request.user.logisticscustomer
        shipment_serializer = CustomerShipmentCreateSerializer(data=request.data)
        sz_id = request.data.get("shipping_zone")
        if shipment_serializer.is_valid():
            logistics_branch = LogisticsBranch.objects.first()
            try:
                sz = LogisticsBranchShippingZone.objects.get(id=sz_id)
            except Exception as e:
                print("Exception", e)
                return Response({
                    "status": "success",
                    "message": "Delivery not available at the selected location for now. Please contact logistics or select a different city"})
            
            shipment = shipment_serializer.save(
                status="Active", sender_city=customer.city, shipment_origin=sz.logistics_branch
            )

            shipment.shipment_status = "Shipment Requested"

            # sender information 
            shipment.customer = customer
            shipment.sender_name = customer.company_name
            shipment.sender_address = customer.address
            shipment.sender_contact_number = customer.contact_number
            shipment.sender_email = customer.user.username

            # cost and payment information 
            cod_value = shipment_serializer.validated_data.get("cod_value")
            szdt = sz.shippingzonedeliverytype_set.filter(delivery_type__delivery_type__in=["Standard", "Standard Delivery"]).last()
            shipment.delivery_type = szdt
            shipment.shipping_charge = szdt.shipping_charge_per_kg
            shipment.cod_handling_charge = szdt.cod_handling_charge * cod_value / 100
            shipment.rejection_handling_charge = szdt.cod_handling_charge * cod_value / 100
            shipment.dropoff_charge = 0 
            shipment.pickup_charge = 0
            shipment.extra_charge = 0
            shipment.service_payment_status = False
            if cod_value > 0:
                shipment.cod_info = "Cash on Delivery"
            else:
                shipment.cod_info = "Already Paid"
            shipment.tracking_code = f"SARATHI-PT-{shipment.id}"
            shipment.save()
            
            ShipmentActivity.objects.create(
                shipment=shipment, shipment_status="Shipment Requested", activity="Shipment uploaded by the customer.")

            resp = {
                "status": "success",
                "message": "New shipment created"
            }
            return Response(resp)
        else:
            resp = {
                "status": "failure",
                "message": shipment_serializer.errors
            }
            return Response(resp)
        


# # Customer Bulk shipment view
# class CustomerBulkShipmentBranchListAPIView(APIView):
#     permission_classes = [CustomerOnlyPermission]

#     def get(self, request, format=None):
#         l_branch = LogisticsBranch.objects.all()

#         serializer = CustomerBulkShipmentBranchListSerializer(l_branch, many=True)
#         resp = {
#             "status": "success",
#             "data": serializer.data
#         }
#         return Response(resp)


#Customer Payment Req view
class CustomerRequestPaymentListAPIView(APIView):
    permission_classes = [CustomerOnlyPermission]
    pagination_class = CustomPagination()

    def get(self, request, format=None):
        customer = request.user.logisticscustomer
        request_payment_list = CustomerPaymentRequest.objects.filter(logistics_customer=customer)

        settled_request = request_payment_list.filter(is_settled=True)
        unsettled_request = request_payment_list.exclude(is_settled=True)
        all_requests = request_payment_list
        current_balance = get_current_balance(customer)
        

        resp = {
            "status": "success",
            "settled_request": settled_request.count(),
            "unsettled_request": unsettled_request.count(),
            "all_requests": all_requests.count(),
            "current_balance": current_balance
        }
        #for pagination
        page = self.pagination_class.paginate_queryset(queryset=request_payment_list, request=request)
        if page is not None:
            serializer = CustomerRequestPaymentListSerializer(page, many=True)
            resp["data"]  = serializer.data
            return self.pagination_class.get_paginated_response(resp)

    
        serializer = CustomerRequestPaymentListSerializer(request_payment_list, many=True)
        resp["data"]  = serializer.data
        
        return Response(resp)



# Customer payment list view
class CustomerPaymentListAPIView(APIView):
    permission_classes = [CustomerOnlyPermission]
    pagination_class = CustomPagination()

    def get(self, request, format=None):
        customer = request.user.logisticscustomer
        payment_list = CustomerPayment.objects.filter(logistics_customer=customer)
        
        #for pagination
        page = self.pagination_class.paginate_queryset(queryset=payment_list, request=request)
        if page is not None:
            serializer = CustomerPaymentListSerializer(page, many=True)
            resp = {
                "status": "success",
                "data": serializer.data
            }
            return self.pagination_class.get_paginated_response(resp)

        serializer = CustomerPaymentListSerializer(payment_list, many=True)
        resp = {
            "status": "success",
            "data": serializer.data
        }
        return Response(resp)


# Customer payment detail view
class CustomerPaymentDetailAPIView(APIView):
    permission_classes = [CustomerOnlyPermission]

    def get_object(self, pk):
        try:
            return CustomerPayment.objects.get(id=pk)
        except CustomerPayment.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        payments = self.get_object(pk)
        serializer = CustomerPaymentDetailSerializer(payments)

        resp = {
            "status": "success",
            "data": serializer.data
        }
        return Response(resp)



# service provider list for payment req
class CustomerPaymentRequestServiceProviderListAPIView(APIView):
    permission_classes = [CustomerOnlyPermission]

    def get(self, request):
        customer = request.user.logisticscustomer
        all_logistics_branch = customer.connected_logistics.all()
        
        serializer = CustomerPaymentRequestServiceProviderSerializer(all_logistics_branch, many=True)

        resp = {
            "status": "success",
            "data": serializer.data
        }
        return Response(resp)


# Receiving accoutn list for payment req
class CustomerPaymentRequestReceivingAccountListAPIView(APIView):
    permission_classes = [CustomerOnlyPermission]

    def get(self, request):
        customer = request.user.logisticscustomer
        receiving_accounts = customer.customerpaymentaccount_set.all()

        serializer = CustomerPaymentRequestReceivingAccountSerializer(receiving_accounts, many=True)
        resp = {
            "status": "success",
            "data": serializer.data
        }
        return Response(resp)


# get current balance for payment req 
class CustomerPaymentRequestCurrentBalanceAPIView(APIView):
    permission_classes = [CustomerOnlyPermission]
    
    def get(self, request, *args, **kwargs):
        customer = request.user.logisticscustomer
        branch_id = self.kwargs["pk"]
        branch = customer.connected_logistics.get(id=branch_id)
        shipments = Shipment.objects.filter(customer=customer, shipment_status__in=customer_completed_charge_sl, service_payment_status=False)
        current_balance = get_current_balance(customer, shipment_origin=branch)
        unsettled_requests_exists = CustomerPaymentRequest.objects.filter(
            status="Active", logistics_customer=customer, logistics_branch=branch, is_settled=False).exists()

        resp = {
            "status": "success",
            "shipment_count": shipments.count(),
            "current_balance": current_balance,
            "unsettled_requests_exists": unsettled_requests_exists
        }
        serializer = CustomerPaymentRequestCurrentBalanceSerializer(branch)
        resp["data"] = serializer.data
        return Response(resp)


# payment req create view
class CustomerPaymentRequestCreateAPIView(APIView):
    permission_classes = [CustomerOnlyPermission]

    def post(self, request):
        customer = request.user.logisticscustomer
        payment_request = CustomerPaymentRequestCreateSerializer(data=request.data)
        branch = request.data.get("logistics_branch")
        logistics_branch = LogisticsBranch.objects.get(id=branch)
        if payment_request.is_valid():
            shipments = Shipment.objects.filter(customer=customer, shipment_status__in=customer_completed_charge_sl, service_payment_status=False)
            current_balance = get_current_balance(customer, shipment_origin=branch)
            if CustomerPaymentRequest.objects.filter(status="Active", logistics_customer=customer, logistics_branch=branch, is_settled=False).exists():
                return Response({"status": "failure", "message": "You have previous unsettled request left. Please contact your service provider or raise an issue."})

            else:
                if current_balance > 0 and shipments.count() > 0:
                    payment_request.logisctics_customer = customer
                    cpr = payment_request.save(
                        logistics_branch=logistics_branch, logistics_customer=customer
                    )
                    cpr.status = "Active"
                    cpr.shipments.add(*shipments)
                    cpr.request_amount = current_balance

                    cpr.save()

                    return Response({
                        "status": "success",
                        "message": "Payment Request Created Successfully"
                    })
                else:
                    return Response({"status": "failure", "message": "You don't have balance in your wallet"})

        else:
            resp = {
                "status": "failure",
                "message": payment_request.errors
            }
            return Response(resp)

       


    

        


        









