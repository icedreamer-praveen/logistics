from re import search
from django.contrib.auth.models import User
from django.contrib.contenttypes import fields
from django.forms.fields import EmailField
from django.http import request
from django.http.response import FileResponse
from django.utils.regex_helper import flatten_result
from rest_framework import serializers, validators
from rest_framework.fields import SerializerMethodField
from rest_framework.views import set_rollback
from datetime import datetime
from accapp.models import City, LogisticsBranch, LogisticsCustomer
from lmsapp.models import CustomerPayment, CustomerPaymentRequest, Shipment, ShipmentRemark
from lbcapp.models import CustomerPaymentAccount, LogisticsBranchShippingZone, ShippingZoneDeliveryType


class CustomerLoginSerializer(serializers.Serializer):
    email = serializers.EmailField(allow_null=False)
    password = serializers.CharField(allow_null=False)


# Dashboard Remarks List
class CustomerDashboardRemarksSerializer(serializers.ModelSerializer):
    created_at = serializers.SerializerMethodField(source="get_created_at")

    class Meta:
        model = ShipmentRemark
        fields = ["id", "remarks", "created_at"]


    def get_created_at(self, obj):
        time = datetime.now()
        created_at = getattr(obj, 'created_at')
        if created_at.day == time.day:
            return str(time.hour - created_at.hour) + " hours ago"
        else:
            if created_at.month == time.month:
                return str(time.day - created_at.day) + " days ago"
            else:
                return str(time.year - created_at.year) + " months ago"

        return created_at


# customer profile
class CustomerProfileSerializer(serializers.ModelSerializer):
    email = serializers.SerializerMethodField()
    address = serializers.StringRelatedField()
    city = serializers.StringRelatedField()
    profile_image = serializers.SerializerMethodField()
    created_at = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")
    district = serializers.SerializerMethodField()

    class Meta:
        model = LogisticsCustomer
        fields = ["id", "full_name", "contact_number", "alternative_contact_number", "email", "address", "city",
         "district", "profile_image", "created_at", "document1", "document2", "document3"]

    def get_email(self, obj):
        return obj.user.username

    def get_profile_image(self, obj):
        if obj.profile_image:
            return obj.profile_image.url
        else:
            return "No profile_image"

    def get_district(self, obj):
        return obj.city.district.name



#customer profile update
class CustomerProfileUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = LogisticsCustomer
        fields = ["profile_image", "full_name", "contact_number", "alternative_contact_number", "city", "address"]


# customer forgot password
class CustomerForgotPasswordSerializer(serializers.Serializer):
    email = serializers.EmailField(allow_null=False)

    class Meta:
        fields = ["email"]

    def validate_email(self, value):
        print(value)
        if not LogisticsCustomer.objects.filter(user__email=value).exists():
            raise serializers.ValidationError(
                "email_does_not_exist")
        return value


# customer password reset
class CustomerResetPasswordSerializer(serializers.Serializer):
    new_password1 = serializers.CharField(allow_null=False)
    new_password2 = serializers.CharField(allow_null=False)

    class Meta:
        fields = ["new_password1", "new_password2"]

    def validate(self, data):
        new_password1 = data.get("new_password1")
        new_password2 = data.get("new_password2")
        if new_password1 != new_password2:
            raise serializers.ValidationError(
                "New Password did not match!"
            )
        return data


# Customer Change password
class CustomerChangePasswordSerializer(serializers.Serializer):
    email = serializers.EmailField(allow_null=False)
    password = serializers.CharField(allow_null=False)
    confirm_password = serializers.CharField(allow_null=False)

    class Meta:
        fields = ["email", "password", "confirm_password"]

    def validate(self, data):
        password = data.get("password")
        confirm_password = data.get("confirm_password")
        if password != confirm_password:
            raise serializers.ValidationError(
                "Password did not match!"
            )
        return data



# customer shipment list
class CustomerShipmentListSerializer(serializers.ModelSerializer):
    created_at = serializers.SerializerMethodField(source="get_created_at")
    receiver_city = serializers.StringRelatedField()
    class Meta:
        model = Shipment
        fields = ["id", "tracking_code", "reference_code", "receiver_name", "receiver_address", "receiver_city",
            "receiver_contact_number", "receiver_alt_contact_number", "cod_value", "created_at"]

    def get_created_at(self, obj):
        return obj.created_at.date()


# customer shipment detail
class CustomerShipmentDetailSerializer(serializers.ModelSerializer):
    created_at = serializers.DateTimeField(format="%Y-%m-%d %H:%M")
    class Meta:
        model = Shipment
        fields = ["id", "receiver_name", "receiver_address", "receiver_city", "receiver_contact_number", "cod_info", 
            "shipping_charge", "cod_handling_charge", "rejection_handling_charge", "service_payment_status", "shipment_origin",
            "tracking_code", "created_at", "reference_code", "parcel_type", "weight", "weight_unit", "length", "breadth",
            "height", "length_unit"]

# customer remarks create
class CustomerRemarksSerializer(serializers.ModelSerializer):
    class Meta:
        model = ShipmentRemark
        fields = ["remarks"]



# customer city list
class CustomerCityListSerializer(serializers.ModelSerializer):
    district = serializers.SerializerMethodField(source="get_district")
    class Meta:
        model = City
        fields = ["id", "name", "district"]

    def get_district(self, obj):
        return obj.district.name

# class CustomerAjaxGetSZSSerializer(serializers.ModelSerializer):
#     # shipping_zone = serializers.SerializerMethodField(source="get_shipping_zone")
#     shipping_zone = serializers.StringRelatedField()
#     class Meta:
#         model = ShippingZoneDeliveryType
#         fields = ["id", "shipping_zone", "shipping_charge_per_kg"]

#     def get_shipping_zone(self, obj):
#         return obj.shipping_zone.logistics_branch


# customer charge (zone)
class CustomerChargeSerializer(serializers.ModelSerializer):
    logistics_branch = serializers.StringRelatedField()
    shipping_charge_per_kg = serializers.SerializerMethodField()

    class Meta:
        model = LogisticsBranchShippingZone
        fields = ['id', 'logistics_branch', 'shipping_charge_per_kg']

    def get_shipping_charge_per_kg(self, obj):
        shipping_charge = obj.shippingzonedeliverytype_set.first().shipping_charge_per_kg
        return shipping_charge

# customer place new shipment
class CustomerShipmentCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shipment
        fields = ["receiver_name", "receiver_contact_number", "receiver_alt_contact_number", "receiver_address", "cod_value", "receiver_city", "parcel_type"]


# customer bulk shipment
# class CustomerBulkShipmentBranchListSerializer(serializers.ModelSerializer):
#     logistics_company = serializers.StringRelatedField()
#     branch_city = serializers.SerializerMethodField(source="get_branch_city")
#     class Meta:
#         model = LogisticsBranch
#         fields = ["branch_city", "logistics_company"]

#     def get_branch_city(self, obj):
#         return obj.branch_city.name



# customer payment request
class CustomerRequestPaymentListSerializer(serializers.ModelSerializer):
    logistics_branch = serializers.StringRelatedField()
    preferred_receiving_account = serializers.StringRelatedField()
    requested_date = serializers.SerializerMethodField(source="get_requested_date")
    is_settled = serializers.SerializerMethodField(source="get_is_settled")

    class Meta:
        model = CustomerPaymentRequest
        fields = ["id", "logistics_branch", "request_amount", "preferred_receiving_account", "requested_date", "is_settled"]

    def get_is_settled(self, obj):
        if obj.is_settled == True:
            return "Settled Already"
        else:
            return "Not Settled"

    def get_requested_date(self, obj):
        return obj.created_at.date()



# customer payment list
class CustomerPaymentListSerializer(serializers.ModelSerializer):
    logistics_branch = serializers.SerializerMethodField(source="get_logistics_branch")
    net_paid_or_received = serializers.SerializerMethodField(source="get_net_paid_or_received")
    payment_request = serializers.StringRelatedField()
    payment_method = serializers.StringRelatedField()
    created_at = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")


    class Meta:
        model = CustomerPayment
        fields = ["id", "payment_request", "payment_method", "logistics_branch", "net_paid_or_received", "created_at"]  

    def get_logistics_branch(self, obj):
        return obj.logistics_branch.logistics_company.company_name

    def get_net_paid_or_received(self, obj):
        return obj.net_paid_or_received


#customer payment(nested)
class CustomerPaymentSerializer(serializers.ModelSerializer):
    service_charge = serializers.SerializerMethodField(source="get_service_charge")
    net_amount = serializers.SerializerMethodField(source="get_net_amount")
    class Meta:
        model = Shipment
        fields = ["id", "tracking_code", "shipment_status", "cod_value", "service_charge", "net_amount"]
    
    def get_service_charge(self, obj):
        return obj.get_service_charge[0]

    def get_net_amount(self, obj):
        return obj.get_service_charge[1]


# customer payment detail(nested serializer)
class CustomerPaymentDetailSerializer(serializers.ModelSerializer):
    shipments = CustomerPaymentSerializer(many=True, read_only=True)
    logistics_branch = serializers.StringRelatedField()
    payment_date = serializers.SerializerMethodField(source="get_payment_date")
    payment_request = serializers.SerializerMethodField(source="get_payment_request")
    payment_method = serializers.StringRelatedField()
    slip = serializers.SerializerMethodField(source="get_slip")
    class Meta:
        model = CustomerPayment
        fields = ["id", "logistics_branch", "payment_date", "payment_request", "payment_method", "net_paid_or_received",
            "slip", "shipments"]

    def get_slip(self, obj):
        if obj.slip:
            return obj.slip.url
        else:
            return "No Slip"

    def get_payment_request(self, obj):
        return obj.payment_request.created_at.date()

    def get_payment_date(self, obj):
        return obj.created_at.date()


# service provider list serializer for payment req
class CustomerPaymentRequestServiceProviderSerializer(serializers.ModelSerializer):
    logistics_company = serializers.StringRelatedField()
    branch_city = serializers.SerializerMethodField(source="get_branch_city")
    class Meta:
        model = LogisticsBranch
        fields = ["id", "logistics_company", "branch_city"]

    def get_branch_city(self, obj):
        return obj.branch_city.name


# Receving account list serializer for payment req
class CustomerPaymentRequestReceivingAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerPaymentAccount
        fields = ["id", "account_in", "account_name", "account_number"]

# current balance serializer for payment req
class CustomerPaymentRequestCurrentBalanceSerializer(serializers.ModelSerializer):
    logistics_company = serializers.StringRelatedField()
    branch_city = serializers.StringRelatedField()
    class Meta:
        model = LogisticsBranch
        fields = ["id", "logistics_company", "branch_city"]


# payment request create
class CustomerPaymentRequestCreateSerializer(serializers.ModelSerializer):
    # logistics_branch = serializers.StringRelatedField()
    # preferred_receiving_account = serializers.StringRelatedField()
    class Meta:
        model = CustomerPaymentRequest
        fields = ["logistics_branch", "message", "preferred_receiving_account"]
