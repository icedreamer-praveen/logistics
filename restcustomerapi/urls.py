from django.urls import path
from .views import *
from rest_framework_simplejwt.views import TokenRefreshView


app_name = "restcustomerapi"
urlpatterns = [
    path("login/", CustomerLoginAPIView.as_view(), name="customerloginapi"),
    path('refresh-token/', TokenRefreshView.as_view(), name='operatorrefreshtoken'),
    path("dashboard-shipment-overview/", CustomerDashbaordShipmentOverviewAPIView.as_view(), name="customerdashboardshipmentoverviewapi"),
    path("dashboard-payment-overview/", CustomerDashboardPaymentOverviewAPIView.as_view(), name="customerdashboardpaymentoverviewapi"),
    path("dashboard-remarks-list/", CustomerDashboardRemarksListAPIView.as_view(), name="customerdashboardremarkslistapi"),
    path("dashboard/new-orders-per-day/", CustomerDashboardNewOrdersAPIView.as_view(), name="customerdashboardnewordersapi"),


    path("profile/", CustomerProfileAPIView.as_view(), name="customerprofileapi"),
    path("profile-update/", CustomerProfileUpdateAPIView.as_view(), name="customerprofileupdateapi"),

    path("password-forgot/", CustomerForgotPasswordAPIView.as_view(), name="customerforgotpasswordapi"),
    path("reset-password/<uidb64>/<token>/", CustomerResetPasswordAPIView.as_view(), name="customerresetpasswrodapi"),
    path("password-change/", CustomerPasswordChangeAPIView.as_view(), name="customerpasswordchangeapi"),
    
    path("shipment-list/", CustomerShipmentListAPIView.as_view(), name="customershipmentlistapi"),
    path("shipment-<tracking_code>/detail/", CustomerShipmentDetailAPIView.as_view(), name="customershipmentdetailapi"),

    path("remarks-<int:pk>/create/", CustomerRemarksCreateAPIView.as_view(), name="customerremarkscreateapi"),
    path("remarks-<int:pk>/list/", CustomerRemarksListAPIView.as_view(), name="customerremarkslistapi"),
    
    path("shipment/city-list/", CustomerShipmentCityListAPIView.as_view(), name="customershipmentcitylistapi"),
    path("shipment-<int:pk>/shipment-zones/", CustomerShipmentZonesAPIView.as_view(), name="customershipmentzonesapi"),
    path("place-new-shipment/", CustomerPlaceNewShipmentCreateAPIView.as_view(), name="customernewshipmentcreateapi"),


    # path("bulk-shipment-branch-list/", CustomerBulkShipmentBranchListAPIView.as_view(), name="customerbulkshipmentbranchlistapi"),

    path("request-payment-list/", CustomerRequestPaymentListAPIView.as_view(), name="customerrequestpaymentlistapi"),
    path("payment-list/", CustomerPaymentListAPIView.as_view(), name="customerpaymentlistapi"),
    path("payment-request-<int:pk>/detail/", CustomerPaymentDetailAPIView.as_view(), name="customerpaymentdetailapi"),

    # # payment request create
    # path("payment-request-service-provider/list/", CustomerPaymentRequestServiceProviderAPIView.as_view(), name="customerpaymentrequestserviceproviderapi"),
    # path("payment-request-create/", CustomerPaymentRequestCreateAPIView.as_view(), name="customerpaymentrequestcreateapi"),

    # service provider list for payment req create
    path("payment-req-service-provider-list/", CustomerPaymentRequestServiceProviderListAPIView.as_view(), name="customerpaymentreqserviceproviderapi"),
    path("payment-req-receiving-account-list/", CustomerPaymentRequestReceivingAccountListAPIView.as_view(), name="customerpaymentrequestreceivingaccountapi"),
    path("payment-request-current-balance-<int:pk>/", CustomerPaymentRequestCurrentBalanceAPIView.as_view(), name="customerpaymentrequestcurrentbalanceapi"),
    path("payment-request-create/", CustomerPaymentRequestCreateAPIView.as_view(), name="customerpaymentrequestcreateapi"),



]