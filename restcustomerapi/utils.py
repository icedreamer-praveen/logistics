from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework import permissions
from django.core.mail import EmailMessage



def get_tokens_for_user(user):
    refresh = RefreshToken.for_user(user)

    return {
        'refresh': str(refresh),
        'access': str(refresh.access_token),
        'email': user.username,
    }


class CustomerOnlyPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        user = request.user
        try:
            customer = user.logisticscustomer
            if customer.status == "Active":
                has_perm = True
            else:
                has_perm = False
        except Exception as e:
            print(e)
            has_perm = False
        return has_perm