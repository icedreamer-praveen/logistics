from accapp.models import TimeStamp, LogisticsCompany, LogisticsBranch, BranchOperator, LogisticsCustomer, City
from accapp.constants import TICKET_STATUS, CTICKET_FOLLOWUP_BY, LTICKET_FOLLOWUP_BY
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models


class DeliveryType(TimeStamp):
    logistics_branch = models.ForeignKey(
        LogisticsBranch, on_delete=models.CASCADE)
    delivery_type = models.CharField(max_length=200)
    image = models.ImageField(
        upload_to="deliverytypes/", null=True, blank=True)

    def __str__(self):
        return self.delivery_type + "(" + self.logistics_branch.branch_city.name + " - " + self.logistics_branch.logistics_company.company_name + ")"


class LogisticsBranchShippingZone(TimeStamp):
    logistics_branch = models.ForeignKey(
        LogisticsBranch, on_delete=models.CASCADE)
    shipping_zone = models.CharField(max_length=200)
    cities = models.ManyToManyField(City)
    can_also_pickup = models.BooleanField(default=False)

    def __str__(self):
        return self.shipping_zone + "(" + self.logistics_branch.branch_city.name + " - " + self.logistics_branch.logistics_company.company_name + ")"


class ShippingZoneDeliveryType(TimeStamp):
    shipping_zone = models.ForeignKey(
        LogisticsBranchShippingZone, on_delete=models.CASCADE)
    delivery_type = models.ForeignKey(DeliveryType, on_delete=models.RESTRICT)
    shipping_charge_per_kg = models.IntegerField(default=0)
    additional_shipping_charge_per_kg = models.IntegerField(default=0)
    cod_handling_charge = models.IntegerField(default=0)
    rejection_handling_charge = models.IntegerField(default=0)

    def __str__(self):
        return self.delivery_type.delivery_type + "(" + self.shipping_zone.logistics_branch.branch_city.name + " - " + self.shipping_zone.logistics_branch.logistics_company.company_name + ")"


class LogisticsCustomerContract(TimeStamp):
    first_party = models.ForeignKey(
        LogisticsBranch, on_delete=models.RESTRICT)
    second_party = models.ForeignKey(
        LogisticsCustomer, on_delete=models.RESTRICT)
    expires_at = models.DateField(null=True, blank=True)
    is_signed = models.BooleanField(default=False)
    signed_date = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.first_party.logistics_company.company_name + ", " +self.first_party.branch_city.name + " - " + self.second_party.full_name


class LogisticCustomerZonalContract(TimeStamp):
    logistics_customer_contract = models.ForeignKey(
        LogisticsCustomerContract, on_delete=models.RESTRICT)
    service_provider = models.ForeignKey(
        LogisticsBranch, on_delete=models.RESTRICT)
    shipping_zone = models.ForeignKey(
        LogisticsBranchShippingZone, on_delete=models.RESTRICT)
    shipping_charge = models.IntegerField(default=0)
    cod_handling_charge_pct = models.IntegerField(default=0)
    rejection_handling_charge_pct = models.IntegerField(default=0)
    is_confirmed = models.BooleanField(default=False)

    def __str__(self):
        return self.shipping_zone.shipping_zone


class LogisticsBranchContract(TimeStamp):
    first_party = models.ForeignKey(
        LogisticsBranch, on_delete=models.RESTRICT, related_name="contractedfirstparties")
    second_party = models.ForeignKey(
        LogisticsBranch, on_delete=models.RESTRICT, related_name="contractedsecondparties")
    first_party_referral_percentage = models.PositiveIntegerField(default=0)
    second_party_referral_percentage = models.PositiveIntegerField(default=0)
    expires_at = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.first_party.logistics_company.company_name + " - " + self.second_party.logistics_company.company_name

    @property
    def active_zonal_contracts(self):
        active_contracts = self.logisticsbranchzonalcontract_set.filter(status="Active", is_confirmed=True).count()
        return active_contracts


class LogisticsBranchZonalContract(TimeStamp):
    logistics_branch_contract = models.ForeignKey(
        LogisticsBranchContract, on_delete=models.RESTRICT)
    service_taker = models.ForeignKey(
        LogisticsBranch, on_delete=models.RESTRICT, related_name="service_taker_lbzcs")
    service_provider = models.ForeignKey(
        LogisticsBranch, on_delete=models.RESTRICT, related_name="service_provider_lbzcs")
    shipping_zone = models.ForeignKey(
        LogisticsBranchShippingZone, on_delete=models.RESTRICT)
    shipping_charge = models.IntegerField(default=0)
    cod_handling_charge_pct = models.IntegerField(default=0)
    rejection_handling_charge_pct = models.IntegerField(default=0)
    is_quoted = models.BooleanField(default=False)
    quoted_date = models.DateTimeField(null=True, blank=True)
    is_confirmed = models.BooleanField(default=False)
    confirmed_date = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.shipping_zone.shipping_zone


class BranchPaymentAccount(TimeStamp):
    logistics_branch = models.ForeignKey(LogisticsBranch, on_delete=models.CASCADE)
    account_in = models.CharField(max_length=200)
    account_name = models.CharField(max_length=250)
    account_number = models.CharField(max_length=200)
    branch = models.CharField(max_length=50, null=True, blank=True)
    is_preferred_option = models.BooleanField(default=False)

    def __str__(self):
        return self.account_in + "(" + str(self.logistics_branch) + ")"


class CustomerPaymentAccount(TimeStamp):
    customer = models.ForeignKey(LogisticsCustomer, on_delete=models.CASCADE)
    account_in = models.CharField(max_length=200)
    account_name = models.CharField(max_length=250)
    account_number = models.CharField(max_length=200)
    branch = models.CharField(max_length=50, null=True, blank=True)
    is_preferred_option = models.BooleanField(default=False)

    def __str__(self):
        return self.account_in + " - " + self.account_number + "(" + self.account_name + ")"


class CustomerSupportTicket(TimeStamp):
    ticket_status = models.CharField(max_length=50, choices=TICKET_STATUS)
    raised_by = models.ForeignKey(LogisticsCustomer, on_delete=models.RESTRICT)
    raised_to = models.ForeignKey(LogisticsBranch, on_delete=models.RESTRICT)
    issue_detail = models.TextField()
    related_file = models.FileField(
        upload_to="issue_file", null=True, blank=True)

    def __str__(self):
        return self.issue_detail


class CustomerTicketFollowup(TimeStamp):
    ticket = models.ForeignKey(
        CustomerSupportTicket, on_delete=models.RESTRICT)
    followup_by_type = models.ForeignKey(
        ContentType, on_delete=models.CASCADE, limit_choices_to=CTICKET_FOLLOWUP_BY)
    followup_by_id = models.PositiveIntegerField()
    followup_by = GenericForeignKey('followup_by_type', 'followup_by_id')
    followup_message = models.TextField()

    def __str__(self):
        return self.ticket.issue_detail


class LogisticsSupportTicket(TimeStamp):
    ticket_status = models.CharField(max_length=50, choices=TICKET_STATUS)
    raised_by = models.ForeignKey(LogisticsBranch, on_delete=models.RESTRICT)
    issue_detail = models.TextField()
    related_file = models.FileField(
        upload_to="issue_file", null=True, blank=True)
    created_by = models.ForeignKey(BranchOperator, on_delete=models.RESTRICT)

    def __str__(self):
        return self.issue_detail


class LogisticsTicketFollowup(TimeStamp):
    ticket = models.ForeignKey(
        CustomerSupportTicket, on_delete=models.RESTRICT)
    followup_by_type = models.ForeignKey(
        ContentType, on_delete=models.CASCADE, limit_choices_to=LTICKET_FOLLOWUP_BY)
    followup_by_id = models.PositiveIntegerField()
    followup_by = GenericForeignKey('followup_by_type', 'followup_by_id')
    followup_message = models.TextField()

    def __str__(self):
        return self.ticket.issue_detail


class LogisticsMessage(TimeStamp):
    logistics_company = models.ForeignKey(
        LogisticsCompany, on_delete=models.CASCADE)
    sender = models.CharField(max_length=200)
    mobile = models.CharField(max_length=20)
    email = models.EmailField(null=True, blank=True)
    message = models.TextField()

    def __str__(self):
        return self.sender


class LogisticsNotice(TimeStamp):
    logistics_company = models.ForeignKey(
        LogisticsCompany, on_delete=models.CASCADE)
    logistics_branch = models.ForeignKey(
        LogisticsBranch, on_delete=models.SET_NULL, null=True, blank=True)
    title = models.CharField(max_length=200)
    notice = models.TextField()
    excluded_customers = models.ManyToManyField(LogisticsCustomer, blank=True)
    created_by = models.ForeignKey(BranchOperator, on_delete=models.RESTRICT)
    show_on_top = models.BooleanField(default=False, null=True, blank=True)

    def __str__(self):
        return str(self.created_by)


class BranchStickyNote(TimeStamp):
    logistics_branch = models.ForeignKey(
        LogisticsBranch, on_delete=models.CASCADE)
    operator = models.ForeignKey(
        BranchOperator, on_delete=models.SET_NULL, null=True, blank=True)
    note = models.TextField()

    def __str__(self):
        return self.note


class CustomerStickyNote(TimeStamp):
    customer = models.ForeignKey(
        LogisticsCustomer, on_delete=models.CASCADE)
    note = models.TextField()

    def __str__(self):
        return self.note
