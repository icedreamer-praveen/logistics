from django.apps import AppConfig


class lbcappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'lbcapp'
