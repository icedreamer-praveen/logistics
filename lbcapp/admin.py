from django.contrib import admin
from .models import *

admin.site.register([DeliveryType, LogisticsBranchShippingZone, ShippingZoneDeliveryType, LogisticsCustomerContract, LogisticCustomerZonalContract, LogisticsBranchContract, LogisticsBranchZonalContract, BranchPaymentAccount, CustomerPaymentAccount, CustomerSupportTicket, CustomerTicketFollowup, LogisticsSupportTicket, LogisticsTicketFollowup, LogisticsMessage, LogisticsNotice,BranchStickyNote, CustomerStickyNote])