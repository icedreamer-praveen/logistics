from accapp.models import LogisticsCustomer
from django import forms
from .models import *


# customer forms --------------------------------------------------------------
class CustomerTicketForm(forms.ModelForm):
    class Meta:
        model = CustomerSupportTicket
        fields = ['raised_to', "issue_detail", "related_file"]
        widgets = {
            'raised_to': forms.Select(attrs={"class": "form-control select2"}),
            'issue_detail': forms.Textarea(attrs={"class": "form-control", "placeholder": "your issue detail"}),
            'related_file': forms.ClearableFileInput(attrs={"class": "form-control"}),
        }

    
    def __init__(self, customer_id=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        customer = LogisticsCustomer.objects.get(id=customer_id)
        self.fields["raised_to"].queryset = customer.connected_logistics.all()


# branch forms --------------------------------------------------------------

class ShippingZoneForm(forms.ModelForm):
    class Meta:
        model  = LogisticsBranchShippingZone
        fields = ['shipping_zone']
        widgets = {
            "shipping_zone": forms.TextInput(attrs={"class": "form-control", "placeholder": "Shipping zone name..."}),
        }


class ShippingZoneDeliveryTypeForm(forms.ModelForm):
    class Meta:
        model  =ShippingZoneDeliveryType
        fields = ["shipping_charge_per_kg", "additional_shipping_charge_per_kg", "cod_handling_charge", "rejection_handling_charge"]
        widgets = {
            "shipping_charge_per_kg": forms.NumberInput(attrs={"class": "form-control"}),
            "additional_shipping_charge_per_kg": forms.NumberInput(attrs={"class": "form-control"}),
            "cod_handling_charge": forms.NumberInput(attrs={"class": "form-control"}),
            "rejection_handling_charge": forms.NumberInput(attrs={"class": "form-control"}),
        }


class LogisticsNoticeCreateForm(forms.ModelForm):
    class Meta:
        model = LogisticsNotice
        fields = ["title", "notice"]
        widgets = {
            "title": forms.TextInput(attrs={"class": "form-control", "placeholder": "Notice title..."}),
            "notice": forms.Textarea(attrs={"class": "form-control", "placeholder": "Notice description..."})
        }


class LogisticsQuotationForm(forms.ModelForm):
    class Meta:
        model = LogisticsBranchZonalContract
        fields = ["shipping_charge", "cod_handling_charge_pct", "rejection_handling_charge_pct"]
        widgets = {
            "shipping_charge": forms.NumberInput(attrs={"class": "form-control", "placeholder": "Shipping Charge..."}),
            "cod_handling_charge_pct": forms.NumberInput(attrs={"class": "form-control", "placeholder": "COD Handling Charge  in percentage..."}),
            "rejection_handling_charge_pct": forms.NumberInput(attrs={"class": "form-control", "placeholder": "Rejection Handling Charge  in percentage..."}),
        }
