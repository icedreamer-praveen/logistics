from accapp.views import CustomerRequiredMixin, LogisticsRequiredMixin
from django.views.generic import View, TemplateView, ListView
from django.shortcuts import render, redirect
from django.http.response import JsonResponse
from django.db.models import Q, Sum, Count
from accapp.models import District, City
from django.contrib import messages
from django.utils import timezone
from .models import *
from .forms import *


# customer views  ----------------------------------------------------------

# customer payment account information
class CustomerPaymentAccountListView(CustomerRequiredMixin, ListView):
    template_name = "customertemplates/profile/customerpaymentaccountlist.html"
    context_object_name = "paymentaccounts"

    def get_queryset(self):
        return CustomerPaymentAccount.objects.filter(customer=self.customer)

    def post(self, request, *args, **kwargs):
        pk = request.POST.get("pk")
        account_in = request.POST.get("account_in")
        account_name = request.POST.get("account_name")
        account_number = request.POST.get("account_number")
        branch = request.POST.get("branch")
        if pk:
            if CustomerPaymentAccount.objects.filter(customer=self.customer, id=pk).exists():
                cpa = CustomerPaymentAccount.objects.get(id=pk)
                cpa.account_in = account_in
                cpa.account_name = account_name
                cpa.account_number = account_number
                cpa.branch = branch
                cpa.save()
                messages.success(request, "Account updated successfully.")
            else:
                messages.error(
                    request, "Ops something went wrong, please try again in a while or report us.")
                return redirect("lbcapp:customerpaymentaccountlist")
        else:
            CustomerPaymentAccount.objects.get_or_create(
                status="Active", customer=self.customer, account_in=account_in, account_name=account_name, account_number=account_number, branch=branch)
            messages.success(request, "Account created successfully.")
        return redirect("lbcapp:customerpaymentaccountlist")


class CustomerPaymentAccountUpdateView(CustomerRequiredMixin, TemplateView):
    template_name = "customertemplates/profile/customerpaymentaccountupdate.html"


class CustomerPaymentAccountDeleteView(CustomerRequiredMixin, TemplateView):
    template_name = "customertemplates/profile/customerpaymentaccountdelete.html"


# settings and connections
class CustomerMarketplaceView(CustomerRequiredMixin, TemplateView):
    template_name = "customertemplates/connection/customermarketplace.html"

    def get(self, request):
        if request.is_ajax():
            keyword = request.GET.get("lb_keyword")
            related_branches = LogisticsBranch.objects.filter(
                branch_coverage=self.customer.city)
            searched_branches = related_branches.filter(
                Q(logistics_company__company_name__icontains=keyword) | Q(branch_city__name__icontains=keyword) | Q(branch_coverage__name__icontains=keyword) | Q(logisticsbranchshippingzone__cities__name__icontains=keyword)).distinct()
            return render(request, "customertemplates/connection/customermarketplacesearch.html", {"searched_branches": searched_branches[:50]})
        else:
            return super().get(request)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["logistics_branch_list"] = LogisticsBranch.objects.filter(
            branch_coverage=self.customer.city)[:50]
        return context


class CustomerLogisticsBranchDetailView(CustomerRequiredMixin, TemplateView):
    template_name = "customertemplates/connection/customerlogisticsbranchdetail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        branch = LogisticsBranch.objects.get(id=self.kwargs.get("branch_id"))
        context["logistics_branch"] = branch
        context["branch_szs"] = branch.logisticsbranchshippingzone_set.filter(
            status="Active")
        return context

    def post(self, request, *args, **kwargs):
        branch = LogisticsBranch.objects.get(id=self.kwargs.get("branch_id"))
        if branch in self.customer.connected_logistics.all():
            resp = {"status": "failure"}
        else:
            self.customer.connected_logistics.add(branch)
            if self.customer.logisticscustomercontract_set.filter(first_party=branch).exists() is False:
                LogisticsCustomerContract.objects.create(
                    first_party=branch, second_party=self.customer, is_signed=False, status="Pending")
            resp = {"status": "success"}
        return JsonResponse(resp)


class CustomerConnectedLogisticsListView(CustomerRequiredMixin, TemplateView):
    template_name = "customertemplates/connection/customerconnectedlogisticslist.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["connected_logistics"] = self.customer.connected_logistics.all()
        return context


class CustomerConnectedLogisticsDetailView(CustomerRequiredMixin, TemplateView):
    template_name = "customertemplates/connection/customerconnectedlogisticsdetail.html"

    def get(self, request, *args, **kwargs):
        try:
            branch_id = self.kwargs.get("branch_id")
            self.logistics_branch = LogisticsBranch.objects.get(id=branch_id)
            if self.logistics_branch in self.customer.connected_logistics.all():
                pass
            else:
                messages.error(request, "Not your service provider.")
                return redirect("lbcapp:customerconnectedlogisticslist")
        except Exception as e:
            messages.error(request, "Service provider does not exist.")
            return redirect("lbcapp:customerconnectedlogisticslist")
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        branch = LogisticsBranch.objects.get(id=self.kwargs.get("branch_id"))
        context["connected_branch"] = branch
        context["branch_szs"] = branch.logisticsbranchshippingzone_set.filter(
            status="Active")
        return context


# notices and support ticket

class CustomerNoticeListView(CustomerRequiredMixin, ListView):
    template_name = "customertemplates/extra/customernoticelist.html"
    context_object_name = "notice_list"
    paginate_by = 25

    def get_queryset(self):
        queryset = LogisticsNotice.objects.filter(
            logistics_branch__in=self.customer.connected_logistics.all()).distinct().order_by("-id")
        return queryset


class CustomerSupportTicketListView(CustomerRequiredMixin, ListView):
    template_name = "customertemplates/extra/customersupportticketlist.html"
    context_object_name = "ticket_list"
    paginate_by = 25

    def get_queryset(self):
        queryset = CustomerSupportTicket.objects.filter(
            raised_by=self.customer)
        self.open_ticket_list = queryset.filter(ticket_status="Open")
        self.reviewing_ticket_list = queryset.filter(ticket_status="Reviewing")
        self.solved_ticket_list = queryset.filter(ticket_status="Solved")
        self.status = "all"
        if "search" in self.request.GET:
            search = self.request.GET.get("search", '')
            queryset = queryset.filter(Q(issue_detail__icontains=search))
        elif "status" in self.request.GET:
            status = self.request.GET.get("status")
            if status == "open":
                queryset = self.open_ticket_list
                self.status = "open"
            elif status == "reviewing":
                queryset = self.reviewing_ticket_list
                self.status = "reviewing"
            elif status == "solved":
                queryset = self.solved_ticket_list
                self.status = "solved"
            else:
                self.status = "all"
                queryset = queryset

        return queryset.order_by("-id")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["open_ticket_list"] = self.open_ticket_list
        context["reviewing_ticket_list"] = self.reviewing_ticket_list
        context["solved_ticket_list"] = self.solved_ticket_list
        context["status"] = self.status
        return context


class CustomerSupportTicketDetailView(CustomerRequiredMixin, TemplateView):
    template_name = "customertemplates/extra/customersupportticketdetail.html"

    def get(self, request, *args, **kwargs):
        try:
            self.ticket = CustomerSupportTicket.objects.get(
                id=self.kwargs.get("pk"), raised_by=self.customer)
        except Exception as e:
            messages.error(request, "Service provider does not exist.")
            return redirect("lbcapp:customersupportticketlist")
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["ticket"] = self.ticket
        return context

    def post(self, request, *args, **kwargs):
        try:
            ticket = CustomerSupportTicket.objects.get(
                id=self.kwargs.get("pk"), raised_by=self.customer)
            followup_message = request.POST.get("followup_message")
            CustomerTicketFollowup.objects.create(
                status="Active", ticket=ticket, followup_message=followup_message, followup_by=self.customer)
        except Exception as e:
            messages.error(request, "Ticket does not exist. ")
            return redirect("lbcapp:customersupportticketlist")
        return redirect("lbcapp:customersupportticketdetail", pk=ticket.id)


class CustomerSupportTicketCreateView(CustomerRequiredMixin, TemplateView):
    template_name = "customertemplates/extra/customersupportticketcreate.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["ticket_form"] = CustomerTicketForm(
            customer_id=self.customer.id)
        return context

    def post(self, request, *args, **kwargs):
        ticket_form = CustomerTicketForm(
            self.customer.id, request.POST, request.FILES)
        if ticket_form.is_valid():
            ticket_form.instance.status = "Active"
            ticket_form.instance.raised_by = self.customer
            ticket_form.instance.ticket_status = "Open"
            ticket_form.save()
            messages.success(request, "Ticket raised successfully.")
        else:
            return render(request, self.template_name, {"ticket_form": ticket_form})
        return redirect("lbcapp:customersupportticketlist")


# logistics branch views --------------------------------------------------------------

# shipping zone management
# shipping zone list, delivery type create and update
class LogisticsShippingZoneListView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/setting/logisticshippingzonelist.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["shipping_zones"] = LogisticsBranchShippingZone.objects.filter(
            logistics_branch=self.branch)
        context["delivery_types"] = DeliveryType.objects.filter(
            logistics_branch=self.branch)
        return context

    def post(self, request, *args, **kwargs):
        delivery_type_name = request.POST.get("delivery_type")
        if delivery_type_name in ["Standard", "standard", "Standard Delivery", "standard delivery"]:
            messages.error(request, "You can't change the name standard. ")
        else:
            pk = request.POST.get("lbdt_pk")
            delivery_type = request.POST.get("delivery_type")
            if pk:
                try:
                    lbdt = DeliveryType.objects.get(
                        logistics_branch=self.branch, id=pk)
                    lbdt.delivery_type = delivery_type
                    lbdt.save()
                    messages.success(
                        request, "Delivery type updated successfully.")
                except Exception as e:
                    print(e)
                    messages.error(request, "Invalid requests.")
            else:
                DeliveryType.objects.create(
                    logistics_branch=self.branch, delivery_type=delivery_type, status="Active")
                messages.success(
                    request, "Delivery type created successfully.")
        return redirect("lbcapp:logisticsshippingzonelist")


#  shipping zone detail, add and remove city from shipping zone .
class LogisticsShippingZoneDetailView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/setting/logisticsshippingzonedetail.html"

    def get(self, request, *args, **kwargs):
        try:
            sz_id = self.kwargs.get("pk")
            self.sz = LogisticsBranchShippingZone.objects.get(
                id=sz_id, logistics_branch=self.branch)
        except Exception as e:
            messages.error(request, "Shipping zone does not exist.")
            return redirect("lbcapp:logisticsshippingzonelist")
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["shipping_zone"] = self.sz
        context["unset_delivery_types"] = DeliveryType.objects.filter(
            logistics_branch=self.branch).exclude(shippingzonedeliverytype__shipping_zone=self.sz)
        context["districts"] = District.objects.filter(status="Active")
        return context

    def post(self, request, *args, **kwargs):
        try:
            sz_id = self.kwargs.get("pk")
            sz = LogisticsBranchShippingZone.objects.get(
                id=sz_id, logistics_branch=self.branch)
            district_id = request.POST.get("district_id")
            city_id = request.POST.get("city_id")
            action = request.POST.get("action")
            if district_id:
                try:
                    district = District.objects.get(id=district_id)
                    cities = City.objects.filter(district=district)
                    if action == "all_add":
                        sz.cities.add(*cities)
                    elif action == "all_remove":
                        sz.cities.remove(*cities)
                    else:
                        return JsonResponse({"status": "failure", "message": "Invalid request."})
                except Exception as e:
                    return JsonResponse({"status": "failure", "message": "District not found."})
            else:
                try:
                    city = City.objects.get(id=city_id)
                    if action == "add":
                        if LogisticsBranchShippingZone.objects.filter(logistics_branch=self.branch, cities=city).exists():
                            return JsonResponse({"status": "failure", "message": "City already in one of your shipping zone."})
                        else:
                            sz.cities.add(city)
                    elif action == "remove":
                        sz.cities.remove(city)
                    else:
                        return JsonResponse({"status": "failure", "message": "Invalid request."})
                except Exception as e:
                    return JsonResponse({"status": "failure", "message": "City not found."})

        except Exception as e:
            return JsonResponse({"status": "failure", "message": "Shipping zone not found."})
        return JsonResponse({"status": "success"})


# shipping zone create
class LogisticsShippingZoneCreateView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/setting/logisticshippingzonecreate.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["shipping_zone_form"] = ShippingZoneForm
        context["szdt_form"] = ShippingZoneDeliveryTypeForm
        context["shipping_zones"] = LogisticsBranchShippingZone.objects.filter(
            logistics_branch=self.branch)
        return context

    def post(self, request, *args, **kwargs):
        shipping_zone_form = ShippingZoneForm(request.POST)
        if shipping_zone_form.is_valid():
            sz_name = shipping_zone_form.cleaned_data.get("shipping_zone")
            if LogisticsBranchShippingZone.objects.filter(logistics_branch=self.branch, shipping_zone__iexact=sz_name).exists():
                messages.error(
                    request, "Please use the different name for shipping zone.")
                return redirect("lbcapp:logisticsshippingzonecreate")
            else:
                shipping_zone_form.instance.logistics_branch = self.branch
                shipping_zone_form.instance.status = "Active"
                sz = shipping_zone_form.save()
                szdt_form = ShippingZoneDeliveryTypeForm(request.POST)
                if szdt_form.is_valid():
                    szdt_form.instance.delivery_type, created = DeliveryType.objects.get_or_create(
                        delivery_type="Standard Delivery", logistics_branch=self.branch)
                    szdt_form.instance.shipping_zone = sz
                    szdt_form.instance.status = "Active"
                    szdt_form.save()
                messages.success(
                    request, "Shipping Zone created successfully.")
                return redirect("lbcapp:logisticsshippingzonedetail", pk=sz.pk)

        else:
            return redirect("lbcapp:logisticsshippingzonelist")

# update shipping zone


class LogisticsShippingZoneUpdateView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/setting/logisticshippingzonecreate.html"

    def get(self, request, *args, **kwargs):
        try:
            sz_id = self.kwargs.get("pk")
            self.sz = LogisticsBranchShippingZone.objects.get(
                id=sz_id, logistics_branch=self.branch)
        except Exception as e:
            messages.error(request, "Shipping zone does not exist.")
            return redirect("lbcapp:logisticsshippingzonelist")
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["shipping_zone_form"] = ShippingZoneForm(instance=self.sz)
        context["shipping_zones"] = LogisticsBranchShippingZone.objects.filter(
            logistics_branch=self.branch)
        return context

    def post(self, request, *args, **kwargs):
        try:
            sz_id = self.kwargs.get("pk")
            self.sz = LogisticsBranchShippingZone.objects.get(
                id=sz_id, logistics_branch=self.branch)
            sz = LogisticsBranchShippingZone.objects.get(id=sz_id)
            shipping_zone_form = ShippingZoneForm(request.POST, instance=sz)
            if shipping_zone_form.is_valid():
                sz = shipping_zone_form.save()
                return redirect("lbcapp:logisticsshippingzonedetail", pk=sz.pk)
            else:
                messages.error(request, "Ops! Something went wrong. ")
                return redirect("lbcapp:logisticsshippingzonelist")
        except Exception as e:
            messages.error(request, "Shipping zone does not exist.")
            return redirect("lbcapp:logisticsshippingzonelist")


class LogisticsSZDTCreateView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/setting/logisticsszdtcreate.html"

    def get(self, request, *args, **kwargs):
        try:
            dt_id = self.kwargs.get("dt_id")
            sz_id = self.kwargs.get("sz_id")
            self.dt = DeliveryType.objects.get(
                id=dt_id, logistics_branch=self.branch)
            self.sz = LogisticsBranchShippingZone.objects.get(
                id=sz_id, logistics_branch=self.branch)
            if ShippingZoneDeliveryType.objects.filter(shipping_zone=self.sz, delivery_type=self.dt).exists():
                messages.error(
                    request, "Charge for this has already been setup.")
                return redirect("lbcapp:logisticsshippingzonelist")
        except Exception as e:
            print(e)
            messages.error(request, "Shipping zone does not exist.")
            return redirect("lbcapp:logisticsshippingzonelist")
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["szdt_form"] = ShippingZoneDeliveryTypeForm
        context["delivery_type"] = self.dt.delivery_type
        context["shipping_zone"] = self.sz
        context["other_szdts"] = ShippingZoneDeliveryType.objects.filter(
            shipping_zone=self.sz)
        return context

    def post(self, request, *args, **kwargs):
        try:
            sz_id = self.kwargs.get("sz_id")
            dt_id = self.kwargs.get("dt_id")
            self.sz = LogisticsBranchShippingZone.objects.get(
                id=sz_id, logistics_branch=self.branch)
            self.dt = DeliveryType.objects.get(
                id=dt_id, logistics_branch=self.branch)
            if ShippingZoneDeliveryType.objects.filter(shipping_zone=self.sz, delivery_type=self.dt).exists():
                messages.error(
                    request, "Charge for this has already been setup.")
                return redirect("lbcapp:logisticsshippingzonelist")
            else:
                szdt_form = ShippingZoneDeliveryTypeForm(request.POST)
                if szdt_form.is_valid():
                    szdt_form.instance.delivery_type = self.dt
                    szdt_form.instance.shipping_zone = self.sz
                    szdt_form.instance.status = "Active"
                    szdt_form.save()
                    messages.success(request, "Charges set successfully.")
                else:
                    messages.error(
                        request, "Ops! something went wrong. Try again in a while.")
                    return redirect("lbcapp:logisticsszdtcreate", sz_id=sz_id, dt_id=dt_id)
        except Exception as e:
            print(e)
            messages.error(request, "Shipping zone does not exist.")
            return redirect("lbcapp:logisticsshippingzonelist")
        return redirect("lbcapp:logisticsshippingzonedetail", pk=sz_id)


class LogisticsSZDTUpdateView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/setting/logisticsszdtcreate.html"

    def get(self, request, *args, **kwargs):
        szdt_id = self.kwargs.get("pk")
        try:
            self.szdt = ShippingZoneDeliveryType.objects.get(
                id=szdt_id, delivery_type__logistics_branch=self.branch, shipping_zone__logistics_branch=self.branch)
        except Exception as e:
            print(e)
            messages.error(request, "Delivery Type package does not exists")
            return redirect("lbcapp:logisticsshippingzonelist")
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["szdt_form"] = ShippingZoneDeliveryTypeForm(instance=self.szdt)
        context["delivery_type"] = self.szdt.delivery_type.delivery_type
        context["shipping_zone"] = self.szdt.shipping_zone
        context["other_szdts"] = ShippingZoneDeliveryType.objects.filter(
            shipping_zone=self.szdt.shipping_zone)
        return context

    def post(self, request, *args, **kwargs):
        szdt_id = self.kwargs.get("pk")
        try:
            szdt = ShippingZoneDeliveryType.objects.get(
                id=szdt_id, delivery_type__logistics_branch=self.branch, shipping_zone__logistics_branch=self.branch)
            szdt_form = ShippingZoneDeliveryTypeForm(
                request.POST, instance=szdt)
            if szdt_form.is_valid():
                szdt_form.save()
                messages.success(request, "Charge updated successfully.")
            else:
                messages.error(
                    request, "Ops! something went wrong. Try again in a while.")
                return redirect("lbcapp:logisticsszdtupdate", pk=szdt_id)
        except Exception as e:
            print(e)
            messages.error(request, "Delivery Type package does not exists")
            return redirect("lbcapp:logisticsshippingzonelist")
        return redirect("lbcapp:logisticsshippingzonedetail", pk=szdt.shipping_zone.id)


class LogisticsNoticeListView(LogisticsRequiredMixin, ListView):
    template_name = "logisticstemplates/extra/logisticsnoticelist.html"
    paginate_by = 25
    context_object_name = "notice_list"

    def get_queryset(self):
        queryset = LogisticsNotice.objects.filter(logistics_branch=self.branch)
        if "search" in self.request.GET:
            search = self.request.GET.get("search", "")
            queryset = queryset.filter(title__icontains=search)
        return queryset.order_by("-id")


class LogisticsNoticeCreateView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/extra/logisticsnoticecreate.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["notice_form"] = LogisticsNoticeCreateForm
        return context

    def post(self, request, *args, **kwargs):
        notice_form = LogisticsNoticeCreateForm(request.POST)
        if notice_form.is_valid():
            notice_form.instance.status = "Active"
            notice_form.instance.logistics_company = self.branch.logistics_company
            notice_form.instance.logistics_branch = self.branch
            notice_form.instance.created_by = self.operator
            notice_form.save()

            # notification background task
            
        else:
            context = super().get_context_data(**kwargs)
            context["notice_form"] = notice_form
            return render(request, self.template_name, context)
        return redirect("lbcapp:logisticsnoticelist")


class LogisticsNoticeUpdateView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/extra/logisticsnoticecreate.html"

    def get(self, request, *args, **kwargs):
        try:
            self.notice = LogisticsNotice.objects.get(
                id=self.kwargs.get("pk"), logistics_branch=self.branch)
        except Exception as e:
            print(e)
            messages.error(request, "Notice not found...")
            return redirect("lbcapp:logisticsnoticelist")
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["notice_form"] = LogisticsNoticeCreateForm(
            instance=self.notice)
        return context

    def post(self, request, *args, **kwargs):
        try:
            notice = LogisticsNotice.objects.get(
                id=self.kwargs.get("pk"), logistics_branch=self.branch)
            notice_form = LogisticsNoticeCreateForm(
                request.POST, instance=notice)
            if notice_form.is_valid():
                notice_form.save()
                messages.success(request, "Notice updated successfully.")
        except Exception as e:
            print(e)
            messages.error(request, "Notice not found...")
            return redirect("lbcapp:logisticsnoticelist")
        return redirect("lbcapp:logisticsnoticelist")


class LogisticsCustomerTicketListView(LogisticsRequiredMixin, ListView):
    template_name = "logisticstemplates/extra/logisticscustomerticketlist.html"
    paginate_by = 25
    context_object_name = "ticket_list"

    def get_queryset(self):
        queryset = CustomerSupportTicket.objects.filter(raised_to=self.branch)
        self.open_ticket_list = queryset.filter(ticket_status="Open")
        self.reviewing_ticket_list = queryset.filter(ticket_status="Reviewing")
        self.solved_ticket_list = queryset.filter(ticket_status="Solved")
        self.all_ticket_list = queryset
        self.status = "all"
        if "search" in self.request.GET:
            search = self.request.GET.get("search", '')
            queryset = queryset.filter(Q(issue_detail__icontains=search))
            self.status = "all"
        elif "status" in self.request.GET:
            status = self.request.GET.get("status")
            if status == "open":
                queryset = self.open_ticket_list
                self.status = "open"
            elif status == "reviewing":
                queryset = self.reviewing_ticket_list
                self.status = "reviewing"
            elif status == "solved":
                queryset = self.solved_ticket_list
                self.status = "solved"
            else:
                self.status = "all"
                queryset = queryset
        return queryset.order_by("-id")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["open_ticket_list"] = self.open_ticket_list
        context["reviewing_ticket_list"] = self.reviewing_ticket_list
        context["solved_ticket_list"] = self.solved_ticket_list
        context["all_ticket_list"] = self.all_ticket_list
        context["status"] = self.status
        return context


class LogisticsCustomerTicketDetailView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/extra/logisticscustomerticketdetail.html"

    def get(self, request, *args, **kwargs):
        try:
            self.ticket = CustomerSupportTicket.objects.get(
                id=self.kwargs.get("pk"), raised_to=self.branch)
        except Exception as e:
            messages.error(request, "Support Ticket does not exist.")
            return redirect("lbcapp:logisticscustomerticketlist")
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["ticket"] = self.ticket
        return context

    def post(self, request, *args, **kwargs):
        try:
            ticket = CustomerSupportTicket.objects.get(
                id=self.kwargs.get("pk"), raised_to=self.branch)
            followup_message = request.POST.get("followup_message")
            CustomerTicketFollowup.objects.create(
                status="Active", ticket=ticket, followup_message=followup_message, followup_by=self.branch)
        except Exception as e:
            messages.error(request, "Ticket does not exist. ")
            return redirect("lbcapp:logisticscustomerticketlist")
        return redirect("lbcapp:logisticscustomerticketdetail", pk=ticket.id)


class LogisticsCustomerTicketChangeView(LogisticsRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        try:
            ticket = CustomerSupportTicket.objects.get(
                id=self.kwargs.get("pk"), raised_to=self.branch)
            action = request.POST.get("action")
            if action == "reviewing":
                ticket.ticket_status = "Reviewing"
            elif action == "solved":
                ticket.ticket_status = "Solved"
            ticket.save()

        except Exception as e:
            return JsonResponse({"status": "failure", "message": "Ticket not found"})
        return JsonResponse({"status": "success", "message": "Ticket status changed successfully."})


class LogisticsMarketplaceView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/connection/logisticsmarketplace.html"

    def get(self, request):
        if request.is_ajax():
            keyword = request.GET.get("lb_keyword")
            related_branches = LogisticsBranch.objects.exclude(
                logistics_company=self.company)
            searched_branches = related_branches.filter(
                Q(logistics_company__company_name__icontains=keyword) | Q(branch_city__name__icontains=keyword) | Q(branch_coverage__name__icontains=keyword) | Q(logisticsbranchshippingzone__cities__name__icontains=keyword)).distinct()
            return render(request, "logisticstemplates/connection/logisticsmarketplacesearch.html", {"searched_branches": searched_branches[:50]})
        else:
            return super().get(request)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["logistics_branch_list"] = LogisticsBranch.objects.exclude(logistics_company=self.company)[:50]
        return context


class LogisticsBranchDetailView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/connection/logisticsbranchdetail.html"

    def get(self, request, *args, **kwargs):
        try:
            self.logistics_branch = LogisticsBranch.objects.get(
                id=self.kwargs.get("branch_id"))
        except Exception as e:
            messages.error(request, "Logistics not found.")
            return redirect("lbcapp:logisticsmarketplace")

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["logistics_branch"] = self.logistics_branch
        context["branch_szs"] = self.logistics_branch.logisticsbranchshippingzone_set.filter(
            status="Active")
        return context

    def post(self, request, *args, **kwargs):
        try:
            branch = LogisticsBranch.objects.get(
                id=self.kwargs.get("branch_id"))
            zone_id = request.POST.get("zone_id")
            shipping_zone = LogisticsBranchShippingZone.objects.get(
                id=zone_id, logistics_branch=branch)

            if LogisticsBranchContract.objects.filter(
                    Q(first_party=self.branch, second_party=branch) | Q(first_party=branch, second_party=self.branch)).exists():
                lbc = LogisticsBranchContract.objects.filter(Q(first_party=self.branch, second_party=branch) | Q(
                    first_party=branch, second_party=self.branch)).last()
            else:
                lbc = LogisticsBranchContract.objects.create(
                    status="Active", first_party=self.branch, second_party=branch)
            lbzcs = LogisticsBranchZonalContract.objects.filter(
                logistics_branch_contract=lbc, shipping_zone=shipping_zone, service_taker=self.branch, service_provider=branch)
            if lbzcs.filter(status__in=["Active", "Pending"], is_confirmed__in=[False, None]).exists():
                return JsonResponse({"status": "failure", "message": "You have already requested quotation before."})
            elif lbzcs.filter(status="Active", is_confirmed=True).exists():
                return JsonResponse({"status": "failure", "message": "You have already signed contract for this zone."})
            else:
                LogisticsBranchZonalContract.objects.create(
                    logistics_branch_contract=lbc, shipping_zone=shipping_zone, service_taker=self.branch, service_provider=branch, is_confirmed=False, status="Pending")
        except Exception as e:
            print("-------------", e)
            messages.error(request, "Ops! Something went wrong.")
            return JsonResponse({"status": "failure", "message": "Ops! Something went wrong."})
        return JsonResponse({"status": "success"})


class LogisticsQuotationListView(LogisticsRequiredMixin, ListView):
    template_name = "logisticstemplates/connection/logisticsquotationlist.html"
    context_object_name = "quotation_list"
    paginate_by = 25

    def get_queryset(self):
        quotation_type = self.request.GET.get("qrt", "rcvd")
        lbzcs = LogisticsBranchZonalContract.objects.filter(
            Q(service_taker=self.branch) | Q(service_provider=self.branch))
        if quotation_type == "snt":
            queryset = lbzcs.filter(
                service_taker=self.branch, is_confirmed=False, status__in=["Pending", "Active"])
            self.quotation_type = "sent"
        else:
            queryset = lbzcs.filter(
                service_provider=self.branch, status__in=["Pending", "Active"], is_confirmed=False)
            self.quotation_type = "received"
        return queryset.order_by("-id")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        lbzcs = LogisticsBranchZonalContract.objects.filter(
            Q(service_taker=self.branch) | Q(service_provider=self.branch)).distinct()
        context["total_sent_quotations"] = LogisticsBranchZonalContract.objects.filter(
            service_taker=self.branch)
        context["total_received_quotations"] = LogisticsBranchZonalContract.objects.filter(
            service_provider=self.branch)
        context["active_sent_quotations"] = LogisticsBranchZonalContract.objects.filter(
            service_taker=self.branch, status__in=["Pending", "Active"], is_confirmed=False)
        context["active_received_quotations"] = LogisticsBranchZonalContract.objects.filter(
            service_provider=self.branch, status__in=["Pending", "Active"], is_confirmed=False)
        context["type"] = self.quotation_type
        return context


class LogisticsQuotationDetailView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/connection/logisticsquotationdetail.html"

    def get(self, request, *args, **kwargs):
        try:
            self.quotation = LogisticsBranchZonalContract.objects.get(Q(service_taker=self.branch) | Q(
                service_provider=self.branch), id=self.kwargs.get("pk"))
        except Exception as e:
            print("------", e)
            messages.error(request, "Quotation not found.")
            return redirect("lbcapp:logisticsquotationlist")
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["quotation"] = self.quotation
        if self.quotation.service_provider == self.branch:
            context["logistics_branch"] = self.quotation.service_taker
            context["quotation_form"] = LogisticsQuotationForm(
                instance=self.quotation)
        else:
            context["logistics_branch"] = self.quotation.service_provider
        return context

    def post(self, request, *args, **kwargs):
        try:
            action = request.POST.get("action")
            if action == "accept-and-sign" and request.is_ajax():
                quotation = LogisticsBranchZonalContract.objects.get(
                    service_taker=self.branch, id=self.kwargs.get("pk"), is_quoted=True)
                if quotation.status in ["Pending", "Active"] and quotation.is_confirmed is not True:
                    quotation.is_confirmed = True
                    quotation.confirmed_date = timezone.localtime(
                        timezone.now())
                    quotation.save()
                    messages.success(request, "Contract signed successfully.")
                    return JsonResponse({"status": "success"})
                else:
                    messages.error(request, "Ops! Something went wrong.")
                    return JsonResponse({"status": "failure"})
            else:
                quotation = LogisticsBranchZonalContract.objects.get(
                    service_provider=self.branch, id=self.kwargs.get("pk"))
                quotation_form = LogisticsQuotationForm(
                    request.POST, instance=quotation)
                if quotation_form.is_valid():
                    if quotation.is_quoted and quotation.is_confirmed is not True and quotation.status != "Disabled":
                        quotation_form.instance.status = "Active"
                        quotation_form.save()
                        messages.success(
                            request, "Quotation updated and sent successfully.")
                    elif quotation.is_quoted is not True and quotation.status != "Disabled":
                        quotation_form.instance.status = "Active"
                        quotation_form.save()
                        quotation.is_quoted = True
                        quotation.quoted_date = timezone.localtime(
                            timezone.now())
                        quotation.save()
                        messages.success(
                            request, "Quotation sent successfully.")
                    else:
                        messages.error(request, "Invalid action taken.")
                else:
                    messages.error(request, "Ops! Something went wrong.")
        except Exception as e:
            print("------", e)
            messages.error(request, "Quotation not found.")
            return redirect("lbcapp:logisticsquotationlist")
        return redirect("lbcapp:logisticsquotationdetail", pk=quotation.id)


class LogisticsConnectedBranchListView(LogisticsRequiredMixin, ListView):
    template_name = "logisticstemplates/connection/logisticsconnectedbranchlist.html"
    context_object_name = "contract_list"
    paginate_by = 25

    def get_queryset(self):
        queryset = LogisticsBranchContract.objects.filter(
            Q(first_party=self.branch) | Q(second_party=self.branch))
        return queryset.order_by("-id")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["provider_contract_list"] = LogisticsBranchZonalContract.objects.filter(
            service_provider=self.branch, is_confirmed=True, status="Active")
        context["taker_contract_list"] = LogisticsBranchZonalContract.objects.filter(
            service_taker=self.branch, is_confirmed=True, status="Active")
        context["covered_cities"] = City.objects.filter(logisticsbranchshippingzone__logistics_branch=self.branch)
        return context


class LogisticsConnectedBranchDetailView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/connection/logisticsconnectedbranchdetail.html"

    def get(self, request, *args, **kwargs):
        try:
            self.logistics_contract = LogisticsBranchContract.objects.get(
                Q(first_party=self.branch) | Q(second_party=self.branch), id=self.kwargs.get("contract_id"))
        except Exception as e:
            print(e)
            messages.error(request, "Contract not found...")
            return redirect("lbcapp:logisticsconnectedbranchlist")
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["logistics_contract"] = self.logistics_contract
        context["provider_zonal_contracts"] = self.logistics_contract.logisticsbranchzonalcontract_set.filter(
            is_confirmed=True, status="Active", service_provider=self.branch)
        context["taker_zonal_contracts"] = self.logistics_contract.logisticsbranchzonalcontract_set.filter(
            is_confirmed=True, status="Active", service_taker=self.branch)
        if self.logistics_contract.first_party != self.branch:
            context["logistics_branch"] = self.logistics_contract.first_party
        else:
            context["logistics_branch"] = self.logistics_contract.second_party
        return context
