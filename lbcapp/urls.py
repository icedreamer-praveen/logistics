from django.urls import path
from .views import *

app_name = "lbcapp"
urlpatterns = [
    # customer urls ------------------------------------------------------
    # payment receiving urls
    path("customer/payment-accounts/", CustomerPaymentAccountListView.as_view(),
         name="customerpaymentaccountlist"),
    # connections
    path("customer/marketplace/", CustomerMarketplaceView.as_view(),
         name="customermarketplace"),
    path("customer/logistics-branch-<branch_id>/", CustomerLogisticsBranchDetailView.as_view(),
         name="customerlogisticsbranchdetail"),
    path("customer/connected-logistics/", CustomerConnectedLogisticsListView.as_view(),
         name="customerconnectedlogisticslist"),
    path("customer/connected-logistics-<branch_id>/", CustomerConnectedLogisticsDetailView.as_view(),
         name="customerconnectedlogisticsdetail"),

    # notices and support ticket
    path("customer/notices/", CustomerNoticeListView.as_view(),
         name="customernoticelist"),
    path("customer/support-ticekts/", CustomerSupportTicketListView.as_view(),
         name="customersupportticketlist"),
    path("customer/support-ticekts-<int:pk>/", CustomerSupportTicketDetailView.as_view(),
         name="customersupportticketdetail"),
    path("customer/support-ticekts-create/", CustomerSupportTicketCreateView.as_view(),
         name="customersupportticketcreate"),

    # branch urls ----------------------------------------------------------

    # shipping zone
    path("logistics/shipping-zones/", LogisticsShippingZoneListView.as_view(),
         name="logisticsshippingzonelist"),
    path("logistics/shipping-zones-<int:pk>/", LogisticsShippingZoneDetailView.as_view(),
         name="logisticsshippingzonedetail"),
    path("logistics/shipping-zones-create/", LogisticsShippingZoneCreateView.as_view(),
         name="logisticsshippingzonecreate"),
    path("logistics/shipping-zones-<int:pk>-update/", LogisticsShippingZoneUpdateView.as_view(),
         name="logisticsshippingzoneupdate"),
    path("logistics/sz-<sz_id>-dt-<dt_id>-szdt-create/", LogisticsSZDTCreateView.as_view(),
         name="logisticsszdtcreate"),
    path("logistics/szdt-<int:pk>-update/", LogisticsSZDTUpdateView.as_view(),
         name="logisticsszdtupdate"),

    # notices and support ticket
    path("logistics/notice-list/", LogisticsNoticeListView.as_view(),
         name="logisticsnoticelist"),
    path("logistics/notice-create/", LogisticsNoticeCreateView.as_view(),
         name="logisticsnoticecreate"),
    path("logistics/notice-<int:pk>-update/", LogisticsNoticeUpdateView.as_view(),
         name="logisticsnoticeupdate"),

    path("logistics/customer-ticket-list/", LogisticsCustomerTicketListView.as_view(),
         name="logisticscustomerticketlist"),
    path("logistics/customer-ticket-<int:pk>-detail/", LogisticsCustomerTicketDetailView.as_view(),
         name="logisticscustomerticketdetail"),
    path("logistics/customer-ticket-<int:pk>-change/", LogisticsCustomerTicketChangeView.as_view(),
         name="logisticscustomerticketchange"),

    # marketplace
    path("logistics/marketplace/", LogisticsMarketplaceView.as_view(),
         name="logisticsmarketplace"),
    path("logistics/logistics-branch-<branch_id>/", LogisticsBranchDetailView.as_view(),
         name="logisticsbranchdetail"),
    path("logistics/quotation-list/", LogisticsQuotationListView.as_view(),
         name="logisticsquotationlist"),
    path("logistics/quotation-<int:pk>-detail/",
         LogisticsQuotationDetailView.as_view(), name="logisticsquotationdetail"),
    path("logistics/connected-logistics/", LogisticsConnectedBranchListView.as_view(),
         name="logisticsconnectedbranchlist"),
    path("logistics/connected-logistics-<contract_id>/", LogisticsConnectedBranchDetailView.as_view(),
         name="logisticsconnectedbranchdetail"),


]
