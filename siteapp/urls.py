from django.urls import path
from .views import *

app_name = "siteapp"

urlpatterns = [

    # client urls ------------------------------------------------------------
    path("", ClientHomeView.as_view(), name="clienthome"),
    path("contact/", ClientContactView.as_view(), name="clientcontact"),
    path("page/<slug:slug>/", ClientPageView.as_view(), name="clientpage"),
    path("services/<slug:slug>/", ClientServiceDetailView.as_view(), name="clientservicedetail"),
    path("track-order/", ClientTrackOrderView.as_view(), name="clienttrackorder"),
    path("become-a-rider/", ClientBecomeRiderView.as_view(), name="clientbecomerider"),
    path("delivery-charges/", ClientChargeView.as_view(), name="clientcharges"),


    # system operator urls ------------------------------------------------------
    path("initial-setup/", InitialSetupView.as_view(), name="initialsetup"),

]
