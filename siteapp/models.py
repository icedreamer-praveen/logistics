from django.db import models
from accapp.models import TimeStamp
# Create your models here.

class LogisticsOrganizationalInformation(TimeStamp):
    name = models.CharField(max_length=200)
    logo = models.ImageField(upload_to="organization")
    profile_image = models.ImageField(
        upload_to="organization", null=True, blank=True)
    vat_pan = models.CharField(max_length=200, null=True, blank=True)
    address = models.CharField(max_length=500)
    slogan = models.CharField(max_length=500, null=True, blank=True)
    contact_no = models.CharField(max_length=200)
    alt_contact_no = models.CharField(max_length=200, null=True, blank=True)
    map_location = models.CharField(max_length=200, null=True, blank=True)
    email = models.EmailField()
    alt_email = models.EmailField(null=True, blank=True)
    about_us = models.TextField()
    facebook = models.CharField(max_length=200, null=True, blank=True)
    instagram = models.CharField(max_length=200, null=True, blank=True)
    youtube = models.CharField(max_length=200, null=True, blank=True)
    whatsapp = models.CharField(max_length=200, null=True, blank=True)
    viber = models.CharField(max_length=200, null=True, blank=True)
    support_policy = models.TextField()
    privacy_policy = models.TextField()
    terms_conditions = models.TextField()
    career = models.TextField()
    video_url = models.URLField(null=True, blank=True)
    meta_description = models.CharField(max_length=256, null=True, blank=True)
    fb_messenger_script = models.CharField(
        max_length=1024, null=True, blank=True)
    google_analytics_script = models.CharField(
        max_length=500, null=True, blank=True)
    fb_pixel_script = models.CharField(max_length=4000, null=True, blank=True)
    detail_pixel = models.TextField(null=True, blank=True)
    # Announcement from logistic admin
    banner_display = models.BooleanField(default=False, null=True, blank=True)
    banner_image = models.ImageField(
        upload_to="organization", null=True, blank=True)
    banner_text = models.TextField(max_length=300, null=True, blank=True)
    enable_marquee = models.BooleanField(default=False, null=True, blank=True)

    def __str__(self):
        return self.name

class LogisticsSlider(TimeStamp):
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to="sliders")
    content = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return self.title


class LogisticsContactMessage(TimeStamp):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    contact = models.CharField(max_length=200)
    email = models.EmailField(max_length=200)
    subject = models.CharField(max_length=200)
    message = models.TextField(max_length=200)

    def __str__(self):
        return self.name


class LogisticsQuote(TimeStamp):
    name = models.CharField(max_length=200)
    current_location = models.CharField(max_length=200)
    destination = models.CharField(max_length=200)
    contact = models.CharField(max_length=200)
    email = models.EmailField(max_length=200)
    message = models.TextField(max_length=200)

    def __str__(self):
        return self.name


class LogisticsService(TimeStamp):
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to="services", null=True, blank=True)
    content = models.TextField(null=True, blank=True)
    slug = models.SlugField(max_length=1024, blank=True, null=True)
    display_position = models.PositiveSmallIntegerField(unique=True)

    def __str__(self):
        return self.title


class ClientLogisticsBranch(TimeStamp):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=500, null=True, blank=True)
    contact = models.CharField(max_length=100)
    city = models.TextField()

    def __str__(self):
        return self.name

class LogisticsTagLine(TimeStamp):
    title = models.CharField(max_length=200)

    def __str__(self):
        return self.title


LOGISTIC_NOTICE_TYPE = (
    ('info', 'Info'),
    ('success', 'Success'),
    ('warning', 'Warning'),
    ('danger', 'Danger'),
)

class ClientLogisticsNotice(TimeStamp):
    title = models.CharField(max_length=1024)
    content = models.TextField(null=True, blank=True)
    notice_type = models.CharField(
        max_length=100, choices=LOGISTIC_NOTICE_TYPE)

    def __str__(self):
        return self.title

class LogisticsTestimonial(TimeStamp):
    name = models.CharField(max_length=1024)
    content = models.TextField(null=True, blank=True)
    logo = models.ImageField(upload_to="testimonials", null=True, blank=True)

    def __str__(self):
        return self.name


class LogisticsFaq(TimeStamp):
    question = models.CharField(max_length=1024)
    answer = models.TextField()

    def __str__(self):
        return self.question


class LogisticsPage(TimeStamp):
    title = models.CharField(max_length=1024)
    slug = models.SlugField(max_length=1024, blank=True, null=True)
    content = models.TextField()
    display_position = models.PositiveSmallIntegerField(unique=True)

    def __str__(self):
        return self.title

RIDER_TYPE = (
                ('Full Time Rider', 'Full Time Rider'),
                ('Part Time Rider', 'Part Time Rider'),
            )

VEHICLE_TYPE = (
                ('Bike/Scooty', 'Bike/Scooty'),
                ('Bicycle', 'Bicycle'),
                ('Others', 'Others')
            )
class RiderApplication(TimeStamp):
    name = models.CharField(max_length=1024)
    address = models.CharField(max_length=200)
    contact = models.CharField(max_length=200)
    email = models.EmailField(max_length=200, null=True, blank=True)
    rider_type = models.CharField(max_length=30, choices=RIDER_TYPE, default="Full Time Rider")
    vehicle_type = models.CharField(max_length=30, choices=VEHICLE_TYPE, default="Bike/Scooty")
    message = models.TextField(max_length=200, null=True, blank=True)

    def __str__(self):
        return self.name + ' (' + str(self.contact) + ')'


