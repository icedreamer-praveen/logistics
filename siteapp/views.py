from accapp.models import Country, Province, District, City
from django.shortcuts import render, redirect
from django.utils.text import slugify
from django.views.generic import View, FormView, TemplateView, CreateView, DetailView
from django.http import JsonResponse
from django.contrib import messages
from django.db.models import Q
from .forms import *
from .models import *
import json
from lmsapp.models import Shipment
from accapp.constants import *
from lbcapp.models import LogisticsBranchShippingZone, LogisticsBranch, ShippingZoneDeliveryType

# client views -----------------------------------------------------------------------

class ClientMixin(object):
    
    def get_context_data(self, **kwargs):

        context = super().get_context_data(**kwargs)
        context['org'] = LogisticsOrganizationalInformation.objects.filter(status="Active").first()
        context['services'] = LogisticsService.objects.filter(status="Active").order_by('display_position')
        context['pages'] = LogisticsPage.objects.filter(status="Active").order_by('display_position')

        return context


class ClientHomeView(ClientMixin, TemplateView):
    template_name = "clienttemplates/clienthome.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tags'] = LogisticsTagLine.objects.filter(status="Active")
        context['testimonials'] = LogisticsTestimonial.objects.filter(status="Active")
        context['faqs'] = LogisticsFaq.objects.filter(status="Active")

        return context


class ClientContactView(ClientMixin, CreateView):
    template_name = "clienttemplates/clientcontact.html"
    form_class = LogisticsContactForm
    success_url = '/'
    success_message = "Thankyou for your message to us. We will get back to you in a short period of time."

    def form_invalid(self, form):
        print(form.errors, '++++++++++++')
        return super().form_invalid(form)


class ClientPageView(ClientMixin, TemplateView):
    template_name = "clienttemplates/clientpage.html"

    def dispatch(self, request, *args, **kwargs):
        try:
            page = LogisticsPage.objects.get(slug=self.kwargs['slug'])
        except Exception as e:
            messages.add_message(request, messages.WARNING, "Page Doesn't Exist!!!")
            return redirect("siteapp:clienthome")

        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page'] = LogisticsPage.objects.get(status="Active", slug=self.kwargs.get("slug")) 

        return context


class ClientServiceDetailView(ClientMixin, DetailView):
    template_name = "clienttemplates/clientpage.html"
    model = LogisticsService
    context_object_name = 'page'


class ClientTrackOrderView(ClientMixin, TemplateView):
    template_name = "clienttemplates/clienttrackorder.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = ClientTrackOrder
        return context

    def post(self, request, *args, **kwargs):
        tracking_code = request.POST.get('tracking_code')
        contact = request.POST.get('contact')
        try:
            shipment = Shipment.objects.get(tracking_code=tracking_code, receiver_contact_number=contact)
            if shipment.shipment_status in customer_pending_sl:
                shipment_status = "Pending"
            elif shipment.shipment_status in customer_processing_sl:
                shipment_status = "Processing"
            elif shipment.shipment_status in customer_dispatched__sl: 
                shipment_status = "Dispatched"
            elif shipment.shipment_status in customer_delivered_sl: 
                shipment_status = "Delivered"
            elif shipment.shipment_status in customer_rejected_sl + customer_returned_sl: 
                shipment_status = "Rejected"
            elif shipment.shipment_status in customer_canceled_sl:
                shipment_status = "Canceled"
            else:
                shipment_status = "Unknown"

            shipment_json = {
                'tracking_code': shipment.tracking_code,
                'shipment_status': shipment_status,
                'created_at': shipment.created_at.strftime('%Y-%m-%d'),
                'receiver_name': shipment.receiver_name,
                'receiver_city': shipment.receiver_city.name + "(" + shipment.receiver_city.district.name + ")",
                'receiver_address': shipment.receiver_address,
                'receiver_contact_number': shipment.receiver_contact_number,
                'sender_name': shipment.shipment_origin.logistics_company.company_name,
                'sender_city': shipment.sender_city.name + "(" + shipment.sender_city.district.name + ")",
                'sender_address': shipment.sender_address,
                'sender_contact_number': shipment.shipment_origin.branch_contact,
                'cod_value': shipment.cod_value,
            }
            return JsonResponse({"status": "success", "shipment": shipment_json})
        except Exception as e:
            print('ClientTrackOrderView', e)
            messages.error(request, "Could not find your order with provided information. Please try again with correct inforamtaion")
            return JsonResponse({"status": "error"})


class ClientBecomeRiderView(ClientMixin, CreateView):
    template_name = 'clienttemplates/clienbecomerider.html'
    form_class = ClientBecomeRiderForm
    success_url = '/'
    success_message = "Thankyou for your request to become a rider at Sarathi Logistics. We will get back to you for con."


class ClientChargeView(ClientMixin, TemplateView):
    template_name = "clienttemplates/clientcharges.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['districts'] = District.objects.filter(status="Active").order_by('name')
        # logistics_branch = LogisticsBranch.objects.filter(status="Active").first()
        # lbzc = LogisticsBranchShippingZone.objects.filter(status="Active", logistics_branch=logistics_branch)
        # szdt = ShippingZoneDeliveryType.objects.filter(shipping_zone__in=lbzc)
        # for location in szdt:
            # print(location.shipping_charge_per_kg)
        # szdt = ShippingZoneDeliveryType.objects.filter(shipping_zone__logistics_branch=logistics_branch)
        # print(szdt, '+++')

        return context

   
# system operator  views -----------------------------------------------------------------------

class InitialSetupView(View):
    
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_superuser:
            pass
        else:
            return redirect("/django-admin/")
        return super().dispatch(request, *args, **kwargs)
    
    def get(self, request, *args, **kwargs):
        json_file = open("location.json")
        data = json.load(json_file)
        country, c_created = Country.objects.get_or_create(status="Active", name="Nepal", slug="nepal")
        city_list = []
        for p in data['provinces']:
            province, p_created = Province.objects.get_or_create(status="Active", country=country, name=p["name"], slug=slugify(p["name"]))
            for d in p["districts"]:
                try:
                    district, d_created = District.objects.get_or_create(status="Active", name=d["name"], slug=slugify(d["name"]), province=province)
                except Exception as e:
                    district = District.objects.filter(Q(name=d["name"]) | Q(slug=slugify(d["name"]))).last()
                for c in d["cities"]:
                    try:
                        city, c_created = City.objects.get_or_create(status="Active", name=c, slug=slugify(c), district=district)
                    except Exception as e:
                        city, c_created = City.objects.get_or_create(status="Active", name=c, slug=slugify(c)+ '-' + district.slug, district=district)
            

        json_file.close()
        resp = {
            "status":"ok"
        }
        return JsonResponse({"status": "success"})