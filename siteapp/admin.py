from django.contrib import admin
from .models import *


admin.site.register([LogisticsOrganizationalInformation, LogisticsSlider, LogisticsContactMessage, LogisticsQuote, LogisticsService, ClientLogisticsBranch, ClientLogisticsNotice, LogisticsPage, LogisticsTagLine, LogisticsTestimonial, LogisticsFaq, RiderApplication])