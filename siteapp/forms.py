from django import forms
from .models import *

# client forms -------------------------------------------------------------
class LogisticsContactForm(forms.ModelForm):
    class Meta:
        model = LogisticsContactMessage
        fields = '__all__'
        widgets = {
            'status': forms.Select(attrs={
                'class': '',

            }),
            'name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter Name',

            }),
            'subject': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter Subject',

            }),
            'contact': forms.TextInput(attrs={
                'class': 'form-control',
                'pattern': '[0-9]{6,15}',
                'placeholder': 'Enter contact number',
                'autocomplete': "off"
            }),
            'email': forms.EmailInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter Email Address',
            }),
            'address': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter street address',
            }),
            'message': forms.Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'Your message here',
            }),
        }

class ClientTrackOrder(forms.Form):
    
    tracking_code = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control', 'placeholder': 'Insert tracking number here'
    }))
    contact = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control', 'placeholder': 'Enter receiver contact number'
    }))  

class ClientBecomeRiderForm(forms.ModelForm):
    class Meta:
        model = RiderApplication
        fields = '__all__'
        widgets = {
            'rider_type': forms.Select(attrs={
                'class': 'form-control',
            }),
            'vehicle_type': forms.Select(attrs={
                'class': 'form-control',

            }),
            'name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter your Name',

            }),
            'contact': forms.TextInput(attrs={
                'class': 'form-control',
                'pattern': '[0-9]{6,15}',
                'placeholder': 'Enter contact number',
                'autocomplete': "off"
            }),
            'email': forms.EmailInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter Email Address',
            }),
            'address': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter street address',
            }),
            'message': forms.Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'Your message here',
            }),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['rider_type'].empty_label = "Rider Type"
        self.fields['vehicle_type'].empty_label = 'Vehicle you Own'

# system owner forms -------------------------------------------------------