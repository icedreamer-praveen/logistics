$(".logout").on("click", function (e) {
    e.preventDefault()
    Swal.fire({
        title: "Are you sure to logout?",
        text: "You will be logged out from your panel.",
        type: "warning",
        showCancelButton: !0,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, logout!",
        confirmButtonClass: "btn btn-warning",
        cancelButtonClass: "btn btn-info ml-1",
        buttonsStyling: !1,
    }).then(function (t) {
        if (t.value == 1) {
            Swal.fire({ type: "error", title: "Success!", text: "You are logged out successfully", confirmButtonClass: "btn btn-danger" });
            location.href="/customer/logout/"
        }

    });
})