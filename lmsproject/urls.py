from django.conf.urls.static import static
from django.urls import path, include
from django.contrib import admin
from django.conf import settings

urlpatterns = [
    path('django-admin/', admin.site.urls),
    path('', include("accapp.urls")),
    path('', include("lbcapp.urls")),
    path('', include("lmsapp.urls")),
    path('', include("siteapp.urls")),
    path('api/v1/rider/', include("restriderapi.urls")),
    path('api/v1/customer/', include("restcustomerapi.urls")),
]


urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)