from .models import CustomerPayment, Shipment, CustomerPaymentRequest, ShipmentBundle, LogisticsPaymentRequest, LogisticsTransaction
from accapp.models import City, LogisticsCustomer, LogisticsBranch, DeliveryPerson
from lbcapp.models import CustomerPaymentAccount, BranchPaymentAccount
from accapp.constants import PARCEL_TYPE
from django import forms
from django.contrib.auth.models import User


# client forms ------------------------------------------------------------------

# customer forms ------------------------------------------------------------------
class CustomerLoginForm(forms.Form):
    email = forms.EmailField(widget=forms.TextInput(
        attrs={"class": "form-control"}))
    password = forms.CharField(
        widget=forms.PasswordInput(attrs={"class": "form-control"}))

class CustomerForgotPasswordForm(forms.Form):
    email = forms.EmailField(widget=forms.EmailInput(attrs={
        'class': 'form-control',
        'placeholder': 'Your Email Address',
    }))

    def clean_email(self):
        email = self.cleaned_data.get("email")
        if not LogisticsCustomer.objects.filter(user__email=email).exists():
            raise forms.ValidationError(
                "Customer with this email doesnot exists. \
                Please contact site administrator for your account recovery.")
        return email

class CustomerPasswordResetForm(forms.Form):
    new_password1 = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'autocomplete': 'new-password',
        'placeholder': 'Enter New Password',
    }), label="New Password")
    new_password2 = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'autocomplete': 'new-password',
        'placeholder': 'Confirm New Password',
    }), label="Confirm New Password")

    def clean(self):
        new_password1 = self.cleaned_data.get("new_password1")
        new_password2 = self.cleaned_data.get("new_password2")
        if new_password1 != new_password2:
            raise forms.ValidationError(
                "New Passwords did not match!")

class CustomerSignupForm(forms.ModelForm):
    signup_email = forms.CharField(widget=forms.EmailInput(
        attrs={"class": "form-control", "placeholder": "Enter your email address", "pattern": "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"}))
    signup_password = forms.CharField(
        widget=forms.PasswordInput(attrs={"class": "form-control", "placeholder": "Enter a password", }))
    confirm_terms = forms.BooleanField(
        widget=forms.CheckboxInput(attrs={"title": "Please accept the terms and conditions to proceed", "style": "zoom:1.2;"}), required=True)

    class Meta:
        model = LogisticsCustomer
        fields = ["full_name", "company_name",
                  "contact_number", "city", "address"]
        widgets = {
            "full_name": forms.TextInput(attrs={"class": "form-control", "placeholder": "Your Name"}),
            "company_name": forms.TextInput(attrs={"class": "form-control", "placeholder": "Your Business Name"}),
            "address": forms.TextInput(attrs={"class": "form-control", "placeholder": "Your Business Address"}),
            "contact_number": forms.NumberInput(attrs={"class": "form-control", "placeholder": "Your business contact number"}),
            "city": forms.Select(attrs={"class": "form-control select2"}),
        }

    def clean_signup_email(self):
        signup_email = self.cleaned_data.get("signup_email")
        if User.objects.filter(username=signup_email).exists():
            raise forms.ValidationError(
                "This email is already used, please use different email.")
        return signup_email


    def clean_confirm_terms(self):
        confirm_terms = self.cleaned_data.get("confirm_terms")
        if confirm_terms is False:
            raise forms.ValidationError(
                "Please accept the terms and conditions to register.")
        return confirm_terms

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['city'].empty_label = 'Select your City'


class CustomerShipmentCreateForm(forms.ModelForm):
    parcel_type = forms.ChoiceField(choices=PARCEL_TYPE, widget=forms.Select(
        attrs={"class": "form-control select2"}))

    class Meta:
        model = Shipment
        fields = ["receiver_name", "receiver_city", "receiver_address", "receiver_contact_number", "parcel_type", "cod_value", "reference_code", "length", "breadth", "height", "length_unit", "weight", "weight_unit",
                  ]
        widgets = {
            "reference_code": forms.TextInput(attrs={"class": "form-control", 'placeholder': 'Your reference code'}),
            "length": forms.NumberInput(attrs={"class": "form-control", "value": 20}),
            "breadth": forms.NumberInput(attrs={"class": "form-control", "value": 20}),
            "height": forms.NumberInput(attrs={"class": "form-control", "value": 20}),
            "length_unit": forms.Select(attrs={"class": "form-control"}),
            "weight": forms.NumberInput(attrs={"class": "form-control", "value": 200}),
            "weight_unit": forms.Select(attrs={"class": "form-control"}),
            "receiver_name": forms.TextInput(attrs={"class": "form-control"}),
            "receiver_city": forms.Select(attrs={"class": "form-control select2"}),
            "receiver_address": forms.TextInput(attrs={"class": "form-control"}),
            "receiver_contact_number": forms.NumberInput(attrs={"class": "form-control"}),
            "parcel_type": forms.TextInput(attrs={"class": "form-control"}),
            "cod_value": forms.NumberInput(attrs={"class": "form-control"}),

        }

class LogisticsShipmentCreateForm(forms.ModelForm):
    parcel_type = forms.ChoiceField(choices=PARCEL_TYPE, widget=forms.Select(
        attrs={"class": "form-control select2"}))

    class Meta:
        model = Shipment
        fields = ["receiver_name", "customer", "receiver_city", "receiver_address", "receiver_contact_number", "parcel_type", "cod_value", "reference_code", "length", "breadth", "height", "length_unit", "weight", "weight_unit",
                  ]
        widgets = {
            "reference_code": forms.TextInput(attrs={"class": "form-control", 'placeholder': 'Your reference code'}),
            "length": forms.NumberInput(attrs={"class": "form-control", "value": 20}),
            "breadth": forms.NumberInput(attrs={"class": "form-control", "value": 20}),
            "height": forms.NumberInput(attrs={"class": "form-control", "value": 20}),
            "length_unit": forms.Select(attrs={"class": "form-control"}),
            "weight": forms.NumberInput(attrs={"class": "form-control", "value": 200}),
            "weight_unit": forms.Select(attrs={"class": "form-control"}),
            "receiver_name": forms.TextInput(attrs={"class": "form-control"}),
            "receiver_city": forms.Select(attrs={"class": "form-control select2"}),
            "customer": forms.Select(attrs={"class": "form-control select2"}),
            "receiver_address": forms.TextInput(attrs={"class": "form-control"}),
            "receiver_contact_number": forms.NumberInput(attrs={"class": "form-control"}),
            "parcel_type": forms.TextInput(attrs={"class": "form-control"}),
            "cod_value": forms.NumberInput(attrs={"class": "form-control"}),

        }
    
    def __init__(self, *args, **kwargs):
        super(LogisticsShipmentCreateForm, self).__init__(*args, **kwargs)
        self.fields['customer'].required = True


class CustomerPaymentRequestForm(forms.ModelForm):
    class Meta:
        model = CustomerPaymentRequest
        fields = ["logistics_branch", "message", "preferred_receiving_account"]
        widgets = {
            "logistics_branch": forms.Select(attrs={"class": "form-control select2"}),
            "preferred_receiving_account": forms.Select(attrs={"class": "form-control select2"}),
            "message": forms.Textarea(attrs={"class": "form-control", "rows": 3})
        }

    def __init__(self, customer, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["logistics_branch"].queryset = customer.connected_logistics.all()
        self.fields["preferred_receiving_account"].queryset = customer.customerpaymentaccount_set.all()


# branch forms ------------------------------------------------------------------

class LogisticsLoginForm(forms.Form):
    email = forms.EmailField(widget=forms.TextInput(
        attrs={"class": "form-control"}))
    password = forms.CharField(
        widget=forms.PasswordInput(attrs={"class": "form-control"}))


class LogisticsShipmentUpdateForm(forms.ModelForm):
    class Meta:
        model = Shipment
        fields = ["receiver_name", "receiver_city", "receiver_address", "receiver_contact_number", "cod_value", "shipping_charge"]
        widgets = {
            "receiver_city": forms.Select(attrs={"class": "form-control select2"}),
            "receiver_address": forms.TextInput(attrs={"class": "form-control"}),
            "receiver_contact_number": forms.TextInput(attrs={"class": "form-control"}),
            "receiver_name": forms.TextInput(attrs={"class": "form-control"}),
            "cod_value": forms.NumberInput(attrs={"class": "form-control"}),
            "shipping_charge": forms.NumberInput(attrs={"class": "form-control"}),
        }


class LogisticsBundleCreateForm(forms.ModelForm):
    class Meta:
        model = ShipmentBundle
        fields = ["bundle_code", "destination_branch", "remarks"]
        widgets = {
            "bundle_code": forms.TextInput(attrs={"class": "form-control", "placeholder": "Consolidation Code...", "required": ""}),
            "destination_branch": forms.Select(attrs={"class": "form-control"}),
            "remarks": forms.Textarea(attrs={"class": "form-control", "rows": 3, "placeholder": "If any remarks."}),
        }

    def __init__(self, this_branch, *args, **kwargs):
        super().__init__(*args, **kwargs)
        active_branches = LogisticsBranch.objects.exclude(id=this_branch.id)
        self.fields["destination_branch"].queryset = active_branches


class LogisticsBundleUpdateForm(forms.ModelForm):
    class Meta:
        model = ShipmentBundle
        fields = ["destination_branch"]
        widgets = {
            "destination_branch": forms.Select(attrs={"class": "form-control"}),
        }

    def __init__(self, this_branch, *args, **kwargs):
        super().__init__(*args, **kwargs)
        active_branches = LogisticsBranch.objects.exclude(id=this_branch.id)
        self.fields["destination_branch"].queryset = active_branches


class LogisticsCustomerPaymentForm(forms.ModelForm):
    class Meta:
        model = CustomerPayment
        fields = ["remarks", "payment_method", "slip"]
        widgets = {
            "remarks": forms.Textarea(attrs={"class": "form-control", "rows": 2}),
            "payment_method": forms.Select(attrs={"class": "form-control"}),
            "slip": forms.FileInput(attrs={"class": "form-control"})
        }

    def __init__(self, customer, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["payment_method"].queryset = CustomerPaymentAccount.objects.filter(
            customer=customer)


class CustomerBulkUploadShipmentForm(forms.Form):
    file = forms.FileField(widget=forms.FileInput(attrs={
        'name': 'file',
        'accept': '.xlsx'
    }))
    logistics_branch = forms.ModelChoiceField(queryset=LogisticsBranch.objects.filter(), widget=forms.Select(attrs={
        'class': 'form-control',
    }),  initial='')


class LogisticsPaymentRequestForm(forms.ModelForm):
    class Meta:
        model = LogisticsPaymentRequest
        fields = ["requested_to", "requester_remarks"]
        widgets = {
            "requested_to": forms.Select(attrs={"class": "form-control select2"}),
            "requester_remarks": forms.Textarea(attrs={"class": "form-control", "rows": 3})
        }

    def __init__(self, logistics, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["requested_to"].queryset = LogisticsBranch.objects.all().exclude(
            id=logistics.id)
        self.fields["preferred_receiving_account"].queryset = logistics.branchpaymentaccount_set.filter(
            status='Active')


class LogisticsLogisticsPaymentForm(forms.Form):
    remarks = forms.CharField(
        widget=forms.Textarea(attrs={"class": "form-control"}))


# transaction management

class LogisticsTransactionCreateForm(forms.ModelForm):
    class Meta:
        model = LogisticsTransaction
        fields = ["transaction_date", "transaction_type", "transaction_category",
                  "transaction_amount", "title", "description", "reference_data"]
        widgets = {
            "transaction_date": forms.DateTimeInput(attrs={"class": "form-control", "type": "datetime-local", "placeholder": "YYYY-MM-DD"}),
            "transaction_type": forms.Select(attrs={"class": "form-control"}),
            "transaction_category": forms.Select(attrs={"class": "form-control"}),
            "transaction_amount": forms.NumberInput(attrs={"class": "form-control"}),
            "title": forms.TextInput(attrs={"class": "form-control"}),
            "description": forms.Textarea(attrs={"class": "form-control", "rows": 3}),
            "reference_data": forms.TextInput(attrs={"class": "form-control"}),
        }
