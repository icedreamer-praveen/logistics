from accapp.models import TimeStamp, LogisticsBranch, BranchOperator, LogisticsCustomer, DeliveryPerson, City
from lbcapp.models import ShippingZoneDeliveryType, CustomerPaymentAccount, BranchPaymentAccount
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from accapp.constants import *
from django.db import models


class Shipment(TimeStamp):
    # general info
    shipment_origin = models.ForeignKey(
        LogisticsBranch, on_delete=models.RESTRICT)
    tracking_code = models.CharField(max_length=50, null=True, blank=True)
    reference_code = models.CharField(max_length=200, null=True, blank=True)
    current_logistics = models.ForeignKey(
        LogisticsBranch, on_delete=models.RESTRICT, null=True, blank=True, related_name="inshipments")
    # shipment physical info
    weight = models.DecimalField(
        max_digits=19, decimal_places=2, null=True, blank=True)
    weight_unit = models.CharField(
        max_length=100, choices=WEIGHT_UNIT, default="Gram", null=True, blank=True)
    length = models.DecimalField(
        max_digits=19, decimal_places=2, null=True, blank=True)
    breadth = models.DecimalField(
        max_digits=19, decimal_places=2, null=True, blank=True)
    height = models.DecimalField(
        max_digits=19, decimal_places=2, null=True, blank=True)
    length_unit = models.CharField(
        max_length=100, choices=LENGTH_UNIT, default="Cm", null=True, blank=True)
    parcel_type = models.CharField(
        max_length=200, null=True, blank=True)  # parcel material type

    shipment_group_id = models.CharField(max_length=200, null=True, blank=True)
    shipment_status = models.CharField(
        max_length=500, choices=SHIPMENT_STATUS, null=True, blank=True)
    # sender
    customer = models.ForeignKey(
        LogisticsCustomer, on_delete=models.RESTRICT, null=True, blank=True)
    sender_city = models.ForeignKey(
        City, on_delete=models.RESTRICT, related_name="sentshipments")
    sender_address = models.CharField(max_length=200)
    request_pickup = models.BooleanField(default=True)
    pickup_charge = models.IntegerField(default=0)
    # receiver
    receiver_name = models.CharField(max_length=200)
    receiver_city = models.ForeignKey(
        City, on_delete=models.RESTRICT, related_name="receivedshipments")
    receiver_address = models.CharField(max_length=200)
    receiver_contact_number = models.CharField(max_length=15)
    receiver_alt_contact_number = models.CharField(max_length=15, null=True, blank=True)
    request_home_delivery = models.BooleanField(default=True)
    dropoff_charge = models.IntegerField(default=0)

    # cod and cost info
    delivery_type = models.ForeignKey(
        ShippingZoneDeliveryType, on_delete=models.RESTRICT, null=True, blank=True)
    cod_info = models.CharField(max_length=200, choices=COD_INFO)
    cod_value = models.IntegerField(
        default=0)  # dependent to cod_info
    shipping_charge = models.IntegerField(default=0)
    cod_handling_charge = models.IntegerField(default=0)
    rejection_handling_charge = models.IntegerField(default=0)
    extra_charge = models.IntegerField(default=0)
    exchanged_after_delivered = models.BooleanField(
        default=False, null=True, blank=True)
    is_reverse_logistics = models.BooleanField(
        default=False, null=True, blank=True)

    # payment information
    # prepayment for individual # may be post payment for business
    service_payment_status = models.BooleanField(default=False)
    service_payment_amount = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.tracking_code

    @property
    def get_service_charge(self):
        cod_handling_charge, rejection_handling_charge, cod_value = 0, 0, 0
        if self.shipment_status in customer_failed_charge_sl:
            rejection_handling_charge = self.rejection_handling_charge
        elif self.shipment_status in customer_completed_cod_sl and self.cod_info == "Cash on Delivery":
            cod_handling_charge = 0
            cod_value = self.cod_value

        service_charge = self.shipping_charge + \
            cod_handling_charge + rejection_handling_charge
        net_amount = cod_value - service_charge
        return service_charge, net_amount


class ExchangeRequest(TimeStamp):
    old_shipment = models.OneToOneField(
        Shipment, on_delete=models.CASCADE, related_name="oldexrequest")
    new_shipment = models.OneToOneField(
        Shipment, on_delete=models.CASCADE, related_name="newexrequest")
    cod_value = models.IntegerField(
        default=0, null=True, blank=True)  # dependent to cod_info
    shipping_charge = models.IntegerField(default=0)
    cod_handling_charge = models.IntegerField(default=0)
    rejection_handling_charge = models.IntegerField(default=0)
    service_payment_status = models.BooleanField(default=False)
    service_payment_amount = models.IntegerField(null=True, blank=True)
    remarks = models.TextField(null=True, blank=True)
    is_approved = models.BooleanField(default=False)

    def __str__(self):
        return self.old_shipment.tracking_code


class RefundRequest(TimeStamp):
    shipment = models.OneToOneField(Shipment, on_delete=models.CASCADE)
    shipping_charge = models.IntegerField(default=0)
    cod_handling_charge = models.IntegerField(default=0)
    rejection_handling_charge = models.IntegerField(default=0)
    service_payment_status = models.BooleanField(default=False)
    service_payment_amount = models.IntegerField(null=True, blank=True)
    remarks = models.TextField(null=True, blank=True)
    is_approved = models.BooleanField(default=False)

    def __str__(self):
        return self.shipment.tracking_code


class ShipmentActivity(TimeStamp):
    shipment = models.ForeignKey(Shipment, on_delete=models.CASCADE)
    rider = models.ForeignKey(
        DeliveryPerson, on_delete=models.RESTRICT, null=True, blank=True)
    shipment_status = models.CharField(max_length=200, choices=SHIPMENT_STATUS)
    activity = models.CharField(max_length=500, null=True, blank=True)
    activity_by = models.ForeignKey(
        BranchOperator, on_delete=models.RESTRICT, related_name="operatoractivities", null=True, blank=True)
    involved_logistics = models.ForeignKey(
        LogisticsBranch, on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return self.shipment_status


class ShipmentBundle(TimeStamp):
    bundle_code = models.CharField(max_length=100, null=True, blank=True)
    source_branch = models.ForeignKey(
        LogisticsBranch, on_delete=models.RESTRICT)
    destination_branch = models.ForeignKey(
        LogisticsBranch, on_delete=models.RESTRICT, null=True, blank=True, related_name="destinationbundles")
    is_sent = models.BooleanField(default=False)
    sent_date = models.DateTimeField(null=True, blank=True)
    is_delivered = models.BooleanField(default=False)
    delivered_date = models.DateTimeField(null=True, blank=True)
    is_confirmed = models.BooleanField(default=False)
    confirmed_date = models.DateTimeField(null=True, blank=True)
    related_file = models.FileField(
        upload_to="shipments/bundles/", null=True, blank=True)
    transit_code = models.CharField(max_length=100, null=True, blank=True)
    transit_charge = models.IntegerField(default=0)

    remarks = models.TextField(null=True, blank=True)

    def __str__(self):
        return "CONSOLIDATION_" + str(self.id) + "(" + self.source_branch.logistics_company.company_name + ")"


class ShipmentBundleShipment(TimeStamp):
    shipment_bundle = models.ForeignKey(
        ShipmentBundle, on_delete=models.RESTRICT)
    shipment = models.ForeignKey(Shipment, on_delete=models.RESTRICT)
    shipment_status = models.CharField(
        max_length=200, choices=SHIPMENT_BUNDLE_SHIPMENT_STATUS, null=True, blank=True)
    source_branch = models.ForeignKey(
        LogisticsBranch, on_delete=models.RESTRICT, related_name="passed_by_shipments")
    destination_branch = models.ForeignKey(
        LogisticsBranch, on_delete=models.RESTRICT, related_name="passed_to_shipments", null=True, blank=True)
    current_sbs = models.BooleanField(default=False, null=True, blank=True)
    is_reverse_logistics = models.BooleanField(
        default=False, null=True, blank=True)
    # charge information
    shipping_charge = models.IntegerField(default=0, null=True, blank=True)
    cod_handling_charge = models.IntegerField(default=0, null=True, blank=True)
    rejection_handling_charge = models.IntegerField(
        default=0, null=True, blank=True)
    extra_charge = models.IntegerField(default=0)
    # payment information
    service_payment_status = models.BooleanField(default=False)
    service_payment_amount = models.IntegerField(null=True, blank=True)
    cod_value = models.IntegerField(default=0, null=True, blank=True)

    def __str__(self):
        return self.shipment.tracking_code


class ShipmentBundleActivity(TimeStamp):
    shipment_bundle = models.ForeignKey(
        ShipmentBundle, on_delete=models.CASCADE)
    activity = models.CharField(max_length=512)
    activity_by = models.ForeignKey(
        BranchOperator, on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return self.shipment_bundle.bundle_code


class DeliveryTask(TimeStamp):
    shipment = models.ForeignKey(
        Shipment, on_delete=models.RESTRICT, null=True, blank=True)
    shipment_bundle = models.ForeignKey(
        ShipmentBundle, on_delete=models.RESTRICT, null=True, blank=True)
    delivery_person = models.ForeignKey(
        DeliveryPerson, on_delete=models.RESTRICT)
    task_type = models.CharField(max_length=50, choices=DP_TASK_TYPE)
    task_status = models.CharField(max_length=50, choices=DP_TASK_STATUS)
    task_details = models.TextField(null=True, blank=True)
    scheduled_date = models.DateTimeField()
    completed_date = models.DateTimeField(null=True, blank=True)
    task_remarks = models.TextField(null=True, blank=True)
    assigned_by = models.ForeignKey(
        BranchOperator, on_delete=models.SET_NULL, null=True, blank=True)

    # for freelancers
    earned_point = models.IntegerField(default=0, null=True, blank=True)
    earned_amount = models.IntegerField(default=0, null=True, blank=True)
    is_earning_settled = models.BooleanField(default=False)

    def __str__(self):
        return self.delivery_person.full_name


class ShipmentRemark(TimeStamp):
    shipment = models.ForeignKey(Shipment, on_delete=models.CASCADE)
    remarks = models.TextField()
    is_private = models.BooleanField(default=False)

    delivery_task = models.ForeignKey(
        DeliveryPerson, on_delete=models.SET_NULL, null=True, blank=True)
    remarks_by_type = models.ForeignKey(
        ContentType, on_delete=models.CASCADE, limit_choices_to=SHIPMENT_REMARKS_BY)
    remarks_by_id = models.PositiveIntegerField()
    remarks_by = GenericForeignKey('remarks_by_type', 'remarks_by_id')
    remarks_written_by = models.ForeignKey(
        BranchOperator, on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return self.shipment.tracking_code + "(" + str(self.remarks) + ")"


class CustomerPaymentRequest(TimeStamp):
    logistics_customer = models.ForeignKey(
        LogisticsCustomer, on_delete=models.CASCADE)
    logistics_branch = models.ForeignKey(
        LogisticsBranch, on_delete=models.CASCADE)
    message = models.TextField(null=True, blank=True)
    shipments = models.ManyToManyField(Shipment, blank=True)
    preferred_receiving_account = models.ForeignKey(
        CustomerPaymentAccount, on_delete=models.SET_NULL, null=True, blank=True)
    request_amount = models.BigIntegerField(null=True, blank=True)
    payout_queue = models.PositiveIntegerField(null=True, blank=True)
    queued_date = models.DateTimeField(null=True, blank=True)
    estimated_payout_date = models.DateField(null=True, blank=True)
    remarks = models.TextField(null=True, blank=True)
    is_settled = models.BooleanField(default=False)
    settled_date = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.message


class CustomerPayment(TimeStamp):
    payment_request = models.ForeignKey(
        CustomerPaymentRequest, on_delete=models.SET_NULL, null=True, blank=True)
    logistics_branch = models.ForeignKey(
        LogisticsBranch, on_delete=models.RESTRICT)
    logistics_customer = models.ForeignKey(
        LogisticsCustomer, on_delete=models.RESTRICT)
    shipments = models.ManyToManyField(Shipment)
    total_shipping_charge = models.BigIntegerField()
    total_cod_handling_charge = models.BigIntegerField(default=0)
    total_rejection_handling_charge = models.BigIntegerField(default=0)
    discount_or_extra = models.BigIntegerField(default=0)
    # algebric sum of the above 4 fields
    total_service_charge = models.BigIntegerField()
    # sum of cod amount of those shipments whose cod_info is Cash on Delivery
    total_cod_value = models.BigIntegerField(default=0)
    # algebric sum of the above two fields
    net_paid_or_received = models.BigIntegerField()
    payment_method = models.ForeignKey(
        CustomerPaymentAccount, on_delete=models.SET_NULL, null=True, blank=True)
    remarks = models.TextField(null=True, blank=True)
    payment_confirmed_by = models.ForeignKey(
        BranchOperator, on_delete=models.SET_NULL, null=True)
    slip = models.FileField(
        upload_to="c_payment_slips/", null=True, blank=True)

    def __str__(self):
        return "Payment: " + str(self.id)


class LogisticsPaymentRequest(TimeStamp):
    requested_by = models.ForeignKey(
        LogisticsBranch, on_delete=models.RESTRICT, related_name='requestedby')
    requested_to = models.ForeignKey(
        LogisticsBranch, on_delete=models.RESTRICT, related_name='requestedto')
    # amount details
    requester_remarks = models.TextField(null=True, blank=True)
    is_settled = models.BooleanField(default=False)
    settled_date = models.DateTimeField(null=True, blank=True)
    # after payment
    payer_logistics = models.ForeignKey(
        LogisticsBranch, on_delete=models.RESTRICT, related_name="paidrequests", null=True, blank=True)
    # same field same value of BranchPayment object
    paid_or_received_amount = models.BigIntegerField(null=True, blank=True)
    is_verified = models.BooleanField(
        default=False, null=True, blank=True)  # receiver will verify

    def __str__(self):
        return str(self.paid_or_received_amount) + '- By' + str(self.requested_by)
    
    @property
    def amount_received(self):
        if self.payer_logistics == self.requested_to:
            branchpayment = self.branchpayment_set.get(status="Active", service_provider_branch=self.requested_by)
        else:
            branchpayment = self.branchpayment_set.get(status="Active", service_provider_branch=self.requested_to)

        amount_received = branchpayment.net_amount 

        return amount_received

    @property
    def amount_paid(self):
        branchpayment = self.branchpayment_set.get(status="Active", service_provider_branch=self.payer_logistics)
        amount_paid = branchpayment.net_amount
        return amount_paid


class BranchPayment(TimeStamp):
    payment_request = models.ForeignKey(
        LogisticsPaymentRequest, on_delete=models.RESTRICT)
    service_user_branch = models.ForeignKey(
        LogisticsBranch, on_delete=models.RESTRICT)
    service_provider_branch = models.ForeignKey(
        LogisticsBranch, on_delete=models.RESTRICT, related_name="dcbranchpayments")
    bundle_shipments = models.ManyToManyField(ShipmentBundleShipment, blank=True)
    total_shipping_charge = models.BigIntegerField()
    total_cod_handling_charge = models.BigIntegerField(default=0)
    total_rejection_handling_charge = models.BigIntegerField(default=0)
    total_service_charge = models.BigIntegerField()
    total_cod_value = models.BigIntegerField(default=0)
    # net amount of a logistics branch. (cod value - service charge)
    net_amount = models.BigIntegerField()
    paid_or_received_amount = models.BigIntegerField()  # net amount of both party
    payment_method = models.ForeignKey(
        BranchPaymentAccount, on_delete=models.SET_NULL, null=True, blank=True)
    # verify it when payment request is verified.
    is_verified = models.BooleanField(default=False)
    remarks = models.TextField(null=True, blank=True)
    slip = models.FileField(
        upload_to="l_payment_slips/", null=True, blank=True)

    def __str__(self):
        return "Payment: " + str(self.id)


class LogisticsTransaction(TimeStamp):
    logistics_branch = models.ForeignKey(LogisticsBranch, on_delete=models.RESTRICT)
    transaction_date = models.DateTimeField()
    transaction_type = models.CharField(max_length=100, choices=TRANSACTION_TYPE)
    transaction_category = models.CharField(max_length=100, choices=TRANSACTION_CATEGORY)
    title = models.CharField(max_length=200)
    description = models.TextField(null=True, blank=True)
    transaction_amount = models.BigIntegerField()
    reference_data = models.CharField(max_length=100, null=True, blank=True) # eg objectmodel_objectid eg. CustomerPayment_50

    def __str__(self):
        return self.title




class CustomerNotification(TimeStamp):
    logistics_branch = models.ForeignKey(
        LogisticsBranch, on_delete=models.SET_NULL, null=True, blank=True)
    logistics_customer = models.ForeignKey(
        LogisticsCustomer, on_delete=models.CASCADE)
    is_for_customer = models.BooleanField(default=True)
    title = models.CharField(max_length=256)
    description = models.TextField(null=True, blank=True)
    redirection_link = models.CharField(max_length=256)
    is_seen = models.BooleanField(default=False)

    def __str__(self):
        return self.title
