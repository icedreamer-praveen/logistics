from django.contrib import admin
from .models import *


admin.site.register([ShipmentActivity, ShipmentBundle, ShipmentBundleShipment, ShipmentBundleActivity, DeliveryTask, ShipmentRemark, CustomerPaymentRequest, BranchPayment, CustomerNotification, ExchangeRequest, LogisticsPaymentRequest,  ])

class ShipmentAdmin(admin.ModelAdmin):
    search_fields = ["tracking_code", "shipment_status", "receiver_contact_number"]


admin.site.register(Shipment, ShipmentAdmin)


class LogisticsTransactionAdmin(admin.ModelAdmin):
    search_fields = ["id", "transaction_category", "title", "transaction_amount"]


admin.site.register(LogisticsTransaction, LogisticsTransactionAdmin)


class CustomerPaymentAdmin(admin.ModelAdmin):
    search_fields = ["net_paid_or_received", "logistics_customer__company_name", "logistics_customer__full_name", "id"]


admin.site.register(CustomerPayment, CustomerPaymentAdmin)