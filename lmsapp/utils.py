from .models import Shipment, CustomerPayment
from django.db.models import Sum
from accapp.constants import *
import random
import string
from django.contrib.auth.tokens import PasswordResetTokenGenerator
import six


def get_current_balance(customer, shipment_origin=None):
    shipments = Shipment.objects.filter(
        status="Active", service_payment_status=False, shipment_status__in=customer_completed_charge_sl, customer=customer)
    if customer and shipment_origin is None:
        shipments = shipments.filter(customer=customer)
    elif customer is None and shipment_origin:
        shipments = shipments.filter(shipment_origin=shipment_origin)
    elif customer and shipment_origin:
        shipments = shipments.filter(
            customer=customer, shipment_origin=shipment_origin)
    else:
        shipments = Shipment.objects.none()

    total_cod_value = shipments.filter(
        shipment_status__in=customer_completed_cod_sl,
        cod_info="Cash on Delivery").aggregate(total=Sum("cod_value")).get("total")
    total_cod_charge = shipments.filter(
        shipment_status__in=customer_completed_cod_sl,
        cod_info="Cash on Delivery").aggregate(total=Sum("cod_handling_charge")).get("total")
    total_shipping_charge = shipments.aggregate(
        total=Sum("shipping_charge")).get("total")
    total_rejection_charge = shipments.filter(
        shipment_status__in=customer_failed_charge_sl).aggregate(total=Sum("rejection_handling_charge")).get("total")

    total_cod_value = total_cod_value if total_cod_value else 0
    total_cod_charge = total_cod_charge if total_cod_charge else 0
    total_shipping_charge = total_shipping_charge if total_shipping_charge else 0
    total_rejection_charge = total_rejection_charge if total_rejection_charge else 0
    current_balance = total_cod_value - total_shipping_charge - \
        total_cod_charge - total_rejection_charge
    return current_balance


def get_incomplete_charge(customer):
    shipments = Shipment.objects.filter(
        status="Active", service_payment_status=False, shipment_status__in=customer_incomplete_cod_and_charge_sl, customer=customer)

    total_cod_value = shipments.filter(
        cod_info="Cash on Delivery").aggregate(total=Sum("cod_value")).get("total")
    total_cod_charge = shipments.filter(
        cod_info="Cash on Delivery").aggregate(total=Sum("cod_handling_charge")).get("total")
    total_shipping_charge = shipments.aggregate(
        total=Sum("shipping_charge")).get("total")

    total_cod_value = total_cod_value if total_cod_value else 0
    total_cod_charge = total_cod_charge if total_cod_charge else 0
    total_shipping_charge = total_shipping_charge if total_shipping_charge else 0
    net_amount = total_cod_value - total_shipping_charge - total_shipping_charge
    return total_cod_value, total_cod_charge + total_shipping_charge, net_amount


def get_payment(customer):
    payments = CustomerPayment.objects.filter(status="Active", logistics_customer=customer)
    total_cod_value = payments.aggregate(
        total=Sum("total_cod_value")).get("total")
    total_service_charge = payments.aggregate(
        total=Sum("total_service_charge")).get("total")
    net_paid_or_received = payments.aggregate(
        total=Sum("net_paid_or_received")).get("total")

    total_cod_value = total_cod_value if total_cod_value else 0
    total_service_charge = total_service_charge if total_service_charge else 0
    net_paid_or_received = net_paid_or_received if net_paid_or_received else 0
    return total_cod_value, total_service_charge, net_paid_or_received


def logistics_get_current_balance(logistics, requested_to):
    shipments = Shipment.objects.filter(
        status="Active", service_payment_status=False, shipment_status__in=customer_completed_charge_sl)
    if customer and shipment_origin is None:
        shipments = shipments.filter(customer=customer)
    elif customer is None and shipment_origin:
        shipments = shipments.filter(shipment_origin=shipment_origin)
    elif customer and shipment_origin:
        shipments = shipments.filter(
            customer=customer, shipment_origin=shipment_origin)
    else:
        shipments = Shipment.objects.none()

    total_cod_value = shipments.filter(
        shipment_status__in=customer_completed_cod_sl,
        cod_info="Cash on Delivery").aggregate(total=Sum("cod_value")).get("total")
    total_cod_charge = shipments.filter(
        shipment_status__in=customer_completed_cod_sl,
        cod_info="Cash on Delivery").aggregate(total=Sum("cod_handling_charge")).get("total")
    total_shipping_charge = shipments.aggregate(
        total=Sum("shipping_charge")).get("total")
    total_rejection_charge = shipments.filter(
        shipment_status__in=customer_failed_charge_sl).aggregate(total=Sum("rejection_handling_charge")).get("total")

    total_cod_value = total_cod_value if total_cod_value else 0
    total_cod_charge = total_cod_charge if total_cod_charge else 0
    total_shipping_charge = total_shipping_charge if total_shipping_charge else 0
    total_rejection_charge = total_rejection_charge if total_rejection_charge else 0
    current_balance = total_cod_value - total_shipping_charge - \
        total_cod_charge - total_rejection_charge
    return current_balance



def get_total_amount_to_pay(branch):
    shipments = Shipment.objects.filter(
        status="Active", service_payment_status=False, shipment_status__in=customer_completed_charge_sl, shipment_origin=branch)
    
    total_amount_to_pay = shipments.filter(shipment_status__in=customer_completed_cod_sl,
        cod_info="Cash on Delivery").aggregate(total=Sum("cod_value")).get("total")
    
    total_amount_to_pay = total_amount_to_pay if total_amount_to_pay else 0
    
    return total_amount_to_pay

class PasswordResetTokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, user, timestamp):
        return (
            six.text_type(user.pk) + six.text_type(timestamp)
        )


password_reset_token = PasswordResetTokenGenerator()


def random_string_generator(size=10,
                            chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))
