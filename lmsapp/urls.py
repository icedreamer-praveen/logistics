from django.urls import path
from .views import *

app_name = "lmsapp"
urlpatterns = [
    # customer urls ----------------------------------------------------------
    # customer urls ----------------------------------------------------------
    # customer urls ----------------------------------------------------------
    # general
    path("customer/", CustomerLoginView.as_view(), name="customerlogin"),
    path("customer/logout/", CustomerLogoutView.as_view(), name="customerlogout"),
    path('customer/forgot-password/', CustomerForgotPasswordView.as_view(),
         name="customerforgotpassword"),
    path('customer/reset-password/<uidb64>/<token>/', CustomerPasswordResetView.as_view(),
         name="customerpasswordreset"),
    path('customer/reset-password/successful/', CustomerPasswordResetCompleteView.as_view(),
         name='customerpasswordresetcomplete'),
    path("customer/dashboard/", CustomerDashboardView.as_view(),
         name="customerdashboard"),
    path("customer/dashboard-ajax/",
         CustomerDashboardAjaxView.as_view(), name="customerdashboardajax"),
    path("customer/signup/", CustomerSignupView.as_view(), name="customersignup"),


    # shipment
    path("customer/shipment-list/", CustomerShipmentListView.as_view(),
         name="customershipmentlist"),
    path("customer/shipment-create/", CustomerShipmentCreateView.as_view(),
         name="customershipmentcreate"),
    path("customer/ajax-get-szs/", CustomerAjaxGetSZSView.as_view(),
         name="customerajaxgetszs"),
    path("customer/bulk-shipment/",
         CustomerBulkShipmentView.as_view(), name="customerbulkshipmentcreate"),
    path("customer/bulk-shipment/create", AjaxCustomerBulkUploadShipmentView.as_view(),
         name="ajaxcustomerbulkshipmentupload"),
    path("customer/shipment-<tracking_code>-detail/",
         CustomerShipmentDetailView.as_view(), name="customershipmentdetail"),
    path("customer/shipment-<tracking_code>-action/",
         CustomerShipmentActionView.as_view(), name="customershipmentaction"),

    # payment
    path("customer/payment-request-list/",
         CustomerPaymentRequestListView.as_view(), name="customerpaymentrequestlist"),
    path("customer/payment-request-create/",
         CustomerPaymentRequestCreateView.as_view(), name="customerpaymentrequestcreate"),
    path("customer/payment-list/", CustomerPaymentListView.as_view(),
         name="customerpaymentlist"),
    path("customer/payment-<int:pk>-detail/",
         CustomerPaymentDetailView.as_view(), name="customerpaymentdetail"),

    # notification click and redirect
    path("customer/notifications/",
         CustomerNotificationListView.as_view(), name="customernotificationlist"),
    path("customer/notifications-<int:pk>-click/",
         CustomerNotificationClickView.as_view(), name="customernotificationclick"),

    # branch urls ------------------------------------------------------------
    # branch urls ------------------------------------------------------------
    # branch urls ------------------------------------------------------------

    path("logistics/", LogisticsLoginView.as_view(), name="logisticslogin"),
    path("logistics/logout/", LogisticsLogoutView.as_view(), name="logisticslogout"),
    path("logistics/dashboard/", LogisticsDashboardView.as_view(),
         name="logisticsdashboard"),
    path("logistics/dashboard-ajax/",
         LogisticsDashboardAjaxView.as_view(), name="logisticsdashboardajax"),

    # shipment management
    path("logistics/shipment-list/",
         LogisticsShipmentListView.as_view(), name="logisticsshipmentlist"),
    path("logistics/ajax/verify-shipment-list-selection/",
         AjaxLogisticsShipmentListVerifyView.as_view(), name="ajaxlogisticsshipmentlistverify"),
    path("logistics/shipment-create/",
         LogisticsShipmentCreateView.as_view(), name="logisticsshipmentcreate"),
    path("logistics/shipment/ajax-get-szs/", LogisticsAjaxGetSZSView.as_view(),
         name="logisticsajaxgetszs"),
    path("logistics/shipment-list-table/",
         LogisticsShipmentListTableView.as_view(), name="logisticsshipmentlisttable"),
    path("logistics/customer-<int:pk>/shipment-list/",
         LogisticsCustomerShipmentListView.as_view(), name="logisticscustomershipmentlist"),
    path("logistics/shipment-<tracking_code>-detail/",
         LogisticsShipmentDetailView.as_view(), name="logisticsshipmentdetail"),
    path("logistics/shipment-<tracking_code>-update/",
         LogisticsShipmentUpdateView.as_view(), name="logisticsshipmentupdate"),
    path("logistics/shipment-action/",
         LogisticsShipmentActionView.as_view(), name="logisticsshipmentaction"),
    path("logistics/shipment-to-bundle/",
         LogisticsShipmentToBundleView.as_view(), name="logisticsshipmenttobundle"),

    path("logistics/consolidations/",
         LogisticsBundleListView.as_view(), name="logisticsbundlelist"),
    path("logistics/consolidations-create/",
         LogisticsBundleCreateView.as_view(), name="logisticsbundlecreate"),
    path("logistics/consolidations-<int:pk>/",
         LogisticsBundleDetailView.as_view(), name="logisticsbundledetail"),
    path("logistics/consolidations-<int:pk>-update/",
         LogisticsBundleUpdateView.as_view(), name="logisticsbundleupdate"),
    path("logistics/bundle-<int:pk>-shipment/",
         LogisticsBundleShipmentView.as_view(), name="logisticsbundleshipment"),
    path("logistics/custom-shipment-list/",
         LogisticsCustomShipmentListView.as_view(), name="logisticscustomshipmentlist"),
    path("logistics/ajax/custom-shipment-list/",
         AjaxLogisticsCustomShipmentListView.as_view(), name="ajaxlogisticscustomshipmentlist"),
    path("logistics/ajax/custom-shipment-table-list/",
         AjaxLogisticsCustomShipmentTableListView.as_view(), name="ajaxlogisticscustomshipmenttablelist"),

    # Logistics Customer Shipment Reports
    path("logistics/reports/customer-<int:pk>/shipmment/",
         LogisticsCustomerShipmentReportView.as_view(), name="logisticscustomershipmentreport"),

    # delivery tasks
    path("logistics/delivery-tasks/",
         LogisticsDeliveryTaskListView.as_view(), name="logisticsdeliverytasklist"),
    path("logistics/delivery-task-<int:pk>/",
         LogisticsDeliveryTaskDetailView.as_view(), name="logisticsdeliverytaskdetail"),

    # shipment label
    path('logistics/shipment-<tracking_code>/shipping-label/',
         LogisticsShipmentLabelView.as_view(), name='logisticsshippinglabel'),
    path("logistics/shipment/bulk-shipping-label/",
         LogisticsBulkShippingLabelView.as_view(), name="logisticsbulkshippinglabel"),

    # payment management
    path("logistics/customer-payment-requests/",
         LogisticsCustomerPaymentRequestListView.as_view(), name="logisticscustomerpaymentrequestlist"),
    path("logistics/customer-payment-request-<int:pk>/",
         LogisticsCustomerPaymentRequestDetailView.as_view(), name="logisticscustomerpaymentrequestdetail"),
    path("logistics/cpr-<int:pk>-pay/",
         LogisticsCustomerPaymentRequestPayView.as_view(), name="logisticscustomerpaymentrequestpay"),
    path("logistics/connected-logistics/payment-list/",
         LogisticsConnectedLogisticsPaymentListView.as_view(), name="logisticsconnectedlogisticspaymentlist"),
    path("logistics/connected-logistics/make-payment/<int:pk>/<logisticscompany>/",
         LogisticsConnectedLogisticsMakePaymentView.as_view(), name="logisticsconnectedpayment"),
    path("logistics/payment-request-create/",
         LogisticsPaymentRequestCreateView.as_view(), name="logisticspaymentrequestcreate"),
    path("logistics/logistics-payment/prequest-<int:pk>/pto-<int:pk1>/",
         LogisticsLogisticsPaymentView.as_view(), name="logisticslogisticspayment"),
    path("logistics/logistics-payment/<int:pk>-verification/",
         LogisticsLogisticsPaymentVerificationView.as_view(), name="logisticslogisticspaymentverification"),
    path("logistics/logistics-payments/",
         LogisticsPaymentReportView.as_view(), name="logisticspaymentrepots"),
    path("logistics/logistics-payments/<int:pk>/detail/",
         LogisticsBranchPaymentDetailView.as_view(), name="logisticsbranchpaymentdetail"),

    # transaction management
    path("logistics/transaction-list/", LogisticsTransactionListView.as_view(), name="logisticstransactionlist"),
    path("logistics/transaction-create/", LogisticsTransactionCreateView.as_view(), name="logisticstransactioncreate"),

    #Reports
    path("logistics/<slug:slug>/logistics-report/", LogisticsReportView.as_view(), name="logisticsreport"),

    path("logistics/overall-account-report/", 
        LogisticsOverallAccountReportView.as_view(), name="logisticsoverallaccountreport"),

     
    #Customer Wallet
    path("logistics/customer/wallet/", LogisticsCustomerWalletListView.as_view(), name="logisticscustomerwalletlist"),
 

]
