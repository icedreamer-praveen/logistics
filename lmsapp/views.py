from lbcapp.models import LogisticsBranchShippingZone, LogisticsBranchContract, LogisticsBranchZonalContract, CustomerSupportTicket
from django.views.generic import View, TemplateView, CreateView, ListView, DetailView, FormView, UpdateView
from accapp.views import CustomerRequiredMixin, LogisticsRequiredMixin
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect
from django.urls import reverse, reverse_lazy
from django.contrib.auth.models import User
from django.db.models import Q, Sum, Count
from datetime import datetime, timedelta
from django.http import JsonResponse
from django.contrib import messages
from django.utils import timezone, http, encoding 
from decimal import Decimal
from .models import *
from .forms import *
from .utils import *
import json
from siteapp.models import LogisticsOrganizationalInformation
from django.template.loader import render_to_string
from siteapp.views import ClientMixin
from django.core.mail import send_mail
from django.conf import settings

# 2 customers views------------------------------------------------------------------------

# customer dashboard


class CustomerLoginView(TemplateView):
    template_name = "customertemplates/customerlogin.html"

    def post(self, request, *args, **kwargs):
        login_form = CustomerLoginForm(request.POST)
        if login_form.is_valid():
            email = login_form.cleaned_data.get("email")
            password = login_form.cleaned_data.get("password")
            user = authenticate(username=email, password=password)
            try:
                customer = user.logisticscustomer
                login(request, user)
                messages.success(request, "You are logged in successfully.")
                return redirect("lmsapp:customerdashboard")
            except Exception as e:
                print(e)
                return render(request, self.template_name, {"error": "Invalid username or password"})
        else:
            print(login_form.errors)
            return render(request, self.template_name, {"error": "Invalid username or password"})


class CustomerLogoutView(CustomerRequiredMixin, View):
    def get(self, request):
        logout(request)
        return redirect("lmsapp:customerlogin")


class CustomerForgotPasswordView(ClientMixin, FormView):
    template_name = "customertemplates/customerpasswordreset.html"
    form_class = CustomerForgotPasswordForm
    success_url = reverse_lazy('lmsapp:customerlogin')
    success_message = 'Password Reset Link has been send to your email. Please check your email.'

    def form_valid(self, form):
        email = form.cleaned_data['email']
        user = User.objects.get(email=email)
        domain = self.request.META['HTTP_HOST']
        html_content = render_to_string(
            'customertemplates/customerpasswordresetemail.html',
            {
                'domain': domain, 'email': email,
                'token': password_reset_token.make_token(user),
                'uid': http.urlsafe_base64_encode(
                    encoding.force_bytes(user.pk)),
                'user': user,
                'organization': LogisticsOrganizationalInformation.objects.filter(status="Active").first()
            })
        send_mail(
            'Password Reset from ' + str(domain),
            html_content,
            settings.EMAIL_HOST_USER,
            [email],
            fail_silently=False,
        )
        return super().form_valid(form)

class CustomerPasswordResetView(ClientMixin, FormView):
    template_name = "customertemplates/customerpasswordresetconfirm.html"
    form_class = CustomerPasswordResetForm
    success_url = reverse_lazy('lmsapp:customerpasswordresetcomplete')

    def dispatch(self, request, uidb64, token):
        self.validlink = False
        try:
            uid = encoding.force_text(http.urlsafe_base64_decode(uidb64))
            user = User.objects.get(id=uid)
        except Exception as e:
            user = None
            self.validlink = False
        self.user = user
        if user is not None and password_reset_token.check_token(user, token):
            self.validlink = True
        else:
            self.validlink = False
        return super().dispatch(request)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.validlink:
            context['validlink'] = True
        else:
            context['validlink'] = False
        return context

    def form_valid(self, form):
        password = form.cleaned_data['new_password2']
        self.user.set_password(password)
        self.user.save()
        return super().form_valid(form)


class CustomerPasswordResetCompleteView(ClientMixin, TemplateView):
    template_name = 'customertemplates/customerpasswordresetcomplete.html'

class CustomerSignupView(TemplateView):
    template_name = 'customertemplates/customersignup.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = CustomerSignupForm
        return context

    def post(self, request, *args, **kwargs):
        signup_form = CustomerSignupForm(request.POST)
        if signup_form.is_valid():
            email = signup_form.cleaned_data.get("signup_email")
            password = signup_form.cleaned_data.get("signup_password")
            full_name = signup_form.cleaned_data.get("full_name")
            contact_number = signup_form.cleaned_data.get("contact_number")
            business_name = signup_form.cleaned_data.get("company_name")
            city = signup_form.cleaned_data.get("city")
            address = signup_form.cleaned_data.get("address")
            user = User.objects.create_user(
                username=email, email=email, password=password)
            signup_form.instance.status = "Active"
            signup_form.instance.user = user
            signup_form.instance.customer_type = 'Business Customer'
            signup_form.instance.full_name = full_name
            signup_form.instance.company_name = business_name
            signup_form.instance.city = city
            signup_form.instance.address = address
            signup_form.instance.contact_number = contact_number
            signup_form.save()
            lb = LogisticsBranch.objects.filter(status="Active").first()
            lc = LogisticsCustomer.objects.get(user=user, status="Active")
            lc.connected_logistics.add(lb)
            login(request, user)
            messages.success(
                request, "You have been registered as business customer. Please proceed through by signing in with your credential")
        else:
            form = CustomerSignupForm()
            print(signup_form.errors)
            return render(request, self.template_name, {"form": signup_form})

        return redirect("lmsapp:customerdashboard")


class CustomerDashboardView(CustomerRequiredMixin, TemplateView):
    template_name = "customertemplates/customerdashboard.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        shipments = Shipment.objects.filter(
            status="Active", customer=self.customer)
        context["requested_shipments_count"] = shipments.filter(
            shipment_status__in=customer_pending_sl).count()
        context["processing_shipments_count"] = shipments.filter(
            shipment_status__in=customer_processing_sl).count()
        context["dispatched_shipments_count"] = shipments.filter(
            shipment_status__in=customer_dispatched__sl).count()
        context["rejected_shipments_count"] = shipments.filter(
            shipment_status__in=customer_rejected_sl).count()
        context["delivered_shipments_count"] = shipments.filter(
            shipment_status__in=customer_delivered_sl).count()
        context["returned_shipments_count"] = shipments.filter(
            shipment_status__in=customer_returned_sl).count()
        context["canceled_shipments_count"] = shipments.filter(
            shipment_status__in=customer_canceled_sl).count()
        context["all_shipments_count"] = shipments.count()

        # payment overview
        context["current_balance"] = get_current_balance(self.customer)
        context["incomplete_cod_amount"] = get_incomplete_charge(self.customer)[
            0]
        context["incomplete_charge"] = get_incomplete_charge(self.customer)[1]
        context["total_cod_received"] = get_payment(self.customer)[0]
        context["total_charge_paid"] = get_payment(self.customer)[1]
        context["net_paid_or_received"] = get_payment(self.customer)[2]

        # remarks 
        context["latest_remarks"] = ShipmentRemark.objects.filter(status="Active", is_private=False, shipment__customer=self.customer).order_by('-created_at')[:10]
        return context


class CustomerDashboardAjaxView(CustomerRequiredMixin, View):
    def get(self, request):
        query = request.GET.get("query")
        if query == "weekly_report":
            days = []
            data = []
            today = timezone.localtime(timezone.now()).date()
            start_date = today - timedelta(days=6)
            for i in range(7):
                date = start_date + timedelta(days=i)
                shipments = Shipment.objects.filter(
                    customer=self.customer, created_at__date=date).count()
                days.append(date)
                data.append(shipments)

            return JsonResponse({"days": days, "data": data})

        return super().get(request)

# shipment management


class CustomerShipmentListView(CustomerRequiredMixin, ListView):
    template_name = "customertemplates/shipment/customershipmentlist.html"
    paginate_by = 50
    context_object_name = "shipments"

    def get_queryset(self):
        shipments = Shipment.objects.filter(
            status="Active", customer=self.customer)
        if "status" in self.request.GET:
            self.status = self.request.GET.get("status")
            if self.status in ["Pending", "pending"]:
                queryset = shipments.filter(
                    shipment_status__in=customer_pending_sl)
            elif self.status in ["Processing", "processing"]:
                queryset = shipments.filter(
                    shipment_status__in=customer_processing_sl)
            elif self.status in ["Dispatched", "dispatched"]:
                queryset = shipments.filter(
                    shipment_status__in=customer_dispatched__sl)
            elif self.status in ["Rejected", "rejected"]:
                queryset = shipments.filter(
                    shipment_status__in=customer_rejected_sl)
            elif self.status in ["Delivered", "delivered"]:
                queryset = shipments.filter(
                    shipment_status__in=customer_delivered_sl)
            elif self.status in ["Returned", "returned"]:
                queryset = shipments.filter(
                    shipment_status__in=customer_returned_sl)
            elif self.status in ["Canceled", "canceled"]:
                queryset = shipments.filter(
                    shipment_status="Shipment Canceled")
            elif self.status in ["All", "all"]:
                queryset = shipments
            else:
                queryset = shipments.filter(
                    shipment_status__in=customer_pending_sl)
                self.status = "Pending"
        elif "search" in self.request.GET:
            self.status = "Search Results"
            keyword = self.request.GET.get('search')
            queryset = shipments.filter(
                Q(tracking_code__icontains=keyword) | Q(receiver_name__icontains=keyword) | Q(receiver_contact_number__icontains=keyword) | Q(receiver_city__name__icontains=keyword) | Q(reference_code__icontains=keyword))
        else:
            self.status = "Pending"
            queryset = shipments.filter(
                shipment_status__in=customer_pending_sl)
        return queryset.order_by("-id")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        shipments = Shipment.objects.filter(
            status="Active", customer=self.customer)
        # counts
        context["status"] = self.status
        context["requested_shipments"] = shipments.filter(
            shipment_status__in=customer_pending_sl)
        context["processing_shipments"] = shipments.filter(
            shipment_status__in=customer_processing_sl)
        context["dispatched_shipments"] = shipments.filter(
            shipment_status__in=customer_dispatched__sl)
        context["rejected_shipments"] = shipments.filter(
            shipment_status__in=customer_rejected_sl)
        context["delivered_shipments"] = shipments.filter(
            shipment_status__in=customer_delivered_sl)
        context["returned_shipments"] = shipments.filter(
            shipment_status__in=customer_returned_sl)

        return context


class CustomerShipmentDetailView(CustomerRequiredMixin, TemplateView):
    template_name = "customertemplates/shipment/customershipmentdetail.html"

    def get(self, request, *args, **kwargs):
        try:
            self.shipment = Shipment.objects.get(
                tracking_code=self.kwargs.get("tracking_code"))
            if self.shipment.customer == self.customer:
                pass
            else:
                messages.error(request, "Unauthorized page request. ")
                return redirect("lmsapp:customershipmentlist")
        except Exception as e:
            messages.error(request, "Shipment does not exist. ")
            return redirect("lmsapp:customershipmentlist")
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        shipment = Shipment.objects.get(
            tracking_code=self.kwargs.get("tracking_code"))
        context["shipment"] = shipment
        context["activities"] = ShipmentActivity.objects.filter(
            shipment=shipment, status="Active").order_by("id")
        context["remarks_list"] = ShipmentRemark.objects.filter(
            shipment=shipment, status="Active", is_private=False).order_by("id")
        if shipment.shipment_status in customer_delivered_sl:
            context["delivered"] = "delivered"
        elif shipment.shipment_status in customer_rejected_sl:
            context["rejected"] = "rejected"
        return context

    def post(self, request, *args, **kwargs):
        shipment = Shipment.objects.get(
            tracking_code=self.kwargs.get("tracking_code"))
        remarks = request.POST.get("remarks")
        ShipmentRemark.objects.create(
            status="Active", shipment=shipment, remarks=remarks, remarks_by=self.customer, is_private=False)
        return redirect("lmsapp:customershipmentdetail", tracking_code=self.kwargs.get("tracking_code"))


class CustomerShipmentActionView(CustomerRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        try:
            shipment = Shipment.objects.get(
                tracking_code=self.kwargs.get("tracking_code"), customer=self.customer)
            action = request.POST.get("action")
            if action == "exchange" and shipment.shipment_status in customer_exchange_processing_sl:
                if ExchangeRequest.objects.filter(status="Active", old_shipment=shipment).exists():
                    messages.error(
                        request, "You have already requested for exchange.")
                    return JsonResponse({"status": "error"})
                else:
                    print("Exchange request")
                    # if shipment is delivered, make it rejected, and process for ready to return, review and update shipment charges, also tick exchanged_after_delivered field in shipment
                    # create a new shipment with the important data collected from the customer
                    # create an exchange request and relate with the shipments
                    # cancel related active task

            elif action == "refund" and shipment.shipment_status in customer_delivered_sl:
                print("Refund request")
                # create a refund request with remarks and update charge according to new review
                # make shipment rejected and process returning and also tick exchanged_after_delivered field in shipment
                # cancel related active task
            else:
                messages.error(request, "Unknown Action")
                return JsonResponse({"status": "error"})
        except Exception as e:
            print(e)
            messages.error(request, "Shipment does not exist. ")
            return redirect("lmsapp:customershipmentlist")
        return JsonResponse({"status": "ok"})


class CustomerShipmentCreateView(CustomerRequiredMixin, CreateView):
    template_name = "customertemplates/shipment/customershipmentcreate.html"
    form_class = CustomerShipmentCreateForm
    success_url = reverse_lazy("lmsapp:customershipmentlist")

    def get(self, request, *args, **kwargs):
        logistics_branch = LogisticsBranch.objects.first()
        if logistics_branch.disable_shipment_create:
            return render(self.request, 'customertemplates/shipment/customerdisableshipmentcreate.html')
        return super().get(request, *args, **kwargs)

    def form_valid(self, form):
        sz_id = self.request.POST.get("shipping_zone")
        try:
            sz = LogisticsBranchShippingZone.objects.get(id=sz_id)
        except Exception as e:
            print('CustomerShipmentCreateView', e)
            return render(self.request, self.template_name, {"error": "Delivery not available at the selected location for now. Please contact logistics or select a different city", "form": self.form_class})
        form.instance.status = "Active"
        form.instance.shipment_status = "Shipment Requested"
        form.instance.shipment_origin = sz.logistics_branch

        # sender information
        form.instance.customer = self.customer
        form.instance.sender_name = self.customer.company_name
        form.instance.sender_city = self.customer.city
        form.instance.sender_address = self.customer.address
        form.instance.sender_contact_number = self.customer.contact_number
        form.instance.sender_email = self.customer.user.username

        # cost and payment information
        cod_value = form.cleaned_data.get(
            "cod_value")
        szdt = sz.shippingzonedeliverytype_set.filter(
            delivery_type__delivery_type__in=["Standard", "Standard Delivery"]).last()
        form.instance.delivery_type = szdt
        form.instance.shipping_charge = szdt.shipping_charge_per_kg
        form.instance.cod_handling_charge = szdt.cod_handling_charge * cod_value / 100
        form.instance.rejection_handling_charge = szdt.cod_handling_charge * cod_value / 100
        form.instance.pickup_charge = 0
        form.instance.dropoff_charge = 0
        form.instance.extra_charge = 0
        form.instance.service_payment_status = False
        shipment = form.save()
        if cod_value > 0:
            shipment.cod_info = "Cash on Delivery"
        else:
            shipment.cod_info = "Already Paid"
        shipment.tracking_code = f"SARATHI-PT-{shipment.id}"
        messages.success(self.request, "Shipment uploaded successfully.")
        ShipmentActivity.objects.create(
            shipment=shipment, shipment_status="Shipment Requested", activity="Shipment uploaded by the customer.")
        return super().form_valid(form)

    def form_invalid(self, form):
        print(form.errors)
        return super().form_invalid(form)


class CustomerAjaxGetSZSView(CustomerRequiredMixin, TemplateView):
    template_name = "customertemplates/shipment/customerajaxgetszs.html"

    def get_context_data(self, **kwargs):
        city_id = self.request.GET.get('city_id', None)
        branches = self.customer.connected_logistics.filter(status="Active")
        shipping_zones = LogisticsBranchShippingZone.objects.filter(
            logistics_branch__branch_coverage=self.customer.city, logistics_branch__in=branches, cities__id=city_id).distinct()
        context = {
            "shipping_zones": shipping_zones
        }
        return context


class CustomerBulkShipmentView(CustomerRequiredMixin, FormView):
    template_name = "customertemplates/shipment/customerbulkshipmentcreate.html"
    form_class = CustomerBulkUploadShipmentForm
    success_url = reverse_lazy('lmsapp:customerdashboard')
    success_message = "Bulk Shipment Requested Successfully"

    def decode_utf8(self, input_iterator):
        for l in input_iterator:
            yield l.decode('utf-8')

    def form_valid(self, form):
        import os.path
        filename = str(form.cleaned_data.get('file'))
        extension = os.path.splitext(filename)[1]
        if extension == '.xlsx':
            import xlrd
            uploaded_file = form.cleaned_data.get('file')
            wb = xlrd.open_workbook(file_contents=uploaded_file.read())
            sheet = wb.sheet_by_index(0)
            logistics_branch = form.cleaned_data.get('logistics_branch')
            # logistics_branch = LogisticsBranch.objects.get()
            call_create_data = []
            shipments = []
            blank_rows = 0
            for i in range(sheet.nrows):
                if i != 0:
                    if blank_rows < 1:
                        # Check Empty row and empty name, contact, street_address, mode of payment
                        if sheet.cell_value(i, 0) == '' and sheet.cell_value(i, 1) == '' and sheet.cell_value(i, 2) == '' and sheet.cell_value(i, 3) == '' and sheet.cell_value(i, 5) == '':
                            blank_rows += 1
                            break
                        sheet_name = sheet.cell_value(i, 0)
                        sheet_name.replace(" ", "")
                        if sheet_name == '':
                            call_create_data.append(False)
                            error_name = 'empty'
                        else:
                            error_name = ''
                        sheet_contact = str(sheet.cell_value(i, 1))
                        sheet_contact.replace(" ", "")
                        if sheet_contact == '':
                            call_create_data.append(False)
                            err_contact = 'empty'
                        else:
                            err_contact = ''
                        sheet_street_addr = sheet.cell_value(i, 3)
                        sheet_street_addr.replace(" ", "")
                        if sheet_street_addr == '':
                            call_create_data.append(False)
                            err_street_addr = 'empty'
                        else:
                            err_street_addr = ''
                        sheet_mop = sheet.cell_value(i, 5)
                        sheet_mop.replace(" ", "")
                        if sheet_mop == '':
                            call_create_data.append(False)
                            err_mop = 'empty'
                        else:
                            err_mop = ''
                        # End empty field check

                        excel_city = sheet.cell_value(i, 2).strip().title()
                        city = City.objects.filter(name__iexact=excel_city)
                        if city.exists():
                            call_create_data.append(True)
                            error = ''
                        else:
                            call_create_data.append(False)
                            error = sheet.cell_value(i, 2)

                        if sheet.cell_value(i, 5) == 'Cash on Delivery' or sheet.cell_value(i, 5) == 'Already Paid':
                            pass
                        else:
                            call_create_data.append(False)
                            error = sheet.cell_value(i, 5)
                        mop = sheet.cell_value(i, 5)

                        try:
                            float(sheet.cell_value(i, 6))
                        except Exception as e:
                            # print('excpet')
                            print(e, '+++++++++++++++++')
                            call_create_data.append(False)
                            error = sheet.cell_value(i, 6)
                        if mop == "Cash on Delivery":
                            parcel_total = sheet.cell_value(i, 6)
                        else:
                            parcel_total = 0

                        contact_no = str(sheet.cell_value(i, 1))
                        r_cno = contact_no.split(".")[0]
                        # print(error_name, '============================')
                        # print(error)

                        shipments.append({
                            'RECEIVER_NAME': sheet.cell_value(i, 0),
                            'RECEIVER_CONTACT_NUMBER': str(r_cno),
                            'DROPOFF_CITY': sheet.cell_value(i, 2),
                            'DROPOFF_STREET_ADDRESS': sheet.cell_value(i, 3),
                            'PARCEL_TYPE': sheet.cell_value(i, 4),
                            'MODE_OF_PAYMENT': sheet.cell_value(i, 5),
                            'PARCEL_TOTAL': parcel_total,
                            'REFERENCE_CODE_OPTIONAL': sheet.cell_value(i, 7),
                            'SHIPMENT_DESCRIPTION_OPTIONAL': sheet.cell_value(i, 8),
                            'error': error,
                            # Empty field error
                            'error_name': error_name,
                            'error_contact': err_contact,
                            'error_street_addr': err_street_addr,
                            'error_mop': err_mop,
                        })
                    else:
                        # print('break called ===============================')
                        break
            if False not in call_create_data:
                return render(self.request, self.template_name, {"form": self.form_class, "csv_data": shipments, 'message': 'success', "customer": self.customer, 'logistics_branch': logistics_branch})
            else:
                return render(self.request, self.template_name, {"form": self.form_class, "csv_data": shipments})
        else:
            return render(self.request, self.template_name, {"form": self.form_class, 'message': 'danger'})

        return super().form_valid(form)

    def form_invalid(self, form):
        print(form.errors, '___________')
        return super().form_invalid(form)


class AjaxCustomerBulkUploadShipmentView(CustomerRequiredMixin, View):

    def post(self, request, *args, **kwargs):
        customer = self.customer
        logistics_branch = self.request.POST.get('logistics_branch')
        single_data = self.request.POST.getlist('single_data[]')
        shipments_list = []
        logistics_branch = LogisticsBranch.objects.get(id=logistics_branch)
        for i in single_data:
            split_data = i.split('&;&')
            if split_data[6].casefold() == 'already paid':
                payment_status = True
                parcel_total = 0
                cod_handling_charge = 0
            elif split_data[6].casefold() == "cash on delivery":
                payment_status = False
                parcel_total = Decimal(split_data[7])
            else:
                payment_status = False
                parcel_total = Decimal(split_data[7])

            if split_data[8] == 'None':
                ven_ref_code = ''
            else:
                ven_ref_code = split_data[8]
            if split_data[9] == 'None':
                ship_des = ''
            else:
                ship_des = split_data[9]

            csv_city = split_data[3].strip()
            city_obj = City.objects.filter(name__iexact=csv_city).last()

            # shipment = {
            #     'shipment_origin': logistics_branch,
            #     'reference_code': '',
            #     'parcel_type': '',
            #     'shipment_status': "Shipment Requested",
            #     'customer': self.customer,
            #     'sender_city': self.customer.city,
            #     'sender_address': self.customer.address,
            #     'receiver_name': split_data[1],
            #     'receiver_contact_number': split_data[2],
            #     'receiver_address': split_data[4],
            #     'receiver_city': city_obj,
            #     'cod_value': parcel_total,
            #     'weight': Decimal(200),
            #     'length': Decimal(2),
            #     'breadth': Decimal(2),
            #     'height': Decimal(2),
            #     'cod_info': split_data[6],
            # }
            s = Shipment.objects.create(
                status='Active',
                shipment_origin=logistics_branch,
                reference_code='',
                parcel_type='',
                shipment_status="Shipment Requested",
                customer=self.customer,
                sender_city=self.customer.city,
                sender_address=self.customer.address,
                receiver_name=split_data[1],
                receiver_contact_number=split_data[2],
                receiver_address=split_data[4],
                receiver_city=city_obj,
                cod_value=parcel_total,
                weight=Decimal(200),
                length=Decimal(2),
                breadth=Decimal(2),
                height=Decimal(2),
                cod_info=split_data[6],)
            # shipments_list.append(Shipment(**shipment))
            s.tracking_code = 'SARATHI-PT-' + str(s.id)
            s.save()

        # bulk_shipments = Shipment.objects.bulk_create(shipments_list)
        # bulk_created_shipments = Shipment.objects.filter()
        # update_bulk_query = []
        # for s in bulk_created_shipments:
        #     code = 'SPMT-C1-' + str(s.id)
        #     s.tracking_code = code
        #     update_bulk_query.append(s)
        # print(update_bulk_query, '____________')
        # Shipment.objects.bulk_update(update_bulk_query, ['tracking_code'])

        messages.add_message(request, messages.SUCCESS,
                             "Bulk shipment upload was successfull")

        return JsonResponse({'message': 'success'})

# customer payment


class CustomerPaymentRequestListView(CustomerRequiredMixin, ListView):
    template_name = "customertemplates/payment/customerpaymentrequestlist.html"
    context_object_name = "payment_request_list"
    paginate_by = 25

    def get_queryset(self):
        queryset = CustomerPaymentRequest.objects.filter(
            logistics_customer=self.customer)
        self.status = "all"
        if "search" in self.request.GET:
            search = self.request.GET.get("search", "")
            queryset = queryset.filter(
                Q(shipments__tracking_code=search) | Q(request_amount=search))
        elif 'status' in self.request.GET:
            status = self.request.GET.get("status")
            if status == "unsettled":
                queryset = queryset.exclude(is_settled=True)
                self.status = "unsettled"
            elif status == "settled":
                queryset = queryset.filter(is_settled=True)
                self.status = "settled"
        return queryset.order_by("-id")

    def get_context_data(self, **kwargs):
        all_requests = CustomerPaymentRequest.objects.filter(
            logistics_customer=self.customer)
        context = super().get_context_data(**kwargs)
        context["settled_requests"] = all_requests.filter(is_settled=True)
        context["unsettled_requests"] = all_requests.exclude(is_settled=True)
        context["all_requests"] = all_requests
        context["status"] = self.status
        context["current_balance"] = get_current_balance(self.customer)

        return context


class CustomerPaymentRequestCreateView(CustomerRequiredMixin, TemplateView):
    template_name = "customertemplates/payment/customerpaymentrequestcreate.html"

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            branch_id = request.GET.get("branch_id")
            branch = self.customer.connected_logistics.get(id=branch_id)
            shipments = Shipment.objects.filter(
                customer=self.customer, shipment_status__in=customer_completed_charge_sl, service_payment_status=False)
            current_balance = get_current_balance(
                self.customer, shipment_origin=branch)
            unsettled_requests_exists = CustomerPaymentRequest.objects.filter(
                status="Active", logistics_customer=self.customer, logistics_branch=branch, is_settled=False).exists()
            return JsonResponse({"status": "success", "branch_name": f'{branch.branch_city} ({branch.logistics_company.company_name})', "current_balance": current_balance, "shipment_count": shipments.count(), 'unsettled_requests_exists': unsettled_requests_exists})

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["payment_request_form"] = CustomerPaymentRequestForm(
            customer=self.customer)
        return context

    def post(self, request, *args, **kwargs):
        payment_request_form = CustomerPaymentRequestForm(
            self.customer, request.POST)
        if payment_request_form.is_valid():
            branch = payment_request_form.cleaned_data.get("logistics_branch")
            shipments = Shipment.objects.filter(
                customer=self.customer, shipment_status__in=customer_completed_charge_sl, service_payment_status=False)
            current_balance = get_current_balance(
                self.customer, shipment_origin=branch)
            if CustomerPaymentRequest.objects.filter(status="Active", logistics_customer=self.customer, logistics_branch=branch, is_settled=False).exists():
                messages.error(
                    request, "You have previous unsettled request left. Please contact your service provider or raise an issue.")
                return redirect("lmsapp:customerpaymentrequestlist")
            else:
                if current_balance > 0 and shipments.count() > 0:
                    payment_request_form.instance.logistics_customer = self.customer
                    cpr = payment_request_form.save()
                    cpr.status = "Active"
                    cpr.shipments.add(*shipments)
                    cpr.request_amount = current_balance

                    cpr.save()
                    messages.success(
                        request, "Payment Request Created Successfully.")
                else:
                    messages.error(
                        request, "You don't have balance in your wallet.")
                    return redirect("lmsapp:customerpaymentrequestlist")

        else:
            messages.error(
                request, "Ops, Something went wront, please try again later.")
            print(payment_request_form.errors)
            return redirect("lmsapp:customerpaymentrequestlist")
        return redirect("lmsapp:customerpaymentrequestlist")


class CustomerPaymentListView(CustomerRequiredMixin, ListView):
    template_name = "customertemplates/payment/customerpaymentlist.html"
    context_object_name = "payment_list"
    paginate_by = 100

    def get_queryset(self):
        payment_list = CustomerPayment.objects.filter(
            status="Active", logistics_customer=self.customer).order_by("-id")
        if "search" in self.request.GET:
            keyword = self.request.GET.get('search', "")
            payment_list = payment_list.filter(Q(logistics_branch__logistics_company__company_name__icontains=keyword) | Q(
                payment_method__account_in__icontains=keyword))
        return payment_list


class CustomerPaymentDetailView(CustomerRequiredMixin, TemplateView):
    template_name = "customertemplates/payment/customerpaymentdetail.html"

    def get(self, request, *args, **kwargs):
        try:
            self.payment = CustomerPayment.objects.get(
                logistics_customer=self.customer, id=self.kwargs.get("pk"))
        except Exception as e:
            print(e)
            messages.error(
                request, "Ops! You are not authorized to view this page.")
            return redirect("lmsapp:customerpaymentlist")
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["payment"] = self.payment
        return context


# notification list and click redirection

class CustomerNotificationListView(CustomerRequiredMixin, ListView):
    template_name = "customertemplates/extra/customernotificationlist.html"
    context_object_name = "notification_list"
    paginate_by = 50

    def get_queryset(self):
        notification_list = CustomerNotification.objects.filter(
            logistics_customer=self.customer, is_for_customer=True).order_by("-id")
        return notification_list


class CustomerNotificationClickView(CustomerRequiredMixin, View):
    def get(self, request, pk):
        try:
            notification = CustomerNotification.objects.get(
                id=pk, logistics_customer=self.customer, is_for_customer=True)
            if not notification.is_seen:
                notification.is_seen = True
                notification.save()
        except Exception as e:
            print(e)
            return redirect("lmsapp:customerdashboard")
        return redirect(notification.redirection_link)


# 3 branch views ------------------------------------------------------------------------
# 3 branch views ------------------------------------------------------------------------
# 3 branch views ------------------------------------------------------------------------
# 3 branch views ------------------------------------------------------------------------


class LogisticsLoginView(TemplateView):
    template_name = "logisticstemplates/logisticslogin.html"

    def post(self, request, *args, **kwargs):
        login_form = LogisticsLoginForm(request.POST)
        if login_form.is_valid():
            email = login_form.cleaned_data.get("email")
            password = login_form.cleaned_data.get("password")
            user = authenticate(username=email, password=password)
            try:
                operator = user.branchoperator
                login(request, user)
                messages.success(request, "You are logged in successfully.")
                return redirect("lmsapp:logisticsdashboard")
            except Exception as e:
                print(e)
                return render(request, self.template_name, {"error": "Invalid username or password"})
        else:
            print(login_form.errors)
            return render(request, self.template_name, {"error": "Invalid username or password"})


class LogisticsLogoutView(LogisticsRequiredMixin, View):
    def get(self, request):
        logout(request)
        return redirect("lmsapp:logisticslogin")


class LogisticsDashboardView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/logisticsdashboard.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["customer_count"] = self.branch.logisticscustomer_set.all().count()
        context["pending_shipment_count"] = Shipment.objects.filter(
            shipment_origin=self.branch, shipment_status="Shipment Requested").count()
        context["payment_request_count"] = CustomerPaymentRequest.objects.filter(
            logistics_branch=self.branch, is_settled=False).count()
        context["ticket_count"] = CustomerSupportTicket.objects.filter(
            raised_to=self.branch).exclude(ticket_status="Solved").count()
        return context


class LogisticsDashboardAjaxView(LogisticsRequiredMixin, View):
    def get(self, request):
        query = request.GET.get("query")
        if query == "weekly_report":
            days = []
            data = []
            today = timezone.localtime(timezone.now()).date()
            start_date = today - timedelta(days=30)
            for i in range(30):
                date = start_date + timedelta(days=i)
                shipments = Shipment.objects.filter(
                    shipment_origin=self.branch, created_at__date=date).count()
                days.append(date)
                data.append(shipments)

            return JsonResponse({"days": days, "data": data})

        return super().get(request)


# shipment management

class LogisticsShipmentCreateView(LogisticsRequiredMixin, CreateView):
    template_name = "logisticstemplates/shipment/logisticsshipmentcreate.html"
    form_class = LogisticsShipmentCreateForm
    success_url = reverse_lazy("lmsapp:logisticsshipmentlist")

    def form_valid(self, form):
        sz_id = self.request.POST.get("shipping_zone")
        try:
            sz = LogisticsBranchShippingZone.objects.get(id=sz_id)
        except Exception as e:
            print('LogisticsShipmentCreateView', e)
            return render(self.request, self.template_name, {"error": "Delivery not available at the selected location for now. Please contact logistics or select a different city", "form": self.form_class})
        form.instance.status = "Active"
        form.instance.shipment_status = "Shipment Requested"
        form.instance.shipment_origin = self.branch

        # sender information
        form.instance.customer = form.cleaned_data.get(
            "customer")
        form.instance.sender_city = self.branch.branch_city
        form.instance.sender_address = self.branch.branch_address
        form.instance.sender_contact_number = self.branch.branch_contact
        form.instance.sender_email = self.branch.branch_email

        # cost and payment information
        cod_value = form.cleaned_data.get(
            "cod_value")
        szdt = sz.shippingzonedeliverytype_set.filter(
            delivery_type__delivery_type__in=["Standard", "Standard Delivery"]).last()
        form.instance.delivery_type = szdt
        form.instance.shipping_charge = szdt.shipping_charge_per_kg
        form.instance.cod_handling_charge = szdt.cod_handling_charge * cod_value / 100
        form.instance.rejection_handling_charge = szdt.cod_handling_charge * cod_value / 100
        form.instance.pickup_charge = 0
        form.instance.dropoff_charge = 0
        form.instance.extra_charge = 0
        form.instance.service_payment_status = False
        shipment = form.save()
        if cod_value > 0:
            shipment.cod_info = "Cash on Delivery"
        else:
            shipment.cod_info = "Already Paid"
        shipment.tracking_code = f"SARATHI-PT-{shipment.id}"
        messages.success(self.request, "Shipment uploaded successfully.")
        ShipmentRemark.objects.create(
            status="Active", shipment=shipment, remarks='Uploaded by Service Provider', remarks_by=self.branch, is_private=False, remarks_written_by=self.operator)
        redirection_link = '/customer/shipment-'+shipment.tracking_code+'-detail/'
        CustomerNotification.objects.create(
            status="Active", logistics_branch=self.branch, logistics_customer=shipment.customer, title="Shipment Created By Service Provider", description="Shipment:" + shipment.tracking_code+" Created by Service Provider", redirection_link=redirection_link)
        return super().form_valid(form)

    def form_invalid(self, form):
        print(form.errors)
        return super().form_invalid(form)


class LogisticsAjaxGetSZSView(LogisticsRequiredMixin, TemplateView):
    template_name = "customertemplates/shipment/customerajaxgetszs.html"

    def get_context_data(self, **kwargs):
        city_id = self.request.GET.get('city_id', None)
        shipping_zones = LogisticsBranchShippingZone.objects.filter(logistics_branch=self.branch, cities__id=city_id).distinct()
        context = {
            "shipping_zones": shipping_zones
        }
        return context

class LogisticsShipmentListView(LogisticsRequiredMixin, ListView):
    template_name = "logisticstemplates/shipment/logisticsshipmentlist.html"
    context_object_name = "shipments"
    paginate_by = 100

    def get_queryset(self):
        origin_shipments = Shipment.objects.filter(shipment_origin=self.branch)
        self.pending_shipments = origin_shipments.filter(
            shipment_status="Shipment Requested")
        self.pickupassigned_shipments = origin_shipments.filter(
            shipment_status__in=["Rider Assigned for Pickup", "Rider Picked up the Shipment"])

        shipments = Shipment.objects.filter(Q(shipment_origin=self.branch) | Q(
            current_logistics=self.branch, shipmentbundleshipment__shipment_bundle__is_confirmed=True, shipmentbundleshipment__current_sbs=True, is_reverse_logistics=False)).distinct()

        self.arrived_shipments = shipments.filter(Q(shipment_origin=self.branch, shipment_status__in=[
                                                  "Shipment Arrived at Warehouse", "Sender at Logistics with Shipment"], current_logistics=None) | Q(current_logistics=self.branch, shipmentbundleshipment__shipment_bundle__is_confirmed=True, shipmentbundleshipment__current_sbs=True, shipment_status='Shipment Delivered to the DC', shipmentbundleshipment__shipment_status='Shipment Delivered to the DC', is_reverse_logistics=False, shipmentbundleshipment__is_reverse_logistics=False))
        self.dropoffassigned_shipments = shipments.filter(Q(shipment_origin=self.branch, shipment_status__in=[
            "Rider Assigned for Dispatch to the Receiver"], current_logistics=None) | Q(current_logistics=self.branch, shipmentbundleshipment__shipment_bundle__is_confirmed=True, shipmentbundleshipment__current_sbs=True, shipment_status='Rider Assigned for Dispatch to the Receiver', shipmentbundleshipment__shipment_status='Shipment Delivered to the DC'))
        self.dispatched_shipments = shipments.filter(Q(shipment_origin=self.branch, shipment_status__in=[
            "Rider Picked up the Shipment for Dispatch"], current_logistics=None) | Q(current_logistics=self.branch, shipmentbundleshipment__shipment_bundle__is_confirmed=True, shipmentbundleshipment__current_sbs=True, shipment_status='Rider Picked up the Shipment for Dispatch', shipmentbundleshipment__shipment_status='Shipment Delivered to the DC'))

        shipments = Shipment.objects.filter(Q(shipment_origin=self.branch) | Q(
            shipmentbundleshipment__shipment_bundle__is_confirmed=True, shipmentbundleshipment__destination_branch=self.branch)).distinct()

        self.delivered_shipments = shipments.filter(
            shipment_status__in=["Shipment Delivered to the Receiver"])

        self.rejected_shipments = shipments.filter(Q(shipment_origin=self.branch, shipment_status__in=["Shipment Rejected", "Shipment Returned to Logistics"], current_logistics=None) | Q(
            shipmentbundleshipment__current_sbs=True, shipment_status__in=["Shipment Rejected", "Shipment Returned to Logistics", "Shipment Delivered to the DC"], current_logistics=self.branch))
        self.readytoreturn_shipments = shipments.filter(Q(current_logistics=None) | Q(
            current_logistics=self.branch), shipment_origin=self.branch, shipment_status__in=["Ready to Return to Sender"])
        self.returntobranch = shipments.filter(current_logistics=self.branch, shipmentbundleshipment__shipment_bundle__is_confirmed=True, shipmentbundleshipment__destination_branch=self.branch,
                                               shipmentbundleshipment__current_sbs=True, shipment_status__in=["Ready to Return to Sender"]).exclude(shipment_origin=self.branch)
        self.return_count = shipments.filter(Q(shipment_origin=self.branch, shipment_status__in=["Ready to Return to Sender"], current_logistics=None) | Q(
            current_logistics=self.branch, shipmentbundleshipment__shipment_bundle__is_confirmed=True, shipmentbundleshipment__destination_branch=self.branch, shipmentbundleshipment__current_sbs=True, shipment_status__in=["Ready to Return to Sender"]))
        self.returnassigned_shipments = shipments.filter(
            shipment_origin=self.branch, shipment_status__in=["Rider Assigned for Returning to the Sender"])
        self.returning_shipments = shipments.filter(
            shipment_origin=self.branch, shipment_status__in=["Rider Picked up the Shipment for Return"])
        self.returned_shipments = shipments.filter(
            shipment_status__in=["Shipment Returned to the Sender"])
        self.canceled_shipments = shipments.filter(
            shipment_status__in=["Shipment Canceled"])
        self.all_shipments = shipments
        if "status" in self.request.GET:
            self.status = self.request.GET.get("status")
            if self.status in ["Pending", "pending"]:
                self.title = "Pending"
                queryset = self.pending_shipments
            elif self.status in ["pickupassigned"]:
                self.title = "Pickup Assigned"
                queryset = self.pickupassigned_shipments
            elif self.status in ["arrived", "Arrived"]:
                self.title = "Arrived"
                queryset = self.arrived_shipments
            elif self.status in ["dropoffassigned"]:
                self.title = "Dropoff Assigned"
                queryset = self.dropoffassigned_shipments
            elif self.status in ["dispatched"]:
                self.title = "Dispatched"
                queryset = self.dispatched_shipments
            elif self.status in ["delivered"]:
                self.title = "Delivered"
                queryset = self.delivered_shipments
            elif self.status in ["rejected"]:
                self.title = "Rejected"
                queryset = self.rejected_shipments
            elif self.status in ["readytoreturn"]:
                self.title = "Ready to Return"
                queryset = self.readytoreturn_shipments
            elif self.status in ['returntobranch']:
                self.title = "Ready to Return (RTB)"
                queryset = self.returntobranch
            elif self.status in ["returnassigned"]:
                self.title = "Assigned for Return"
                queryset = self.returnassigned_shipments
            elif self.status in ["returning"]:
                self.title = "Returning"
                queryset = self.returning_shipments
            elif self.status in ["returned"]:
                self.title = "Returned"
                queryset = self.returned_shipments
            elif self.status in ["all"]:
                self.title = "All"
                queryset = self.all_shipments
            elif self.status in ["canceled"]:
                self.title = "Canceled"
                queryset = self.canceled_shipments
            else:
                self.status = "pending"
                self.title = "Pending"
                queryset = self.pending_shipments
        elif "search" in self.request.GET:
                keyword = self.request.GET.get('search')
                self.title = "Searched Results with keyword: " + " ' "+ keyword + " ' "
                self.status = 'search'
                search_shipments = self.all_shipments.filter(Q(tracking_code__icontains=keyword) | Q(receiver_name__icontains=keyword) | Q(receiver_contact_number__icontains=keyword) | Q(receiver_city__name__icontains=keyword) | Q(reference_code__icontains=keyword))
                queryset = search_shipments
        else:
            self.status = "pending"
            self.title = "Pending"
            queryset = self.pending_shipments
        return queryset.order_by("-id")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["all_shipment_data"] = [
            {"title": "Pending", "status": "pending",
                "shipment_count": self.pending_shipments},
            {"title": "Pick Up Assigned", "status": "pickupassigned",
             "shipment_count": self.pickupassigned_shipments},
            {"title": "Arrived", "status": "arrived",
             "shipment_count": self.arrived_shipments},
            {"title": "Drop Off Assigned", "status": "dropoffassigned",
             "shipment_count": self.dropoffassigned_shipments},
            {"title": "Dispatched", "status": "dispatched",
             "shipment_count": self.dispatched_shipments},
            {"title": "Delivered", "status": "delivered",
             "shipment_count": self.delivered_shipments},
            {"title": "Rejected", "status": "rejected",
             "shipment_count": self.rejected_shipments},
            {"title": "Ready to Return", "status": "readytoreturn",
             "shipment_count": self.return_count},
            {"title": "Return Assigned", "status": "returnassigned",
             "shipment_count": self.returnassigned_shipments},
            {"title": "Returning", "status": "returning",
             "shipment_count": self.returning_shipments},
            {"title": "Returned", "status": "returned",
             "shipment_count": self.returned_shipments},
            {"title": "Canceled", "status": "canceled", "shipment_count": self.canceled_shipments},
            {"title": "All", "status": "all", "shipment_count": self.all_shipments},
        ]
        context["status"] = self.status
        context["title"] = self.title
        context["delivery_persons"] = DeliveryPerson.objects.filter(
            logistics_branch=self.branch)
        context["available_bundles"] = ShipmentBundle.objects.filter(
            is_sent=False, source_branch=self.branch)

        return context


class LogisticsCustomShipmentListView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/shipment/logisticscustomshipmentlist.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["delivery_persons"] = DeliveryPerson.objects.filter(status="Active", logistics_branch=self.branch)
        context["customers"] = LogisticsCustomer.objects.filter(status="Active")

        return context

class AjaxLogisticsCustomShipmentListView(LogisticsRequiredMixin, View):
    
    def post(self, request, *args, **kwargs):
        # customer = self.customer
        status = self.request.POST.getlist('status[]')
        customer_id = self.request.POST.get('customer_id')
        rider_id = self.request.POST.get('rider_id')
        shipment_status = []
        if '0' in status:
            shipment_status.append('Shipment Requested')
        if '1' in status:
            shipment_status.append('Rider Assigned for Pickup')
        if '2' in status:
            shipment_status.append('Shipment Arrived at Warehouse')
        if '3' in status:
            shipment_status.append('Rider Assigned for Dispatch to the Receiver')
        if '4' in status:
            shipment_status.append('Rider Picked up the Shipment for Dispatch')
        if '5' in status:
            shipment_status.append('Shipment Delivered to the Receiver')
        if '6' in status:
            shipment_status.append('Shipment Rejected')
        if '7' in status:
            shipment_status.append('Ready to Return to Sender')
        if '8' in status:
            shipment_status.append('Rider Assigned for Returning to the Sender')
        if '9' in status:
            shipment_status.append('Rider Picked up the Shipment for Return')
        if '10' in status:
            shipment_status.append('Shipment Returned to the Sender')

        if customer_id != 0:
            try:
                customer = LogisticsCustomer.objects.get(status="Active", id=customer_id)
            except:
                customer = None
        else:
            customer = None
        if rider_id != 0:
            try:
                rider = DeliveryPerson.objects.get(status="Active", id=rider_id)
            except:
                rider = None
        else:
            rider = None

        shipments = Shipment.objects.filter(status="Active", shipment_origin=self.branch, shipment_status__in=shipment_status)
        if customer == None and rider == None:
            shipments = shipments
        elif customer == None and rider != None:
            new_shipments = []
            dts = DeliveryTask.objects.filter(status="Active", delivery_person=rider, task_status__in=['Assigned', 'Started', 'Doing'])
            for dt in dts:
                if dt.shipment in shipments and dt.shipment not in new_shipments:
                    new_shipments.append(dt.shipment)
            shipments = new_shipments
        elif customer != None and rider == None:
            shipments = shipments.filter(customer=customer)
        else:
            new_shipments = []
            dts = DeliveryTask.objects.filter(status="Active", delivery_person=rider, task_status__in=['Assigned', 'Started', 'Doing'])
            shipments = Shipment.objects.filter(status="Active", shipment_origin=self.branch, shipment_status__in=shipment_status, customer=customer)
            for dt in dts:
                if dt.shipment in shipments and dt.shipment not in new_shipments:
                    new_shipments.append(dt.shipment)
            shipments = new_shipments
             
        context = {
            'message': 'success',
            'shipments': shipments,
            'logged_in_operator': self.operator,
        }
        return render(request, "logisticstemplates/shipment/ajaxlogisticscustomshipmentlist.html", context)


class AjaxLogisticsCustomShipmentTableListView(LogisticsRequiredMixin, TemplateView):
    template_name = 'logisticstemplates/shipment/logisticscustomshipmenttablelist.html'

    def post(self, request, *args, **kwargs):
        shipments_ids = json.loads(self.request.POST.get("shipment_ids"))
        print(shipments_ids, '+++')
        context = {
            'date_time': timezone.localtime(timezone.now()),
            'branch': self.branch,
            'org': LogisticsOrganizationalInformation.objects.filter(status="Active").first(),
            'shipments': Shipment.objects.filter(status="Active", id__in=shipments_ids, shipment_origin=self.branch)
        }

        return render(request, 'logisticstemplates/shipment/logisticscustomshipmenttablelist.html', context)


class LogisticsShipmentListTableView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/shipment/test.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        shipments = Shipment.objects.filter(Q(shipment_origin=self.branch) | Q(current_logistics=self.branch, shipmentbundleshipment__shipment_bundle__is_confirmed=True, shipmentbundleshipment__current_sbs=True, is_reverse_logistics=False)).distinct()
        # dropoff_assigned_shipments = shipments.filter(Q(shipment_origin=self.branch, shipment_status__in=[
        #                                           "Rider Assigned for Dispatch to the Receiver"], current_logistics=None) | Q(current_logistics=self.branch, shipmentbundleshipment__shipment_bundle__is_confirmed=True, shipmentbundleshipment__current_sbs=True, shipment_status='Rider Assigned for Dispatch to the Receiver', shipmentbundleshipment__shipment_status='Shipment Delivered to the DC'))

        context['riders'] = DeliveryPerson.objects.filter(status="Active", logistics_branch=self.branch)
        # context['shipments'] = dropoff_assigned_shipments
            
        return context

    def post(self, request, *args, **kwargs):
        rider_id = request.POST.get('riderselect')
        delivery_type_id = request.POST.get('tasktypeselect')
        try:
            dp = DeliveryPerson.objects.get(id=rider_id, logistics_branch=self.branch)
        except Exception as e:
            print('LogisticsShipmentListTableView', e)
            return redirect("lmsapp:logisticsshipmentlisttable")
        
        riders = DeliveryPerson.objects.filter(status="Active", logistics_branch=self.branch)
        
        # shipments = Shipment.objects.filter(Q(shipment_origin=self.branch) | Q(current_logistics=self.branch, shipmentbundleshipment__shipment_bundle__is_confirmed=True, shipmentbundleshipment__current_sbs=True, is_reverse_logistics=False)).distinct()

        if delivery_type_id == '1':
            dts = DeliveryTask.objects.filter(status="Active", delivery_person=dp, task_status="Assigned", shipment__shipment_status="Rider Assigned for Dispatch to the Receiver")
            dt = 'Assigned For Delivery'
        else:
            dts = DeliveryTask.objects.filter(status="Active", delivery_person=dp, task_status="Assigned", shipment__shipment_status="Rider Assigned for Pickup")
            dt = 'Assigned For Pickup'

        org = LogisticsOrganizationalInformation.objects.filter(status="Active").first()


        return render(request, self.template_name, {"dts": dts, 'riders': riders, 'branch': self.branch, 'rider': dp, "org": org, 'dt': dt})   




class LogisticsCustomerShipmentListView(LogisticsRequiredMixin, ListView):
    template_name = "logisticstemplates/shipment/logisticsshipmentlist.html"
    context_object_name = "shipments"
    paginate_by = 100

    def get_queryset(self):
        self.customer = LogisticsCustomer.objects.get(id=self.kwargs['pk'])
        shipments = Shipment.objects.filter(
            shipment_origin=self.branch, customer=self.customer)
        self.pending_shipments = shipments.filter(
            shipment_status="Shipment Requested")
        self.pickupassigned_shipments = shipments.filter(
            shipment_status__in=["Rider Assigned for Pickup", "Rider Picked up the Shipment"])
        self.arrived_shipments = shipments.filter(shipment_status__in=[
                                                  "Shipment Arrived at Warehouse", "Sender at Logistics with Shipment"])
        self.dropoffassigned_shipments = shipments.filter(
            shipment_status__in=["Rider Assigned for Dispatch to the Receiver"])
        self.dispatched_shipments = shipments.filter(
            shipment_status__in=["Rider Picked up the Shipment for Dispatch"])
        self.delivered_shipments = shipments.filter(
            shipment_status__in=["Shipment Delivered to the Receiver"])
        self.rejected_shipments = shipments.filter(
            shipment_status__in=["Shipment Rejected", "Shipment Returned to Logistics"])
        self.readytoreturn_shipments = shipments.filter(
            shipment_status__in=["Ready to Return to Sender"])
        self.returnassigned_shipments = shipments.filter(
            shipment_status__in=["Rider Assigned for Returning to the Sender"])
        self.returning_shipments = shipments.filter(
            shipment_status__in=["Rider Picked up the Shipment for Return"])
        self.returned_shipments = shipments.filter(
            shipment_status__in=["Shipment Returned to the Sender"])
        self.all_shipments = shipments
        if "status" in self.request.GET:
            self.status = self.request.GET.get("status")
            if self.status in ["Pending", "pending"]:
                self.title = "Pending"
                queryset = self.pending_shipments
            elif self.status in ["pickupassigned"]:
                self.title = "Pickup Assigned"
                queryset = self.pickupassigned_shipments
            elif self.status in ["arrived", "Arrived"]:
                self.title = "Arrived"
                queryset = self.arrived_shipments
            elif self.status in ["dropoffassigned"]:
                self.title = "Dropoff Assigned"
                queryset = self.dropoffassigned_shipments
            elif self.status in ["dispatched"]:
                self.title = "Dispatched"
                queryset = self.dispatched_shipments
            elif self.status in ["delivered"]:
                self.title = "Delivered"
                queryset = self.delivered_shipments
            elif self.status in ["rejected"]:
                self.title = "Rejected"
                queryset = self.rejected_shipments
            elif self.status in ["readytoreturn"]:
                self.title = "Ready to Return"
                queryset = self.readytoreturn_shipments
            elif self.status in ["returnassigned"]:
                self.title = "Assigned for Return"
                queryset = self.returnassigned_shipments
            elif self.status in ["returning"]:
                self.title = "Returning"
                queryset = self.returning_shipments
            elif self.status in ["returned"]:
                self.title = "Returned"
                queryset = self.returned_shipments
            elif self.status in ["all"]:
                self.title = "All"
                queryset = self.all_shipments
            else:
                self.status = "pending"
                self.title = "Pending"
                queryset = self.pending_shipments
        else:
            self.status = "pending"
            self.title = "Pending"
            queryset = self.pending_shipments
        return queryset.order_by("-id")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["all_shipment_data"] = [
            {"title": "Pending", "status": "pending",
                "shipment_count": self.pending_shipments},
            {"title": "Pick Up Assigned", "status": "pickupassigned",
             "shipment_count": self.pickupassigned_shipments},
            {"title": "Arrived", "status": "arrived",
             "shipment_count": self.arrived_shipments},
            {"title": "Drop Off Assigned", "status": "dropoffassigned",
             "shipment_count": self.dropoffassigned_shipments},
            {"title": "Dispatched", "status": "dispatched",
             "shipment_count": self.dispatched_shipments},
            {"title": "Delivered", "status": "delivered",
             "shipment_count": self.delivered_shipments},
            {"title": "Rejected", "status": "rejected",
             "shipment_count": self.rejected_shipments},
            {"title": "Ready to Return", "status": "readytoreturn",
             "shipment_count": self.readytoreturn_shipments},
            {"title": "Return Assigned", "status": "returnassigned",
             "shipment_count": self.returnassigned_shipments},
            {"title": "Returning", "status": "returning",
             "shipment_count": self.returning_shipments},
            {"title": "Returned", "status": "returned",
             "shipment_count": self.returned_shipments},
            {"title": "All", "status": "all", "shipment_count": self.all_shipments},
        ]
        context["status"] = self.status
        context["title"] = self.title
        context["customer"] = self.customer
        context["delivery_persons"] = DeliveryPerson.objects.filter(
            logistics_branch=self.branch)

        return context


class AjaxLogisticsShipmentListVerifyView(LogisticsRequiredMixin, View):
    
    def post(self, request, *args, **kwargs):
        shipment_ids = request.POST.get("shipment_ids")
        action = self.request.POST.get("action")
        shipment_ids = shipment_ids.split(",")
        if action == 'confirm-delivered':
            shipments = Shipment.objects.filter(Q(id__in=shipment_ids, shipment_origin=self.branch, shipment_status="Rider Picked up the Shipment for Dispatch") | Q(
                    id__in=shipment_ids, current_logistics=self.branch, shipmentbundleshipment__current_sbs=True, shipment_status="Rider Picked up the Shipment for Dispatch")).distinct()
            shipment_count = shipments.count()
            cod_value = shipments.aggregate(total=Sum('cod_value'))['total'] or 0 

            return JsonResponse({"status": "success", "shipment_count": shipment_count, "cod_value": cod_value})

        elif action == 'assign-dropoff':
            shipments = Shipment.objects.filter(Q(id__in=shipment_ids, shipment_origin=self.branch, shipment_status__in=[
                                                    "Shipment Arrived at Warehouse", "Sender at Logistics with Shipment", "Shipment Rejected"], current_logistics=None) |
                                                    Q(id__in=shipment_ids, shipmentbundleshipment__current_sbs=True, current_logistics=self.branch, shipment_status__in=[
                                                        "Shipment Delivered to the DC", "Shipment Rejected"], shipmentbundleshipment__shipment_status='Shipment Delivered to the DC'))
            shipment_count = shipments.count()
            
            return JsonResponse({"status": "success", "shipment_count": shipment_count})

        elif action == 'confirm-dispatched':
            shipments = Shipment.objects.filter(Q(id__in=shipment_ids, shipment_origin=self.branch, shipment_status="Rider Assigned for Dispatch to the Receiver") | Q(
                id__in=shipment_ids, current_logistics=self.branch, shipmentbundleshipment__current_sbs=True, shipment_status="Rider Assigned for Dispatch to the Receiver")).distinct()
            
            shipment_count = shipments.count()
            
            return JsonResponse({"status": "success", "shipment_count": shipment_count})

        return JsonResponse({"status": "error"})


class LogisticsShipmentDetailView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/shipment/logisticsshipmentdetail.html"

    def get(self, request, *args, **kwargs):
        try:
            self.shipment = Shipment.objects.get(tracking_code=self.kwargs.get(
                "tracking_code"))
            logistics = []
            logistics.append(self.shipment.shipment_origin)
            for logistic in self.shipment.shipmentbundleshipment_set.filter(status="Active", shipment_bundle__is_confirmed=True, shipment_bundle__status="Active"):
                logistics.append(logistic.destination_branch)
            if self.branch not in logistics:
                messages.error(request, "No authorization.")
                return redirect("lmsapp:logisticsshipmentlist")
        except Exception as e:
            messages.error(request, "Shipment not found.")
            return redirect("lmsapp:logisticsshipmentlist")
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["shipment"] = self.shipment
        context["activities"] = ShipmentActivity.objects.filter(
            shipment=self.shipment)
        context["remarks_list"] = ShipmentRemark.objects.filter(
            shipment=self.shipment, status="Active").order_by("id")
        context["possible_rejection_conditions"] = possible_rejection_conditions
        return context

    def post(self, request, *args, **kwargs):
        shipment = Shipment.objects.get(
            tracking_code=self.kwargs.get("tracking_code"))
        remarks = request.POST.get("remarks")
        ShipmentRemark.objects.create(
            status="Active", shipment=shipment, remarks=remarks, remarks_by=self.branch, is_private=False, remarks_written_by=self.operator)
        redirection_link = '/customer/shipment-'+shipment.tracking_code+'-detail/'
        CustomerNotification.objects.create(
            status="Active", logistics_branch=self.branch, logistics_customer=shipment.customer, title="Shipment Remarks Added", description="Shipment:" + shipment.tracking_code+" Remarks Added", redirection_link=redirection_link)
        return redirect("lmsapp:logisticsshipmentdetail", tracking_code=self.kwargs.get("tracking_code"))


class LogisticsShipmentUpdateView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/shipment/logisticsshipmentupdate.html"

    def get(self, request, *args, **kwargs):
        try:
            self.shipment = Shipment.objects.get(tracking_code=self.kwargs.get(
                "tracking_code"), shipment_origin=self.branch)
        except Exception as e:
            messages.error(request, "Shipment not found.")
            return redirect("lmsapp:logisticsshipmentlist")
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form"] = LogisticsShipmentUpdateForm(instance=self.shipment)
        return context

    def post(self, request, *args, **kwargs):
        try:
            shipment = Shipment.objects.get(tracking_code=self.kwargs.get(
                "tracking_code"), shipment_origin=self.branch)
            shipment_form = LogisticsShipmentUpdateForm(request.POST, instance=shipment)

            if shipment_form.is_valid():
                shipment = shipment_form.save()
                if shipment.cod_value > 0:
                    shipment.cod_info = "Cash on Delivery"
                else:
                    shipment.cod_info = "Already Paid"
                shipment.save()

        except Exception as e:
            print(e)
            messages.error(request, "Shipment not found.")
            return redirect("lmsapp:logisticsshipmentlist")
        return redirect("lmsapp:logisticsshipmentdetail", tracking_code=shipment.tracking_code)


class LogisticsShipmentLabelView(LogisticsRequiredMixin, TemplateView):
    template_name = 'logisticstemplates/shipment/logisticsshippinglabel.html'

    def get(self, request, *args, **kwargs):
        try:
            self.shipment = Shipment.objects.get(tracking_code=self.kwargs.get(
                "tracking_code"), shipment_origin=self.branch)
        except Exception as e:
            messages.error(request, "Shipment not found.")
            return redirect("lmsapp:logisticsshipmentlist")
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['date_time'] = timezone.localtime(timezone.now())
        shipment = Shipment.objects.get(
            tracking_code=self.kwargs['tracking_code'])
        context['shipment'] = shipment
        context['logistics_branch'] = self.operator.logistics_branch

        return context


class LogisticsBulkShippingLabelView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/shipment/logisticsbulkshippinglabel.html"

    def post(self, request, *args, **kwargs):
        shipments_ids = json.loads(self.request.POST.get("shipment_ids"))
        context = {
            'date_time': timezone.localtime(timezone.now()),
            'shipmentlist': Shipment.objects.filter(id__in=shipments_ids),
            'logistics_branch': self.operator.logistics_branch
        }
        return render(request, self.template_name, context)


class LogisticsShipmentActionView(LogisticsRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        action = request.POST.get("action")
        if action == None:
            action = request.POST.get("action1")

        shipment_ids = request.POST.get("shipment_ids")
        if shipment_ids == None:
            shipment_ids = request.POST.get("shipment_ids2")
        
        shipment_ids = shipment_ids.split(",")
        if action == "assign-pickup":
            delivery_person_id = request.POST.get("delivery_person_id")
            task_details = request.POST.get("task_details", '')
            try:
                delivery_person = DeliveryPerson.objects.get(
                    id=delivery_person_id, logistics_branch=self.branch)
                shipments = Shipment.objects.filter(
                    id__in=shipment_ids, shipment_origin=self.branch, shipment_status="Shipment Requested")
                shipment_activities = []
                delivery_tasks = []
                if shipments.count() == len(shipment_ids):
                    for shipment in shipments:
                        # activity
                        shipment_activities.append(ShipmentActivity(status="Active", shipment=shipment, shipment_status="Rider Assigned for Pickup",
                                                   activity_by=self.operator, activity="Rider assigned for picking up the shipment.", rider=delivery_person))
                        # delivery task
                        delivery_tasks.append(DeliveryTask(shipment=shipment, delivery_person=delivery_person, task_type="Pick Up",
                                              task_status="Assigned", scheduled_date=timezone.localtime(timezone.now()), status="Active", task_details=task_details))
                    ShipmentActivity.objects.bulk_create(shipment_activities)
                    DeliveryTask.objects.bulk_create(delivery_tasks)
                    shipments.update(
                        shipment_status="Rider Assigned for Pickup")
                    messages.success(
                        request, "Delivery Person is assigned for the selected shipments.")
                else:
                    messages.error(
                        request, "Could not find some of the shipments. ")
            except Exception as e:
                print(e)
                messages.error(request, "Delivery person not found.")
        elif action == "confirm-arrived":
            shipments = Shipment.objects.filter(
                id__in=shipment_ids, shipment_origin=self.branch, shipment_status="Rider Assigned for Pickup")
            shipment_activities = []
            if shipments.count() == len(shipment_ids):
                for shipment in shipments:
                    delivery_person = shipment.shipmentactivity_set.last().rider
                    # activity
                    shipment_activities.append(ShipmentActivity(status="Active", shipment=shipment, shipment_status="Shipment Arrived at Warehouse",
                                               activity_by=self.operator, activity="Delivery Person Picked up and brought the shipment to warehouse.", rider=delivery_person))
                    # delivery task
                ShipmentActivity.objects.bulk_create(shipment_activities)
                DeliveryTask.objects.filter(shipment__id__in=shipment_ids, task_type="Pick Up", task_status="Assigned").update(
                    task_status="Completed Success", completed_date=timezone.localtime(timezone.now()))
                shipments.update(
                    shipment_status="Shipment Arrived at Warehouse")
                messages.success(
                    request, "Selected shipments status are now changed to Arrived at Warehouse.")
                return JsonResponse({"status": "success"})
            else:
                messages.error(
                    request, "Could not find some of the shipments. ")
                return JsonResponse({"status": "failure"})
        elif action == "assign-dropoff":
            delivery_person_id = request.POST.get("delivery_person_id")
            task_details = request.POST.get("task_details", '')
            try:
                delivery_person = DeliveryPerson.objects.get(
                    id=delivery_person_id, logistics_branch=self.branch)
                shipments = Shipment.objects.filter(Q(id__in=shipment_ids, shipment_origin=self.branch, shipment_status__in=[
                                                    "Shipment Arrived at Warehouse", "Sender at Logistics with Shipment", "Shipment Rejected"], current_logistics=None) |
                                                    Q(id__in=shipment_ids, shipmentbundleshipment__current_sbs=True, current_logistics=self.branch, shipment_status__in=[
                                                        "Shipment Delivered to the DC", "Shipment Rejected"], shipmentbundleshipment__shipment_status='Shipment Delivered to the DC'))
                shipment_activities = []
                delivery_tasks = []
                if shipments.count() == len(shipment_ids):
                    for shipment in shipments:
                        # activity
                        shipment_activities.append(ShipmentActivity(status="Active", shipment=shipment, shipment_status="Rider Assigned for Dispatch to the Receiver",
                                                   activity_by=self.operator, activity="Rider Assigned for Dispatch to the Receiver.", rider=delivery_person))
                        # delivery task
                        delivery_tasks.append(DeliveryTask(shipment=shipment, delivery_person=delivery_person, task_type="Drop Off",
                                              task_status="Assigned", scheduled_date=timezone.localtime(timezone.now()), status="Active", task_details=task_details))
                    ShipmentActivity.objects.bulk_create(shipment_activities)
                    DeliveryTask.objects.bulk_create(delivery_tasks)
                    shipments.update(
                        shipment_status="Rider Assigned for Dispatch to the Receiver")
                    messages.success(
                        request, "Delivery Person is assigned for the selected shipments.")
                else:
                    messages.error(
                        request, "Could not find some of the shipments. ")
            except Exception as e:
                print(e)
                messages.error(request, "Delivery person not found.")
        elif action == "confirm-dispatched":
            shipments = Shipment.objects.filter(Q(id__in=shipment_ids, shipment_origin=self.branch, shipment_status="Rider Assigned for Dispatch to the Receiver") | Q(
                id__in=shipment_ids, current_logistics=self.branch, shipmentbundleshipment__current_sbs=True, shipment_status="Rider Assigned for Dispatch to the Receiver")).distinct()
            shipment_activities = []
            if shipments.count() == len(shipment_ids):
                for shipment in shipments:
                    delivery_person = shipment.shipmentactivity_set.last().rider
                    # activity
                    shipment_activities.append(ShipmentActivity(status="Active", shipment=shipment, shipment_status="Rider Picked up the Shipment for Dispatch",
                                               activity_by=self.operator, activity="Delivery Person Picked up the shipment to deliver to the receiver.", rider=delivery_person))
                    # delivery task
                ShipmentActivity.objects.bulk_create(shipment_activities)
                DeliveryTask.objects.filter(
                    shipment__id__in=shipment_ids, task_type="Drop Off", task_status="Assigned").update(task_status="Started")
                shipments.update(
                    shipment_status="Rider Picked up the Shipment for Dispatch")
                messages.success(
                    request, "Selected shipments status are now changed to Dispatched to the Receiver.")
                return JsonResponse({"status": "success"})
            else:
                messages.error(
                    request, "Could not find some of the shipments. ")
                return JsonResponse({"status": "failure"})
        elif action == "confirm-delivered":
            shipments = Shipment.objects.filter(Q(id__in=shipment_ids, shipment_origin=self.branch, shipment_status="Rider Picked up the Shipment for Dispatch") | Q(
                id__in=shipment_ids, current_logistics=self.branch, shipmentbundleshipment__current_sbs=True, shipment_status="Rider Picked up the Shipment for Dispatch")).distinct()

            shipment_activities = []
            if shipments.count() == len(shipment_ids):
                for shipment in shipments:
                    delivery_person = shipment.shipmentactivity_set.last().rider
                    # activity
                    shipment_activities.append(ShipmentActivity(status="Active", shipment=shipment, shipment_status="Shipment Delivered to the Receiver",
                                               activity_by=self.operator, activity="Delivery Person delivered the shipments to the receiver.", rider=delivery_person))
                    # delivery task
                ShipmentActivity.objects.bulk_create(shipment_activities)
                DeliveryTask.objects.filter(
                    shipment__id__in=shipment_ids, task_type="Drop Off", task_status__in=["Started", "Doing"]).update(
                    task_status="Completed Success", completed_date=timezone.localtime(timezone.now()))
                shipments.update(
                    shipment_status="Shipment Delivered to the Receiver")
                messages.success(
                    request, "Selected shipments status are now changed to Delivered to the Receiver.")
                return JsonResponse({"status": "success"})
            else:
                messages.error(
                    request, "Could not find some of the shipments. ")
                return JsonResponse({"status": "failure"})
        elif action == "reject-shipment":
            rejection_remarks = request.POST.get("rejection_remarks", '')
            try:
                shipments = Shipment.objects.filter(Q(id__in=shipment_ids, shipment_origin=self.branch, shipment_status__in=possible_rejection_conditions) | Q(
                    id__in=shipment_ids, current_logistics=self.branch, shipmentbundleshipment__current_sbs=True, shipment_status__in=possible_rejection_conditions))
                print(shipments, '+_+_+_+_+_______')
                shipment_activities = []
                shipment_remarks = []
                if shipments.count() == len(shipment_ids):
                    for shipment in shipments:
                        delivery_person = shipment.shipmentactivity_set.last().rider
                        # activity
                        shipment_activities.append(ShipmentActivity(status="Active", shipment=shipment, shipment_status="Shipment Rejected",
                                                   activity_by=self.operator, activity=f"Shipment Rejected after {shipment.shipment_status}.", rider=delivery_person))
                        shipment_remarks.append(ShipmentRemark(
                            status="Active", shipment=shipment, remarks=rejection_remarks, remarks_by=self.branch, remarks_written_by=self.operator))
                        # delivery task
                    ShipmentActivity.objects.bulk_create(shipment_activities)
                    ShipmentRemark.objects.bulk_create(shipment_remarks)
                    shipments.update(shipment_status="Shipment Rejected")
                    DeliveryTask.objects.filter(
                    shipment__id__in=shipment_ids, task_type="Drop Off", task_status__in=["Assigned", "Started", "Doing"]).update(task_status="Completed Fail", completed_date=timezone.localtime(timezone.now()))
                    messages.success(
                        request, "Shipments Rejected successfully.")
                    if len(shipment_ids) == 1:
                        shipment = Shipment.objects.get(id=shipment_ids[0])
                        return redirect("lmsapp:logisticsshipmentdetail", tracking_code=shipment.tracking_code)
                else:
                    messages.error(
                        request, "Could not find some of the shipments. ")
            except Exception as e:
                print(e)
                messages.error(request, "Delivery person not found.")
        elif action == "confirm-readytoreturn":
            shipments = Shipment.objects.filter(Q(id__in=shipment_ids, shipment_origin=self.branch, shipment_status__in=[
                                                "Shipment Rejected", "Shipment Returned to Logistics", "Shipment Returned to the Logistics by DC"]) | Q(id__in=shipment_ids, current_logistics=self.branch, shipmentbundleshipment__current_sbs=True, shipment_status__in=[
                                                "Shipment Rejected", "Shipment Returned to Logistics", "Shipment Returned to the Logistics by DC"]) | Q(id__in=shipment_ids, current_logistics=self.branch, shipmentbundleshipment__current_sbs=True, shipment_status__in=[
                                                "Shipment Delivered to the DC"], is_reverse_logistics=True,shipmentbundleshipment__is_reverse_logistics=True))
            shipment_activities = []
            if shipments.count() == len(shipment_ids):
                for shipment in shipments:
                    # activity
                    shipment_activities.append(ShipmentActivity(status="Active", shipment=shipment, shipment_status="Ready to Return to Sender",
                                               activity_by=self.operator, activity="Shipment marked as Ready To Return to the Senders."))
                    # delivery task
                ShipmentActivity.objects.bulk_create(shipment_activities)
                shipments.update(
                    shipment_status="Ready to Return to Sender", is_reverse_logistics=True)
                messages.success(
                    request, "Selected shipments status are now changed to Ready To Return.")
                return JsonResponse({"status": "success"})
            else:
                messages.error(
                    request, "Could not find some of the shipments. ")
            return JsonResponse({"status": "failure"})
        elif action == "assign-return":
            delivery_person_id = request.POST.get("delivery_person_id")
            task_details = request.POST.get("task_details", '')
            try:
                delivery_person = DeliveryPerson.objects.get(
                    id=delivery_person_id, logistics_branch=self.branch)
                shipments = Shipment.objects.filter(
                    id__in=shipment_ids, shipment_origin=self.branch, shipment_status__in=["Ready to Return to Sender"])
                shipment_activities = []
                delivery_tasks = []
                if shipments.count() == len(shipment_ids):
                    for shipment in shipments:
                        # activity
                        shipment_activities.append(ShipmentActivity(status="Active", shipment=shipment, shipment_status="Rider Assigned for Returning to the Sender",
                                                   activity_by=self.operator, activity="Rider Assigned for Returning to the Sender.", rider=delivery_person))
                        # delivery task
                        delivery_tasks.append(DeliveryTask(shipment=shipment, delivery_person=delivery_person, task_type="Return",
                                              task_status="Assigned", scheduled_date=timezone.localtime(timezone.now()), status="Active", task_details=task_details))
                    ShipmentActivity.objects.bulk_create(shipment_activities)
                    DeliveryTask.objects.bulk_create(delivery_tasks)
                    shipments.update(
                        shipment_status="Rider Assigned for Returning to the Sender")
                    messages.success(
                        request, "Delivery Person is assigned for the selected shipments.")
                else:
                    messages.error(
                        request, "Could not find some of the shipments. ")
            except Exception as e:
                print(e)
                messages.error(request, "Delivery person not found.")
        elif action == "confirm-returning":
            shipments = Shipment.objects.filter(
                id__in=shipment_ids, shipment_origin=self.branch, shipment_status="Rider Assigned for Returning to the Sender")
            shipment_activities = []
            if shipments.count() == len(shipment_ids):
                for shipment in shipments:
                    delivery_person = shipment.shipmentactivity_set.last().rider
                    # activity
                    shipment_activities.append(ShipmentActivity(status="Active", shipment=shipment, shipment_status="Rider Picked up the Shipment for Return",
                                               activity_by=self.operator, activity="Delivery Person Picked up the shipment to return to the sender.", rider=delivery_person))
                    # delivery task
                ShipmentActivity.objects.bulk_create(shipment_activities)
                DeliveryTask.objects.filter(
                    shipment__id__in=shipment_ids, task_type="Return", task_status="Assigned").update(task_status="Doing")
                shipments.update(
                    shipment_status="Rider Picked up the Shipment for Return")
                messages.success(
                    request, "Selected shipments status are now changed to Returning to the Sender.")
                return JsonResponse({"status": "success"})
            else:
                messages.error(
                    request, "Could not find some of the shipments. ")
                return JsonResponse({"status": "failure"})
        elif action == "confirm-returned":
            shipments = Shipment.objects.filter(
                id__in=shipment_ids, shipment_origin=self.branch, shipment_status="Rider Picked up the Shipment for Return")
            shipment_activities = []
            if shipments.count() == len(shipment_ids):
                for shipment in shipments:
                    delivery_person = shipment.shipmentactivity_set.last().rider
                    # activity
                    shipment_activities.append(ShipmentActivity(status="Active", shipment=shipment, shipment_status="Shipment Returned to the Sender",
                                               activity_by=self.operator, activity="Shipment Returned to the Sender", rider=delivery_person))
                    # delivery task
                ShipmentActivity.objects.bulk_create(shipment_activities)
                DeliveryTask.objects.filter(shipment__id__in=shipment_ids, task_type="Drop Off", task_status="Doing").update(
                    task_status="Completed Success", completed_date=timezone.localtime(timezone.now()))
                shipments.update(
                    shipment_status="Shipment Returned to the Sender")
                messages.success(
                    request, "Selected shipments status are now changed to Returned to the Sender.")
                return JsonResponse({"status": "success"})
            else:
                messages.error(
                    request, "Could not find some of the shipments. ")
                return JsonResponse({"status": "failure"})
        
        elif action == 'change-to-arrived':
            change_remarks = request.POST.get("change_remarks", '')
            remarks_type = request.POST.get("remarks_type", '')
            try:
                shipments = Shipment.objects.filter(Q(id__in=shipment_ids, shipment_origin=self.branch) | Q(id__in=shipment_ids, current_logistics=self.branch, shipmentbundleshipment__current_sbs=True))
                shipment_activities = []
                shipment_remarks = []
                if remarks_type == 'private':
                    privacy = True
                else:
                    privacy = False

                if shipments.count() == len(shipment_ids):
                    for shipment in shipments:
                        # activity
                        shipment_activities.append(ShipmentActivity(status="Active", shipment=shipment, shipment_status="Shipment Arrived at Warehouse", activity_by=self.operator, activity=change_remarks))
                        shipment_remarks.append(ShipmentRemark(
                            status="Active", shipment=shipment, remarks=change_remarks, remarks_by=self.branch, remarks_written_by=self.operator, is_private=privacy))
                        # delivery task
                        last_dt = shipment.deliverytask_set.last()
                        last_dt.task_status = 'Completed Fail'
                        last_dt.save()
                    ShipmentActivity.objects.bulk_create(shipment_activities)
                    ShipmentRemark.objects.bulk_create(shipment_remarks)
                    shipments.update(shipment_status="Shipment Arrived at Warehouse")
                    DeliveryTask.objects.filter(
                    shipment__id__in=shipment_ids, task_type="Drop Off", task_status__in=["Assigned", "Started", "Doing"]).update(task_status="Completed Fail", completed_date=timezone.localtime(timezone.now()))
                    messages.success(
                        request, "Shipments status changed to 'ARRIVED TO WAREHOUSE' successfully.")
                    if len(shipment_ids) == 1:
                        shipment = Shipment.objects.get(id=shipment_ids[0])
                        return redirect("lmsapp:logisticsshipmentdetail", tracking_code=shipment.tracking_code)
                else:
                    messages.error(
                        request, "Could not find some of the shipments. ")
            except Exception as e:
                print(e)
                messages.error(request, "Something went wrong. Please refresh and try again or contact administrator.")
        else:
            messages.error(request, "Invalid Action")
        return redirect("lmsapp:logisticsshipmentlist")


class LogisticsShipmentToBundleView(LogisticsRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        shipment_ids = request.POST.get("shipment_ids1")
        shipment_ids = shipment_ids.split(",")
        bundle_id = request.POST.get("bundle_id")
        try:
            bundle = ShipmentBundle.objects.get(
                id=bundle_id, source_branch=self.branch)
            shipments = Shipment.objects.filter(Q(id__in=shipment_ids, shipment_origin=self.branch, shipment_status__in=[
                                                "Shipment Arrived at Warehouse", "Sender at Logistics with Shipment", "Shipment Rejected"]) | Q(id__in=shipment_ids, current_logistics=self.branch, shipment_status__in=[
                                                    "Shipment Delivered to the DC"]) | Q(id__in=shipment_ids, current_logistics=self.branch, shipment_status__in=['Ready to Return to Sender'], shipmentbundleshipment__current_sbs=True))
            shipment_activities = []
            shipment_bundle_shipment = []

            if shipments.count() == len(shipment_ids):
                for shipment in shipments:
                    shipment.shipment_status = "Rider Assigned for Dispatch to DC"
                    shipment.save()
                    # activity
                    shipment_activities.append(ShipmentActivity(status="Active", shipment=shipment, shipment_status=shipment.shipment_status,
                                               activity_by=self.operator, activity="Shipment is included in the consolidation "+bundle.bundle_code, involved_logistics=self.branch))

                    shipment_bundle_shipment.append(ShipmentBundleShipment(
                        status="Active", shipment_bundle=bundle, shipment=shipment,  source_branch=self.branch, cod_value=shipment.cod_value))

                ShipmentActivity.objects.bulk_create(shipment_activities)
                ShipmentBundleShipment.objects.bulk_create(
                    shipment_bundle_shipment)
                messages.success(
                    request, "Selected shipments added to a bundle.")
            else:
                messages.error(
                    request, "Could not add these shipments to a bundle. ")
        except Exception as e:
            print(e)
            messages.error(request, "Bundle not found.")

        return redirect("lmsapp:logisticsshipmentlist")


# bundle management

class LogisticsBundleListView(LogisticsRequiredMixin, ListView):
    template_name = "logisticstemplates/shipment/logisticsbundlelist.html"
    context_object_name = "bundle_list"
    paginate_by = 100

    def get_queryset(self):
        if self.request.GET.get('qrt') == 'processing':
            queryset = ShipmentBundle.objects.filter(
                source_branch=self.branch, is_sent=False)
        elif self.request.GET.get('qrt') == 'snt':
            queryset = ShipmentBundle.objects.filter(
                source_branch=self.branch, is_sent=True)
        elif self.request.GET.get('qrt') == 'rcvd':
            queryset = ShipmentBundle.objects.filter(
                destination_branch=self.branch, is_sent=True)
        else:
            queryset = ShipmentBundle.objects.filter(
                destination_branch=self.branch, is_sent=True, is_confirmed=True)

        return queryset.order_by("-id")


class LogisticsBundleCreateView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/shipment/logisticsbundlecreate.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["bundle_form"] = LogisticsBundleCreateForm(
            this_branch=self.branch)
        return context

    def post(self, request, *args, **kwargs):
        bundle_form = LogisticsBundleCreateForm(self.branch, request.POST)
        if bundle_form.is_valid():
            try:
                bundle_form.instance.status = "Active"
                bundle_form.instance.source_branch = self.branch
                bundle_form.instance.is_sent = False
                bundle_form.instance.current_sbs = False
                bundle = bundle_form.save()
                ShipmentBundleActivity.objects.create(
                    shipment_bundle=bundle, activity="Consolidation created successfully", activity_by=self.operator)
                # delivery task
                # if bundle.delivery_person:
                #     DeliveryTask.objects.create(
                #         status="Active", shipment_bundle=bundle, task_type="Bundle Drop Off", delivery_person=bundle.delivery_person, task_status="Assigned",
                #         task_details="Staff assigned to drop off the consolidation", scheduled_date=timezone.localtime(timezone.now()), assigned_by=self.operator)
                messages.success(
                    request, "Consolidation created successfully.")

            except Exception as e:
                print(e)
                messages.error(request, "Ops! Something went wrong.")
                return redirect("lmsapp:logisticsbundlelist")
        else:
            messages.error(request, "Ops! Something went wrong.")
            return redirect("lmsapp:logisticsbundlelist")
        return redirect("lmsapp:logisticsbundledetail", pk=bundle.id)


class LogisticsBundleDetailView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/shipment/logisticsbundledetail.html"

    def get(self, request, *args, **kwargs):
        try:
            self.bundle = ShipmentBundle.objects.get(Q(source_branch=self.branch) | Q(
                destination_branch=self.branch), id=self.kwargs.get("pk"))
        except Exception as e:
            print(e)
            messages.error(request, "Consolidation not found...")
            return redirect("lmsapp:logisticsbundlelist")
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["bundle"] = self.bundle
        context["sbs_list"] = self.bundle.shipmentbundleshipment_set.filter(
            status="Active")
        context["activities"] = self.bundle.shipmentbundleactivity_set.all()
        context["delivery_persons"] = DeliveryPerson.objects.filter(
            logistics_branch=self.branch)
        if DeliveryTask.objects.filter(shipment_bundle=self.bundle, task_type='Bundle Pick Up', task_status__in=['Assigned', 'Started', 'Doing', 'Completed Success']).exists():
            context['active_delivery_task'] = 'Assigned'
        return context

    def post(self, request, *args, **kwargs):
        try:
            bundle = ShipmentBundle.objects.get(Q(source_branch=self.branch) | Q(
                destination_branch=self.branch), id=self.kwargs.get("pk"))
            action = request.POST.get("action")
            if action == "consolidation-sent" and bundle.source_branch == self.branch:
                bundle.is_sent = True
                bundle.sent_date = timezone.localtime(timezone.now())
                bundle.save()
                ShipmentBundleActivity.objects.create(
                    status="Active", shipment_bundle=bundle, activity=f"Consolidation has been sent to the destination logistics", activity_by=self.operator)
                messages.success(
                    request, "Consolidation has been sent to the destination branch successfully.")
            elif action == "consolidation-received" and bundle.destination_branch == self.branch:
                bundle.is_delivered = True
                bundle.delivered_date = timezone.localtime(timezone.now())
                bundle.save()
                ShipmentBundleActivity.objects.create(
                    status="Active", shipment_bundle=bundle, activity=f"Consolidation has been received by the destination logistics", activity_by=self.operator)
                messages.success(
                    request, "Consolidation has been received by the Destination Logistics.")
            elif action == "consolidation-confirmed" and bundle.destination_branch == self.branch and bundle.is_confirmed is False:
                bundle.is_confirmed = True
                bundle.confirmed_date = timezone.localtime(timezone.now())
                bundle.save()
                ShipmentBundleActivity.objects.create(
                    status="Active", shipment_bundle=bundle, activity=f"Consolidation has been confirmed by the destination logistics", activity_by=self.operator)
                shipment_activities = []
                for sbs in bundle.shipmentbundleshipment_set.filter(status="Active"):
                    shipment = sbs.shipment
                    shipment.shipmentbundleshipment_set.update(
                        current_sbs=False)
                    sbs.shipment_status = "Shipment Delivered to the DC"
                    sbs.current_sbs = True
                    sbs.destination_branch = self.branch
                    if shipment.is_reverse_logistics:
                        sbs.is_reverse_logistics = True
                    sbs.save()
                    shipment.current_logistics = self.branch
                    print(shipment.current_logistics,
                          self.branch, '+++++++++++++++++++')
                    shipment.shipment_status = "Shipment Delivered to the DC"
                    shipment.save()
                    shipment_activities.append(ShipmentActivity(status="Active", shipment=shipment, shipment_status="Shipment Delivered to the DC",
                                               activity="Shipment has been delivered to the Distribution Center.", activity_by=self.operator, involved_logistics=self.branch))
                ShipmentActivity.objects.bulk_create(shipment_activities)
                if DeliveryTask.objects.filter(status="Active", shipment_bundle=bundle, task_type="Bundle Pick Up", task_status="Assigned").exists():
                    dt = DeliveryTask.objects.filter(
                        status="Active", shipment_bundle=bundle, task_type="Bundle Pick Up", task_status="Assigned").last()
                    dt.task_status = "Completed Success"
                    dt.completed_date = timezone.localtime(timezone.now())
                    dt.save()
                if bundle.is_delivered is False:
                    bundle.is_delivered = True
                    bundle.delivered_date = timezone.localtime(timezone.now())
                    bundle.save()
                messages.success(
                    request, "Consolidation confirmed successfully.")
            elif action == "assign-staff-to-pickup" and bundle.destination_branch == self.branch:
                rider = DeliveryPerson.objects.get(
                    id=request.POST.get("delivery_person_id"))
                task_details = request.POST.get("task_details")
                DeliveryTask.objects.create(
                    status="Active", shipment_bundle=bundle, delivery_person=rider, task_type="Bundle Pick Up",
                    task_status="Assigned", task_details=task_details, scheduled_date=timezone.localtime(timezone.now()), assigned_by=self.operator)
                messages.success(
                    request, "Delivery task created successfully for bundle pickup.")
                return redirect("lmsapp:logisticsbundledetail", pk=bundle.id)
            else:
                messages.error(request, "Invalid Operation...")
                return JsonResponse({"status": "error"})
        except Exception as e:
            print(e)
            messages.error(request, "Consolidation not found...")
            return JsonResponse({"status": "error"})
        return JsonResponse({"status": "success"})


class LogisticsBundleUpdateView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/shipment/logisticsbundleupdate.html"

    def get(self, request, *args, **kwargs):
        try:
            self.bundle = ShipmentBundle.objects.get(
                source_branch=self.branch, id=self.kwargs.get("pk"))
        except Exception as e:
            print(e)
            messages.error(request, "Consolidation not found...")
            return redirect("lmsapp:logisticsbundlelist")
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["bundle"] = self.bundle
        context["sbs_list"] = self.bundle.shipmentbundleshipment_set.filter(
            status="Active")
        context["bundle_form"] = LogisticsBundleUpdateForm(
            this_branch=self.branch, instance=self.bundle)
        return context

    def post(self, request, *args, **kwargs):
        try:
            bundle = ShipmentBundle.objects.get(
                source_branch=self.branch, id=self.kwargs.get("pk"))
            # dp = bundle.delivery_person
            bundle_form = LogisticsBundleUpdateForm(
                self.branch, request.POST, instance=bundle)
            if bundle_form.is_valid():
                bundle = bundle_form.save()
                # if dp is None and bundle.delivery_person:
                #     DeliveryTask.objects.create(
                #         status="Active", shipment_bundle=bundle, task_type="Bundle Drop Off", delivery_person=bundle.delivery_person, task_status="Assigned",
                #         task_details="Staff assigned to drop off the consolidation", scheduled_date=timezone.localtime(timezone.now()), assigned_by=self.operator)
                # elif dp and bundle.delivery_person:
                #     dt = DeliveryTask.objects.get(
                #         shipment_bundle=bundle, delivery_person=dp, delivery_person__logistics_branch=self.branch)
                #     if dt.delivery_person != bundle.delivery_person:
                #         ShipmentBundleActivity.objects.create(
                #             status="Active", shipment_bundle=bundle, activity=f"Delivery person was changed from {dp.user.username} to {bundle.delivery_person.user.username}", activity_by=self.operator)

                messages.success(
                    request, "Consolidation updated successfully.")
            else:
                messages.error(request, "Ops! Something went wrong.")
                return redirect("lmsapp:logisticsbundlelist")
        except Exception as e:
            print(e)
            messages.error(request, "Ops! Something went wrong.")
            return redirect("lmsapp:logisticsbundlelist")
        return redirect("lmsapp:logisticsbundledetail", pk=bundle.id)


class LogisticsBundleShipmentView(LogisticsRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        try:
            tracking_code = request.GET.get("tracking_code")
            shipment = Shipment.objects.get(tracking_code=tracking_code)
            bundle = ShipmentBundle.objects.get(
                source_branch=self.branch, id=self.kwargs.get("pk"))
            shipment_json = {
                'id': shipment.id,
                'tracking_code': shipment.tracking_code,
                'shipment_status': shipment.shipment_status,
                'receiver_city': shipment.receiver_city.name + "(" + shipment.receiver_city.district.name + ")",
                'receiver_address': shipment.receiver_address
            }
            return JsonResponse({"status": "success", "data": shipment_json})
        except Exception as e:
            print(e)
            return JsonResponse({"status": "failure", "message": "Shipment not found."})

    def post(self, request, *args, **kwargs):
        try:
            tracking_code = request.POST.get("tracking_code")
            action = request.POST.get("action")
            shipment = Shipment.objects.get(
                tracking_code=tracking_code, shipment_origin=self.branch)
            bundle = ShipmentBundle.objects.get(
                source_branch=self.branch, id=self.kwargs.get("pk"))
            if action == "add-to-bundle":
                if shipment.shipment_status in ["Shipment Arrived at Warehouse", "Sender at Logistics with Shipment"]:
                    if ShipmentBundleShipment.objects.filter(status="Active", shipment_bundle=bundle, shipment=shipment).exists() is False:
                        shipment.shipment_status = "Rider Assigned for Dispatch to DC"
                        shipment.save()
                        ShipmentActivity.objects.create(shipment=shipment, shipment_status=shipment.shipment_status,
                                                        activity="Shipment is included in the consolidation.", status="Active", activity_by=self.operator, involved_logistics=self.branch)
                        ShipmentBundleShipment.objects.create(
                            shipment_bundle=bundle, shipment=shipment, source_branch=self.branch, destination_branch=bundle.destination_branch, status="Active", cod_value=shipment.cod_value)
                        messages.success(
                            request, "Shipment included in the consolidation successfully.")
                        return JsonResponse({"status": "success"})
                    else:
                        messages.error(
                            request, "Shipment already in the Consolidation")
                        return JsonResponse({"status": "failure", "message": "Shipment already in the consolidation"})
                else:
                    messages.error(
                        request, "Shipment can not be included in this bundle.")
                    return JsonResponse({"status": "failure", "message": "Shipment can not be included in this bundle."})
            elif action == "remove-from-bundle":
                sbs_id = request.POST.get("sbs_id")
                sbs = ShipmentBundleShipment.objects.get(
                    status="Active", shipment_bundle=bundle, shipment=shipment, id=sbs_id)
                sbs.status = "Disabled"
                sbs.save()
                shipment.shipment_status = "Shipment Arrived at Warehouse"
                shipment.save()
                ShipmentBundleActivity.objects.create(
                    status="Active", shipment_bundle=bundle, activity=f"{shipment.tracking_code} has been removed from the consolidation.", activity_by=self.operator)
                ShipmentActivity.objects.create(
                    shipment=shipment, shipment_status=shipment.shipment_status, activity=f"{sbs.shipment.tracking_code} has been removed from the consolidation.", status="Active", activity_by=self.operator, involved_logistics=self.branch)
                messages.success(
                    request, "Shipment has been removed from the consolidation.")
                return JsonResponse({"status": "success"})
        except Exception as e:
            print(e)
            messages.error(request, "Shipment can not be found.")
            return JsonResponse({"status": "failure", "message": "Shipment not found."})


# delivery task managment

class LogisticsDeliveryTaskListView(LogisticsRequiredMixin, ListView):
    template_name = "logisticstemplates/shipment/logisticsdeliverytasklist.html"
    context_object_name = "deliverytasklist"
    queryset = DeliveryTask.objects.all()
    paginate_by = 100

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(
            delivery_person__logistics_branch=self.branch)
        return queryset.order_by("-id")


class LogisticsDeliveryTaskDetailView(LogisticsRequiredMixin, DetailView):
    template_name = "logisticstemplates/shipment/logisticsdeliverytaskdetail.html"
    context_object_name = "delivery_task"
    model = DeliveryTask

    def get(self, request, *args, **kwargs):
        try:
            delivery_task = DeliveryTask.objects.get(id=self.kwargs.get(
                "pk"), delivery_person__logistics_branch=self.branch)
        except Exception as e:
            print(e)
            messages.error(request, "You are not authorized.")
            return redirect("lmsapp:logisticsdeliverytasklist")
        return super().get(request, *args, **kwargs)


# payment management
class LogisticsCustomerPaymentRequestListView(LogisticsRequiredMixin, ListView):
    template_name = "logisticstemplates/payment/logisticscustomerpaymentrequestlist.html"
    context_object_name = "payment_request_list"
    paginate_by = 100

    def get_queryset(self):
        queryset = CustomerPaymentRequest.objects.filter(
            logistics_branch=self.branch)
        self.status = "all"
        if "search" in self.request.GET:
            search = self.request.GET.get("search", "")
            queryset = queryset.filter(
                Q(shipments__tracking_code=search) | Q(request_amount=search))
        elif 'status' in self.request.GET:
            status = self.request.GET.get("status")
            if status == "unsettled":
                queryset = queryset.exclude(is_settled=True)
                self.status = "unsettled"
            elif status == "settled":
                queryset = queryset.filter(is_settled=True)
                self.status = "unsettled"
        return queryset.order_by("-id")

    def get_context_data(self, **kwargs):
        all_requests = CustomerPaymentRequest.objects.filter(
            logistics_branch=self.branch)
        context = super().get_context_data(**kwargs)
        context["settled_requests"] = all_requests.filter(is_settled=True)
        context["unsettled_requests"] = all_requests.exclude(is_settled=True)
        context["all_requests"] = all_requests
        context["status"] = self.status
        unsettled_amount = all_requests.exclude(is_settled=True).aggregate(
            total=Sum("request_amount")).get("total")

        context["unsettled_amount"] = unsettled_amount if unsettled_amount else 0
        return context


class LogisticsCustomerPaymentRequestDetailView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/payment/logisticscustomerpaymentrequestdetail.html"

    def get(self, request, *args, **kwargs):
        try:
            self.payment_request = CustomerPaymentRequest.objects.get(
                logistics_branch=self.branch, id=self.kwargs.get("pk"))
        except Exception as e:
            print(e)
            messages.error(request, "Payment request not found...")
            return redirect("lmsapp:logisticscustomerpaymentrequestlist")
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        customer = self.payment_request.logistics_customer
        context["payment_request"] = self.payment_request
        context["customer_payment_form"] = LogisticsCustomerPaymentForm(
            customer=customer)
        # additional completed shipments
        current_balance = get_current_balance(
            customer, shipment_origin=self.branch)
        completed_shipments = Shipment.objects.filter(
            customer=customer, shipment_status__in=customer_completed_charge_sl, service_payment_status=False).count()
        if self.payment_request.shipments.all().count() < completed_shipments:
            context["completed_shipments"] = completed_shipments
            context["current_balance"] = current_balance
        return context

    def post(self, request, *args, **kwargs):
        try:
            payment_request = CustomerPaymentRequest.objects.get(
                logistics_branch=self.branch, id=self.kwargs.get("pk"))
            action = request.POST.get("action")
            if action == "include-shipments":
                old_amount = payment_request.request_amount
                new_amount = get_current_balance(
                    payment_request.logistics_customer, shipment_origin=self.branch)
                payment_request.request_amount = new_amount
                shipments = Shipment.objects.filter(
                    customer=payment_request.logistics_customer, shipment_status__in=customer_completed_charge_sl, service_payment_status=False)
                payment_request.remarks = f"Request amount changed from Rs. {old_amount} to {new_amount}.({payment_request.shipments.count()} to {shipments.count()} shipments) on {timezone.localtime(timezone.now())}"
                payment_request.shipments.add(*shipments)
                payment_request.save()
                messages.success(
                    request, "Newly completed shipments included in this COD request successfully.")
                return JsonResponse({"status": "success"})

            return JsonResponse({"status": "processing"})
        except Exception as e:
            print(e)
            messages.error(request, "Payment request not found...")
            return redirect("lmsapp:logisticscustomerpaymentrequestlist")
        return redirect("lmsapp:logisticscustomerpaymentrequestdetail", pk=payment_request.id)


class LogisticsCustomerPaymentRequestPayView(LogisticsRequiredMixin, View):

    def post(self, request, *args, **kwargs):
        try:
            payment_request = CustomerPaymentRequest.objects.get(
                logistics_branch=self.branch, id=self.kwargs.get("pk"))
            cpf = LogisticsCustomerPaymentForm(
                payment_request.logistics_customer, request.POST, request.FILES)
            if cpf.is_valid():
                remarks = cpf.cleaned_data.get("remarks")
                payment_method = cpf.cleaned_data.get("payment_method")
                slip = request.FILES.get("slip")
                # shipping_charge = sbs.aggregate(total=Sum('shipping_charge'))['total'] or 0
                total_shipping_charge = payment_request.shipments.all().aggregate(
                    total=Sum('shipping_charge'))['total'] or 0
                total_rejection_handling_charge = payment_request.shipments.all().aggregate(
                    total=Sum('rejection_handling_charge'))['total'] or 0
                total_cod_handling_charge = payment_request.shipments.all().aggregate(
                    total=Sum('cod_handling_charge'))['total'] or 0
                total_service_charge = total_shipping_charge + \
                    total_rejection_handling_charge + total_cod_handling_charge
                total_cod_value = payment_request.shipments.all().aggregate(
                    total=Sum('cod_value'))['total'] or 0
                cp = CustomerPayment.objects.create(
                    status="Active", payment_request=payment_request, logistics_branch=self.branch, logistics_customer=payment_request.logistics_customer, total_shipping_charge=total_shipping_charge, total_cod_handling_charge=total_cod_handling_charge, total_rejection_handling_charge=total_rejection_handling_charge, discount_or_extra=0, total_service_charge=total_service_charge, total_cod_value=total_cod_value, net_paid_or_received=payment_request.request_amount, payment_method=payment_method, remarks=remarks, payment_confirmed_by=self.operator, slip=slip)
                cp.shipments.set(payment_request.shipments.all())
                payment_request.is_settled = True
                payment_request.settled_date = timezone.localtime(
                    timezone.now())
                payment_request.save()
                cp.shipments.update(service_payment_status=True)
                return redirect("lmsapp:logisticscustomerpaymentrequestdetail", pk=payment_request.id)
            else:
                messages.error(request, "Something went wrong.")
                return redirect("lmsapp:logisticscustomerpaymentrequestdetail", pk=payment_request.id)

        except Exception as e:
            print(e)
            messages.error(request, "Payment request not found...")
            return redirect("lmsapp:logisticscustomerpaymentrequestlist")


class LogisticsCustomerShipmentReportView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/reports/logisticscustomershipmentreport.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        customer = LogisticsCustomer.objects.get(id=self.kwargs['pk'])
        total_shipments = Shipment.objects.filter(
            status="Active", shipment_origin=self.branch, customer=customer)
        pending_shipments = total_shipments.filter(
            shipment_status="Shipment Requested")
        canceled_shipments = total_shipments.filter(
            shipment_status="Shipment Canceled")
        assigned_for_pickup_shipments = total_shipments.filter(
            shipment_status="Rider Assigned for Pickup")
        at_warehouse_shipments = total_shipments.filter(shipment_status__in=[
                                                        "Rider Picked up the Shipment", "Shipment Arrived at Warehouse", "Sender at Logistics with Shipment"])
        assigned_for_dropoff_shipments = total_shipments.filter(
            shipment_status="Rider Assigned for Dispatch to the Receiver")
        dispatched_shipments = total_shipments.filter(
            shipment_status="Rider Picked up the Shipment for Dispatch")
        delivered_shipments = total_shipments.filter(
            shipment_status="Shipment Delivered to the Receiver")
        rejected_shipments = total_shipments.filter(
            shipment_status="Shipment Rejected")
        readytoreturn_shipments = total_shipments.filter(
            shipment_status="Ready to Return to Sender")
        assigned_readytoreturn_shipments = total_shipments.filter(
            shipment_status="Rider Assigned for Returning to the Sender")
        ontheway_readytoreturn_shipments = total_shipments.filter(
            shipment_status="Rider Picked up the Shipment for Return")
        returned_shipments = total_shipments.filter(
            shipment_status="Shipment Returned to the Sender")

        context = {
            'logged_in_operator': self.operator,
            'customer': customer,
            'total_shipments': total_shipments.count(),
            'pending_shipments': pending_shipments.count(),
            'canceled_shipments': canceled_shipments.count(),
            'assigned_for_pickup_shipments': assigned_for_pickup_shipments.count(),
            'at_warehouse_shipments': at_warehouse_shipments.count(),
            'assigned_for_dropoff_shipments': assigned_for_dropoff_shipments.count(),
            'dispatched_shipments': dispatched_shipments.count(),
            'delivered_shipments': delivered_shipments.count(),
            'rejected_shipments': rejected_shipments.count(),
            'readytoreturn_shipments': readytoreturn_shipments.count(),
            'assigned_readytoreturn_shipments': assigned_readytoreturn_shipments.count(),
            'ontheway_readytoreturn_shipments': ontheway_readytoreturn_shipments.count(),
            'returned_shipments': returned_shipments.count(),

        }
        return context


class LogisticsConnectedLogisticsPaymentListView(LogisticsRequiredMixin, ListView):
    template_name = "logisticstemplates/payment/logisticsconnectedlogisticspaymentlist.html"
    context_object_name = "connected_logistics"
    paginate_by = 100

    def get_queryset(self):
        self.branch_contract = LogisticsBranchContract.objects.filter(
            Q(first_party=self.branch) | Q(second_party=self.branch))
        self.connected_logistics = []
        for branch in self.branch_contract:
            if branch.first_party != self.branch or branch.second_party != self.branch:
                if branch.first_party == self.branch:
                    self.connected_logistics.append(branch.second_party)
                else:
                    self.connected_logistics.append(branch.first_party)
        self.status = "Connected Logistics List"
        self.all_lpr = LogisticsPaymentRequest.objects.filter(
            Q(status="Active", requested_to=self.branch) | Q(status="Active", requested_by=self.branch))
        self.unsettled_lpr = self.all_lpr.filter(
            is_settled=False, is_verified=False)
        self.verify_lpr = self.all_lpr.filter(
            is_settled=True, is_verified=False)
        self.settled_lpr = self.all_lpr.filter(
            is_settled=True, is_verified=True)
        if 'status' in self.request.GET:
            status = self.request.GET.get("status")
            if status == "unsettled":
                self.queryset = self.unsettled_lpr
                self.status = "Unsettled Payment Requests"
            elif status == "settled":
                self.queryset = self.settled_lpr
            elif status == "verify":
                self.queryset = self.verify_lpr
                self.status = "Settled Payment Requests"
            elif status == "all":
                self.queryset = self.all_lpr
                self.status = "All Payment Requests"
            else:
                self.queryset = self.connected_logistics
        else:
            self.queryset = self.connected_logistics

        return self.queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['connected_logistics'] = self.connected_logistics
        context['mybranch'] = self.branch
        context['payment_requests'] = self.all_lpr.count()
        context['unsettled_requests'] = self.unsettled_lpr.count()
        context['settled_requests'] = self.settled_lpr.count()
        context['verify_requests'] = self.verify_lpr.count()
        context['connected_logistics_count'] = len(self.connected_logistics)
        context['status'] = self.status
        context['prequests'] = self.queryset

        return context


class LogisticsConnectedLogisticsMakePaymentView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/payment/logisticsconnectedlogisticsmakepayment.html"

    def get(self, request, *args, **kwargs):
        self.connected_logistics = LogisticsBranch.objects.get(
            id=self.kwargs['pk'])
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        logistics = self.connected_logistics
        context['connected_logistics'] = logistics
        sbs = ShipmentBundleShipment.objects.filter(
            status="Active", destination_branch=logistics, source_branch=self.branch, service_payment_status=False)
        context['sbss'] = sbs
        shipping_charge = sbs.aggregate(
            total=Sum('shipping_charge'))['total'] or 0
        cod_charge = sbs.aggregate(total=Sum('cod_handling_charge'))[
            'total'] or 0
        rejection_charge = sbs.aggregate(
            total=Sum('rejection_handling_charge'))['total'] or 0
        cod_value = sbs.aggregate(total=Sum('shipment__cod_value'))[
            'total'] or 0
        to_pay = shipping_charge + cod_charge + rejection_charge
        net_value = cod_value - to_pay
        context['to_pay'] = to_pay
        context['cod_value'] = cod_value
        context['net_value'] = net_value

        another_sbs = ShipmentBundleShipment.objects.filter(
            status="Active", destination_branch=self.branch, source_branch=logistics, service_payment_status=False)
        context['another_sbss'] = another_sbs
        another_shipping_charge = another_sbs.aggregate(
            total=Sum('shipping_charge'))['total'] or 0
        another_cod_charge = another_sbs.aggregate(
            total=Sum('cod_handling_charge'))['total'] or 0
        another_rejection_charge = another_sbs.aggregate(
            total=Sum('rejection_handling_charge'))['total'] or 0
        another_cod_value = another_sbs.aggregate(
            total=Sum('shipment__cod_value'))['total'] or 0
        another_to_pay = another_shipping_charge + \
            another_cod_charge + another_rejection_charge
        another_net_value = another_cod_value - another_to_pay

        context['another_to_pay'] = another_to_pay
        context['another_cod_value'] = another_cod_value
        context['another_net_value'] = another_net_value

        context['final_net_value'] = net_value - another_net_value
        context['final_shipment_count'] = sbs.count() + another_sbs.count()

        return context

    def post(self, request, *args, **kwargs):
        requested_to = LogisticsBranch.objects.get(
            id=request.POST.get('requested_to_id'))
        if LogisticsPaymentRequest.objects.filter(is_settled=False, requested_to=requested_to, requested_by=self.branch, status="Active").exists() or LogisticsPaymentRequest.objects.filter(is_settled=False, requested_to=self.branch, requested_by=requested_to, status="Active").exists():
            return JsonResponse({"status": "error"})
        else:
            lpr = LogisticsPaymentRequest.objects.create(
                status="Active", requested_by=self.branch, requested_to=requested_to)
            lpr.requester_remarks = 'Please make a payment asap.'
            lpr.save()

        return redirect("lmsapp:customerpaymentrequestlist")


class LogisticsPaymentRequestCreateView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/payment/logisticspaymentrequestcreate.html"

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            branch_id = request.GET.get("requested_to_id")
            logistics = LogisticsBranch.objects.get(id=branch_id)
            sbs = ShipmentBundleShipment.objects.filter(
                destination_branch=logistics, source_branch=self.branch)
            shipping_charge = sbs.aggregate(total=Sum('shipping_charge'))
            cod_charge = sbs.aggregate(total=Sum('cod_handling_charge'))
            rejection_charge = sbs.aggregate(
                total=Sum('rejection_handling_charge'))
            cod_value = sbs.aggregate(
                total=Sum('shipment__cod_value'))['total']
            to_pay = shipping_charge['total'] + \
                cod_charge['total'] + rejection_charge['total']
            net_value = cod_value - to_pay
            return JsonResponse({"status": "success", "branch_name": f'{logistics.branch_city} ({logistics.logistics_company.company_name})', "current_balance": net_value, "shipment_count": sbs.count()})

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["payment_request_form"] = LogisticsPaymentRequestForm(
            logistics=self.branch)
        return context

    def post(self, request, *args, **kwargs):
        payment_request_form = LogisticsPaymentRequestForm(
            self.branch, request.POST)
        if payment_request_form.is_valid():
            branch = payment_request_form.cleaned_data.get("requested_to")
            print(branch, ' =+++++++++++++++')
            shipments = Shipment.objects.filter(
                shipment_origin=self.branch, shipment_status__in=customer_completed_charge_sl, service_payment_status=False)
            current_balance = logistics_get_current_balance(
                self.branch, requested_to=branch)
            if CustomerPaymentRequest.objects.filter(status="Active", logistics_customer=self.customer, logistics_branch=branch, is_settled=False).exists():
                messages.error(
                    request, "You have previous unsettled request left. Please contact your service provider or raise an issue.")
                return redirect("lmsapp:customerpaymentrequestlist")
            else:
                if current_balance > 0 and shipments.count() > 0:
                    payment_request_form.instance.logistics_customer = self.customer
                    cpr = payment_request_form.save()
                    cpr.status = "Active"
                    cpr.shipments.add(*shipments)
                    cpr.request_amount = current_balance

                    cpr.save()
                    messages.success(
                        request, "Payment Request Created Successfully.")
                else:
                    messages.error(
                        request, "You don't have balance in your wallet.")
                    return redirect("lmsapp:customerpaymentrequestlist")

        else:
            messages.error(
                request, "Ops, Something went wront, please try again later.")
            print(payment_request_form.errors)
            return redirect("lmsapp:customerpaymentrequestlist")
        return redirect("lmsapp:customerpaymentrequestlist")


class LogisticsLogisticsPaymentView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/payment/logisticslogisticspayment.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['mybranch'] = self.branch
        self.paymentrequest = LogisticsPaymentRequest.objects.get(
            id=self.kwargs['pk1'])
        if self.branch == self.paymentrequest.requested_by:
            logistics_branch = self.paymentrequest.requested_to
        else:
            logistics_branch = self.paymentrequest.requested_by
        context["logistics_branch"] = logistics_branch
        if not self.paymentrequest.is_settled:
            context['paymentform'] = LogisticsLogisticsPaymentForm
        else:
            context['already_settled'] = True

        context['paymentrequest'] = self.paymentrequest

        sbs = ShipmentBundleShipment.objects.filter(
            status="Active", destination_branch=logistics_branch, source_branch=self.branch, service_payment_status=False)
        another_sbs = ShipmentBundleShipment.objects.filter(
            status="Active", destination_branch=self.branch, source_branch=logistics_branch, service_payment_status=False)
        shipping_charge = sbs.aggregate(
            total=Sum('shipping_charge'))['total'] or 0
        cod_charge = sbs.aggregate(total=Sum('cod_handling_charge'))[
            'total'] or 0
        rejection_charge = sbs.aggregate(
            total=Sum('rejection_handling_charge'))['total'] or 0
        cod_value = sbs.aggregate(total=Sum('shipment__cod_value'))[
            'total'] or 0
        to_pay = shipping_charge + cod_charge + rejection_charge
        net_value = cod_value - to_pay

        context['sbss'] = sbs
        context['another_sbss'] = another_sbs

        context['to_pay'] = cod_charge + rejection_charge + shipping_charge
        context['cod_value'] = cod_value

        another_shipping_charge = another_sbs.aggregate(
            total=Sum('shipping_charge'))['total'] or 0
        another_cod_charge = another_sbs.aggregate(
            total=Sum('cod_handling_charge'))['total'] or 0
        another_rejection_charge = another_sbs.aggregate(
            total=Sum('rejection_handling_charge'))['total'] or 0
        another_cod_value = another_sbs.aggregate(
            total=Sum('shipment__cod_value'))['total'] or 0

        another_to_pay = another_cod_charge + \
            another_rejection_charge + another_shipping_charge
        context['another_to_pay'] = another_to_pay
        context['another_cod_value'] = another_cod_value
        another_net_value = another_cod_value - another_to_pay

        net_amount = net_value - another_net_value
        if net_amount >= 0:
            context['net_amount_to_receive'] = net_amount
        else:
            context['net_amount_to_pay'] = -net_amount

        return context

    def post(self, request, *args, **kwargs):
        paymentformremarks = request.POST.get('logisticspaymentremarks')
        try:
            lpr = LogisticsPaymentRequest.objects.get(
                id=request.POST.get('paymentrequest_id'))
            # lpr.remarks = paymentformremarks
            lpr.is_settled = True
            lpr.settled_date = timezone.localtime(timezone.now())
            lpr.payer_logistics = self.branch
            lpr.paid_or_received_amount = request.POST.get("amount_paid")
            lpr.save()
            if self.branch == lpr.requested_by:
                logistics_branch = lpr.requested_to
            else:
                logistics_branch = lpr.requested_by
            sbss = ShipmentBundleShipment.objects.filter(
                status="Active", destination_branch=logistics_branch, source_branch=self.branch, service_payment_status=False)
            shipping_charge = sbss.aggregate(
                total=Sum('shipping_charge'))['total'] or 0
            cod_charge = sbss.aggregate(total=Sum('cod_handling_charge'))[
                'total'] or 0
            rejection_charge = sbss.aggregate(
                total=Sum('rejection_handling_charge'))['total'] or 0
            cod_value = sbss.aggregate(total=Sum('shipment__cod_value'))[
                'total'] or 0
            to_pay = shipping_charge + cod_charge + rejection_charge
            net_value = cod_value - to_pay

            another_sbss = ShipmentBundleShipment.objects.filter(
                status="Active", destination_branch=self.branch, source_branch=logistics_branch, service_payment_status=False)
            another_shipping_charge = another_sbss.aggregate(
                total=Sum('shipping_charge'))['total'] or 0
            another_cod_charge = another_sbss.aggregate(
                total=Sum('cod_handling_charge'))['total'] or 0
            another_rejection_charge = another_sbss.aggregate(
                total=Sum('rejection_handling_charge'))['total'] or 0
            another_cod_value = another_sbss.aggregate(
                total=Sum('shipment__cod_value'))['total'] or 0
            another_to_pay = another_shipping_charge + \
                another_cod_charge + another_rejection_charge
            another_net_value = another_cod_value - another_to_pay

            bp = BranchPayment.objects.create(payment_request=lpr, service_user_branch=logistics_branch, service_provider_branch=self.branch,
                                              total_shipping_charge=0, total_service_charge=0, net_amount=0, paid_or_received_amount=0)
            for sbs in sbss:
                sbs.service_payment_status = True
                sbs.service_payment_amount = float(
                    sbs.cod_handling_charge + sbs.rejection_handling_charge + sbs.shipping_charge)
                sbs.save()

            bp.bundle_shipments.add(*sbss)
            bp.total_shipping_charge = shipping_charge
            bp.total_cod_handling_charge = cod_charge
            bp.total_rejection_handling_charge = rejection_charge
            bp.total_service_charge = to_pay
            bp.total_cod_value = cod_value
            bp.net_amount = net_value
            bp.paid_or_received_amount = another_net_value - net_value
            bp.status = "Active"
            bp.save()

            another_bp = BranchPayment.objects.create(payment_request=lpr, service_user_branch=self.branch, service_provider_branch=logistics_branch,
                                                      total_shipping_charge=0, total_service_charge=0, net_amount=0, paid_or_received_amount=0)
            for sbs in another_sbss:
                sbs.service_payment_status = True
                sbs.service_payment_amount = float(
                    sbs.cod_handling_charge + sbs.rejection_handling_charge + sbs.shipping_charge)
                sbs.save()

            another_bp.bundle_shipments.add(*another_sbss)
            another_bp.total_shipping_charge = another_shipping_charge
            another_bp.total_cod_handling_charge = another_cod_charge
            another_bp.total_rejection_handling_charge = another_rejection_charge
            another_bp.total_service_charge = another_to_pay
            another_bp.total_cod_value = another_cod_value
            another_bp.net_amount = another_net_value
            another_bp.paid_or_received_amount = another_net_value - net_value
            another_bp.status = "Active"
            another_bp.save()

            messages.success(
                request, "Payment Recorded Successfully")
        except Exception as e:
            messages.error(
                request, "Ops, Something went wront, please try again later.")
            print(LogisticsLogisticsPaymentView, e)
            return redirect("lmsapp:logisticsconnectedlogisticspaymentlist")
        return redirect("lmsapp:logisticsconnectedlogisticspaymentlist")


class LogisticsLogisticsPaymentVerificationView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/payment/logisticslogisticspaymentverification.html"

    def get(self, request, *args, **kwargs):
        try:
            lpr = LogisticsPaymentRequest.objects.get(id=self.kwargs['pk'])
            logistics = lpr.requested_by
            another_logistics = lpr.requested_to
            if logistics != lpr.payer_logistics and self.branch == logistics:
                print(' I will verify', self.branch)
            elif another_logistics != lpr.payer_logistics and self.branch == another_logistics:
                print(' I will verify', another_logistics)
            else:
                messages.error(
                    request, "No authorization.")
        except:
            messages.error(
                request, "No authorization.")

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        lpr = LogisticsPaymentRequest.objects.get(id=self.kwargs['pk'])
        context['lpr'] = lpr
        context['branchpayment'] = BranchPayment.objects.get(
            status="Active", payment_request=lpr, service_provider_branch=self.branch)
        context['another_branchpayment'] = BranchPayment.objects.get(
            status="Active", payment_request=lpr, service_user_branch=self.branch)
        return context

    def post(self, request, *args, **kwargs):
        lpr = LogisticsPaymentRequest.objects.get(id=self.kwargs['pk'])
        if lpr.is_verified:
            messages.error(
                request, "Payment already been verified.")
        else:
            lpr.is_verified = True
            lpr.save()
            bp = BranchPayment.objects.filter(
                status="Active", payment_request=lpr)
            bp.update(is_verified=True)
            messages.success(
                request, "Payment has been verified.")

        return redirect("lmsapp:logisticsconnectedlogisticspaymentlist")


class LogisticsPaymentReportView(LogisticsRequiredMixin, ListView):
    template_name = "logisticstemplates/reports/logisticslogisticspaymentlist.html"
    context_object_name = "paymentlist"
    paginate_by = 25

    def get_queryset(self):
        queryset = LogisticsPaymentRequest.objects.filter(
            status="Active", is_settled=True, is_verified=True)

        return queryset.order_by("-id")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        payments = LogisticsPaymentRequest.objects.filter(
            Q(requested_by=self.branch) | Q(requested_to=self.branch)).order_by('-id')
        my_payments = payments.filter(
            status="Active", is_settled=True, is_verified=True)
        context['my_payments'] = my_payments
        context['my_branch'] = self.branch
        return context


class LogisticsBranchPaymentDetailView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/reports/logisticsbranchpaymentdetail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["mybranch"] = self.branch
        lpr = LogisticsPaymentRequest.objects.get(id=self.kwargs['pk'])
        context['lpr'] = lpr
        context['my_shipments'] = BranchPayment.objects.get(
            status="Active", payment_request=lpr, service_provider_branch=self.branch)
        context['another_shipments'] = BranchPayment.objects.get(
            status="Active", payment_request=lpr, service_user_branch=self.branch)
        return context


# transaction management

class LogisticsTransactionListView(LogisticsRequiredMixin, ListView):
    template_name = "logisticstemplates/payment/logisticstransactionlist.html"
    queryset = LogisticsTransaction.objects.filter(status="Active")
    context_object_name = "transaction_list"
    paginate_by = 100

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(logistics_branch=self.branch)
        return queryset.order_by("-transaction_date")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        transactions = LogisticsTransaction.objects.filter(
            status="Active", logistics_branch=self.branch)
        total_cod_collection = transactions.filter(transaction_category="COD Collection").aggregate(
            total=Sum("transaction_amount")).get("total")
        total_cod_handover = transactions.filter(transaction_category="COD Handover").aggregate(
            total=Sum("transaction_amount")).get("total")
        total_expense = transactions.filter(transaction_category="Expense").aggregate(
            total=Sum("transaction_amount")).get("total")
        total_cash_in = transactions.filter(transaction_type="Cash In").aggregate(
            total=Sum('transaction_amount')).get("total")
        total_cash_out = transactions.filter(transaction_type="Cash Out").aggregate(
            total=Sum('transaction_amount')).get("total")
        total_cod_collection = total_cod_collection if total_cod_collection else 0
        total_cod_handover = total_cod_handover if total_cod_handover else 0
        total_expense = total_expense if total_expense else 0
        total_cash_in = total_cash_in if total_cash_in else 0
        total_cash_out = total_cash_out if total_cash_out else 0
        cash_in_hand = total_cash_in - total_cash_out
        context["total_cash_in"] = total_cash_in
        context["total_cash_out"] = total_cash_out
        context["cash_in_hand"] = cash_in_hand
        context["total_cod_collection"] = total_cod_collection
        context["total_cod_handover"] = total_cod_handover
        context["total_expense"] = total_expense
        return context


class LogisticsTransactionCreateView(LogisticsRequiredMixin, CreateView):
    template_name = "logisticstemplates/payment/logisticstransactioncreate.html"
    form_class = LogisticsTransactionCreateForm
    success_url = reverse_lazy("lmsapp:logisticstransactionlist")

    def form_valid(self, form):
        form.instance.logistics_branch = self.branch
        form.instance.status = "Active"
        messages.success(
            self.request, "Transaction record added successfully.")
        return super().form_valid(form)


class LogisticsReportView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/reports/logisticsdailyreports.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        today = timezone.localtime(timezone.now()).date()
        context['today'] = today
        
        origin_shipments = Shipment.objects.filter(shipment_origin=self.branch)
        new_shipments = origin_shipments.filter(status="Active", created_at__date=today)
        pickupassigned_shipments = DeliveryTask.objects.filter(status="Active", task_type="Pick Up", task_status="Completed Success", completed_date__date=today)
        dispatched_shipments = DeliveryTask.objects.filter(status="Active", task_type="Drop Off", task_status="Assigned", created_at__date=today)
        delivered_shipments = DeliveryTask.objects.filter(status="Active", task_type="Drop Off", task_status="Completed Success", completed_date__date=today)
        context['total_amount_to_pay'] = get_total_amount_to_pay(self.branch)
        context['new_shipments'] = new_shipments
        context['pickupassigned_shipments'] = pickupassigned_shipments
        context['dispatched_shipments'] = dispatched_shipments
        context['delivered_shipments'] = delivered_shipments
        return context


    def post(self, request, *args, **kwargs):
        from_date = request.POST.get('startingdate')
        to_date = request.POST.get('endingdate')
        print(from_date, to_date, 'here______________')

        origin_shipments = Shipment.objects.filter(shipment_origin=self.branch)
        new_shipments = origin_shipments.filter(status="Active", created_at__date__gte=from_date, created_at__date__lte=to_date)
        pickupassigned_shipments = DeliveryTask.objects.filter(status="Active", task_type="Pick Up", task_status="Completed Success", completed_date__date__gte=from_date, completed_date__date__lte=to_date)
        dispatched_shipments = DeliveryTask.objects.filter(status="Active", task_type="Drop Off", task_status="Assigned", created_at__date__gte=from_date, created_at__date__lte=to_date)
        delivered_shipments = DeliveryTask.objects.filter(status="Active", task_type="Drop Off", task_status="Completed Success", completed_date__date__gte=from_date, completed_date__date__lte=to_date)

        context = {
            'logged_in_operator' : self.operator,
            'total_amount_to_pay': get_total_amount_to_pay(self.branch), 
            'new_shipments': new_shipments,
            'pickupassigned_shipments': pickupassigned_shipments,
            'dispatched_shipments': dispatched_shipments,
            'delivered_shipments': delivered_shipments,
            'from_date': from_date,
            'to_date': to_date,

        }

        return render(request, self.template_name, context)



class LogisticsOverallAccountReportView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/reports/logisticsoverallaccountreport.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        shipments = Shipment.objects.filter(status__in=customer_delivered_sl)
        payment_completed_shipments = shipments.filter(service_payment_status=True)
        payment_incomplete_shipments = shipments.filter(service_payment_status=False)
        incomplete_shipment_amount = payment_incomplete_shipments.aggregate(total=Sum('shipping_charge')).get('total')
        print(incomplete_shipment_amount)
#Logistics Customer Wallet
class LogisticsCustomerWalletListView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/payment/logisticscustomerwalletlist.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        customers = LogisticsCustomer.objects.filter(connected_logistics=self.branch)
        context['customers'] = customers
        return context