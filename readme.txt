Panels:
- Client Website
- Customer Panel
- Branch Panel
- System Operator Panel


Apps
- accapp : account app 
- lbcapp : branch and customer linkage
- lmsapp : shipment and payment management
- siteapp : website
- restbranchapi : branch api
- restcustomerapi : customer api
- rider api 


----------------------------------- MODELS.PY Special Fields Descriptions ----------------------------------------

App 1: accapp MODELS : managing user accounts

App 2: lbcapp MODELS : managing relationships among logistics and customers

App 3: lmsapp MODELS : managing shipments and payments processes
    1. Shipment Model
        - shipment_origin: the logistics branch where customers place shipment order
        - current_logistics: the logistics branch who should handle the shipment from shipment bundle currently
        - exchanged_after_delivered: If customer places exchange order after shipment is delivered, it must be True to manage COD amount.

    2. ShipmentBundleShipment:
        - current_sbs: There may be many bundles that contains the same shipment, to filter the current shipment from the bundle, it is used

    



Permissions for Logistics Operators
class BranchPermission:
    permission_code = models.CharField(max_length=20)
    permission_title = models.CharField(max_length=200)


class BranchOperatorPermission:
    branch_operator = models.ForeignKey(BranchOperator, on_delete=models.CASCADE)
    permissions = models.ManyToManyField(Permission)

1. Check Shipment Status
2. Process Shipments
3. Process Payments
4. Process Issue Tickets
5. Manage Customers




-----------------------------Upcoming Features------------------------------------------
1. Customer wise charge 
2. Coupon Code for Customers
