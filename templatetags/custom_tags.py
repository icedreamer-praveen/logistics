from django import template
import decimal
from lbcapp.models import City, ShippingZoneDeliveryType, LogisticsCustomer
from lmsapp.models import Shipment
from accapp.constants import *
from django.db.models import Sum

register = template.Library()


@register.filter
def city_charge(n):
    lbsz = n.logisticsbranchshippingzone_set.filter(logistics_branch=1).last()
    szdt = ShippingZoneDeliveryType.objects.filter(shipping_zone=lbsz).last()
    if  szdt != None:
        charge = szdt.shipping_charge_per_kg
    else:
        charge = '---'
    return charge

@register.filter
def wallet_balance(customer):
    customer = LogisticsCustomer.objects.get(id=customer)
    shipments = Shipment.objects.filter(
        status="Active", service_payment_status=False, shipment_status__in=customer_completed_charge_sl, customer=customer)
    
    total_cod_value = shipments.filter(
        shipment_status__in=customer_completed_cod_sl,
        cod_info="Cash on Delivery").aggregate(total=Sum("cod_value")).get("total")
    total_cod_charge = shipments.filter(
        shipment_status__in=customer_completed_cod_sl,
        cod_info="Cash on Delivery").aggregate(total=Sum("cod_handling_charge")).get("total")
    total_shipping_charge = shipments.aggregate(
        total=Sum("shipping_charge")).get("total")
    total_rejection_charge = shipments.filter(
        shipment_status__in=customer_failed_charge_sl).aggregate(total=Sum("rejection_handling_charge")).get("total")

    total_cod_value = total_cod_value if total_cod_value else 0
    total_cod_charge = total_cod_charge if total_cod_charge else 0
    total_shipping_charge = total_shipping_charge if total_shipping_charge else 0
    total_rejection_charge = total_rejection_charge if total_rejection_charge else 0
    current_balance = total_cod_value - total_shipping_charge - \
        total_cod_charge - total_rejection_charge
    return current_balance