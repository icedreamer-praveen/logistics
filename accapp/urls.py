from django.urls import path
from .views import *

app_name = "accapp"
urlpatterns = [
    # customer urls ------------------------------------------
    path("customer/profile/", CustomerProfileView.as_view(), name="customerprofile"),
    path("customer/profile-update/", CustomerProfileUpdateView.as_view(),
         name="customerprofileupdate"),
    path("customer/password-change/", CustomerPasswordChangeView.as_view(),
         name="customerpasswordchange"),

    # branch urls --------------------------------------------------

    path("logistics/get-cities/", LogisticsGetCitiesView.as_view(),
         name="logisticsgetcities"),
    path("logistics/pickup-coverage/",
         LogisticsPickupCoverageView.as_view(), name="logisticspickupcoverage"),

    # operators management
    path("logistics/branch-list/",
         LogisticsBranchListView.as_view(), name="logisticsbranchlist"),
    path("logistics/branch-create/",
         LogisticsBranchCreateView.as_view(), name="logisticsbranchcreate"),
    path("logistics/operators/",
         LogisticsOperatorListView.as_view(), name="logisticsoperatorlist"),
    path("logistics/operators-create/",
         LogisticsOperatorCreateView.as_view(), name="logisticsoperatorcreate"),
    path("logistics/operators-<int:pk>-detail/",
         LogisticsOperatorDetailView.as_view(), name="logisticsoperatordetail"),
    path("logistics/operators-<int:pk>-update/",
         LogisticsOperatorUpdateView.as_view(), name="logisticsoperatorupdate"),
    path("logistics/operators-<int:pk>-password-change/",
         LogisticsOperatorPasswordChangeView.as_view(), name="logisticsoperatorpasswordchange"),

    # delivery persons
    path("logistics/delivery-persons/", LogisticsDeliveryPersonListView.as_view(),
         name="logisticsdeliverypersonlist"),
    path("logistics/delivery-persons-create/", LogisticsDeliveryPersonCreateView.as_view(),
         name="logisticsdeliverypersoncreate"),
    path("logistics/delivery-persons-<int:pk>-detail/", LogisticsDeliveryPersonDetailView.as_view(),
         name="logisticsdeliverypersondetail"),
    path("logistics/delivery-persons-<int:pk>-update/", LogisticsDeliveryPersonUpdateView.as_view(),
         name="logisticsdeliverypersonupdate"),
    path("logistics/delivery-persons-<int:pk>-password-change/", LogisticsDeliveryPersonPasswordChangeView.as_view(),
         name="logisticsdeliverypersonpasswordchange"),

    path("logistics/delivery-persons/attendances/", 
        LogisticsDeliveryPersonAttendanceView.as_view(), name="logisticsdeliverypersonattendance"),

    path("logistics/delivery-persons/checkin/",
         LogisticsDeliveryPersonCheckinView.as_view(), name="logisticsdeliverypersoncheckin"),

    # customer management
    path("logistics/customer-list/", LogisticsCustomerListView.as_view(),
         name="logisticscustomerlist"),
    path("logistics/customer-<int:pk>-detail/",
         LogisticsCustomerDetailView.as_view(), name="logisticscustomerdetail"),

]
