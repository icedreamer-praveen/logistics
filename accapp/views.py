from lbcapp.models import DeliveryType, LogisticsCustomerContract, LogisticsBranchShippingZone
from django.views.generic import View, TemplateView, ListView, CreateView
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.http import JsonResponse, HttpResponse
from lmsapp.models import CustomerNotification
from lbcapp.models import LogisticsNotice
from siteapp.models import LogisticsOrganizationalInformation
from django.shortcuts import render, redirect
from django.urls import reverse, reverse_lazy
from django.contrib import messages
from django.core import serializers
from django.utils import timezone
from .models import *
from .forms import *
import json


# mixins --------------------------------------------------------------------

class CustomerRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            try:
                self.customer = request.user.logisticscustomer
                self.branch = self.customer.creator_branch
                self.city = self.customer.city.id

            except:
                return redirect(reverse('lmsapp:customerlogin')+"?next="+request.path)
        else:
            return redirect(reverse('lmsapp:customerlogin')+"?next="+request.path)
        return super(CustomerRequiredMixin, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CustomerRequiredMixin, self).get_context_data(**kwargs)
        context["customer"] = self.customer
        context["org"] = LogisticsOrganizationalInformation.objects.filter(status="Active").first()
        context["unread_notifications"] = CustomerNotification.objects.filter(
            logistics_customer=self.customer, is_for_customer=True, is_seen=False)
        context['notice_show_on_top'] = LogisticsNotice.objects.filter(status="Active", show_on_top=True).last()
        return context


class LogisticsRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            try:
                self.operator = request.user.branchoperator
                self.branch = self.operator.logistics_branch
                self.company = self.branch.logistics_company
            except:
                return redirect(reverse('lmsapp:logisticslogin')+'?next='+request.path)
        else:
            return redirect(reverse('lmsapp:logisticslogin')+'?next='+request.path)
        return super(LogisticsRequiredMixin, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["logged_in_operator"] = self.operator
        context["org"] = LogisticsOrganizationalInformation.objects.filter(status="Active").first()
        return context

# customer views  --------------------------------------------------------------------

# customer profile


class CustomerProfileView(CustomerRequiredMixin, TemplateView):
    template_name = "customertemplates/profile/customerprofile.html"


class CustomerProfileUpdateView(CustomerRequiredMixin, TemplateView):
    template_name = "customertemplates/profile/customerprofileupdate.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["customer_form"] = CustomerProfileForm(instance=self.customer)
        return context

    def post(self, request, *args, **kwargs):
        customer_form = CustomerProfileForm(
            request.POST, request.FILES, instance=self.customer)
        if customer_form.is_valid():
            customer = customer_form.save()
            messages.success(request, "Your profile updated successfully.")
        else:
            return render(request, self.template_name, {"customer_form": customer_form, "error": "Invalid username or password"})
        return redirect("accapp:customerprofile")


class CustomerPasswordChangeView(CustomerRequiredMixin, TemplateView):
    template_name = "customertemplates/profile/customerpasswordchange.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["password_form"] = CustomerPasswordChangeForm
        return context

    def post(self, request, *args, **kwargs):
        password_form = CustomerPasswordChangeForm(request.POST)
        if password_form.is_valid():
            password = password_form.cleaned_data.get("password")
            request.user.set_password(password)
            request.user.save()
            messages.success(request, "Password changed successfully.")
        else:
            return render(request, self.template_name, {"password_form": password_form, "error": "Ops. something went wrong. "})
        return redirect("accapp:customerprofile")


# logistics branch views -------------------------------------------------------------------


class LogisticsGetCitiesView(LogisticsRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        district_id = request.GET.get("district_id")
        district = District.objects.get(id=district_id)
        cities = City.objects.filter(district=district)
        added_cities = City.objects.filter(logisticsbranchshippingzone__logistics_branch=self.branch)
        context = {
            "cities": cities,
            "district": district,
            "added_cities": added_cities
        }
        return render(request, "logisticstemplates/setting/logisticsgetcities.html", context)


class LogisticsPickupCoverageView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/profile/logisticspickupcoverage.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["coverage_cities"] = self.branch.branch_coverage.all()
        context["districts"] = District.objects.filter(status="Active")
        return context

    def post(self, request, *args, **kwargs):
        district_id = request.POST.get("district_id")
        city_id = request.POST.get("city_id")
        action = request.POST.get("action")
        if district_id:
            try:
                district = District.objects.get(id=district_id)
                cities = City.objects.filter(district=district)
                if action == "all_add":
                    self.branch.branch_coverage.add(*cities)
                    status = "success"
                    message = "All cities of this district added successfully."
                elif action == "all_remove":
                    self.branch.branch_coverage.remove(*cities)
                    status = "success"
                    message = "All cities of this district removed successfully."
                else:
                    status = "error"
                    message = "Ops! Something went wrong."
            except Exception as e:
                status = "error"
                message = "District not found."
        else:

            try:
                city = City.objects.get(id=city_id)
                if city not in self.branch.branch_coverage.all() and action == "add":
                    self.branch.branch_coverage.add(city)
                    status = "success"
                    message = "City added successfully"
                elif city in self.branch.branch_coverage.all() and action == "remove":
                    self.branch.branch_coverage.remove(city)
                    status = "success"
                    message = "City removed successfully"
                else:
                    status = "error"
                    message = "Already done this action."
            except Exception as e:
                print(e)
                status = "error"
                message = "City not found."
        resp = {
            "status": status,
            "message": message
        }
        return JsonResponse(resp)


# logistics branch management
class LogisticsBranchListView(LogisticsRequiredMixin, ListView):
    template_name = "logisticstemplates/profile/logisticsbranchlist.html"
    queryset = LogisticsBranch.objects.all()
    context_object_name = "branch_list"

    def get_queryset(self):
        queryset = super().get_queryset().filter(logistics_company=self.company)
        return queryset.order_by("id")


class LogisticsBranchCreateView(LogisticsRequiredMixin, CreateView):
    template_name = "logisticstemplates/profile/logisticsbranchcreate.html"
    form_class = LogisticsBranchCreateForm
    success_url = reverse_lazy("accapp:logisticsbranchlist")

    def get(self, request, *args, **kwargs):
        if self.branch.is_main_branch and self.operator.operator_type in ["Admin", "SuperAdmin"]:
            pass
        else:
            messages.error(
                request, "You are not authorized to perform this action.")
            return redirect('accapp:logisticsbranchlist')
        return super().get(request, *args, **kwargs)

    def form_valid(self, form):
        # branch
        if self.branch.is_main_branch and self.operator.operator_type in ["Admin", "SuperAdmin"]:
            form.instance.status = "Active"
            form.instance.logistics_company = self.company
            form.instance.is_main_branch = False
            branch = form.save()
            # user
            login_email = form.cleaned_data.get("login_email")
            login_password = form.cleaned_data.get("login_password")
            user = User.objects.create_user(
                login_email, login_email, login_password)
            # operator
            full_name = form.cleaned_data.get("full_name")
            mobile = form.cleaned_data.get("mobile")
            BranchOperator.objects.create(status="Active", logistics_branch=branch,
                                          user=user, full_name=full_name, mobile=mobile, operator_type="Admin")
            messages.success(
                self.request, "Logistics Branch Created successfully.")
            # delivery type and shipping zone
            delivery_type = DeliveryType.objects.create(
                status="Active", logistics_branch=branch, delivery_type="Standard Delivery")
            shipping_zone = LogisticsBranchShippingZone.objects.create(
                status="Active", logistics_branch=branch, shipping_zone="My Local Area", can_also_pickup=True)
            shipping_zone.cities.add(*branch.branch_coverage.all())
        else:
            messages.error(
                self.request, "You are not authorized to perform this action.")
            return redirect('accapp:logisticsbranchlist')

        return super().form_valid(form)


# logistics operator management
class LogisticsOperatorListView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/user/logisticsoperatorlist.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["logistics_operators"] = BranchOperator.objects.filter(
            logistics_branch=self.branch)
        return context


class LogisticsOperatorCreateView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/user/logisticsoperatorcreate.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["logistics_operator_form"] = LogisticsOperatorCreateForm
        return context

    def post(self, request, *args, **kwargs):
        logistics_operator_form = LogisticsOperatorCreateForm(
            request.POST, request.FILES)
        if logistics_operator_form.is_valid():
            email = logistics_operator_form.cleaned_data.get("email")
            password = logistics_operator_form.cleaned_data.get("password")
            user = User.objects.create_user(
                username=email, email=email, password=password)
            logistics_operator_form.instance.status = "Active"
            logistics_operator_form.instance.user = user
            logistics_operator_form.instance.logistics_branch = self.branch
            lo = logistics_operator_form.save()
        else:
            return render(request, self.template_name, {"logistics_operator_form": logistics_operator_form})

        return redirect("accapp:logisticsoperatorlist")


class LogisticsOperatorDetailView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/user/logisticsoperatordetail.html"

    def get(self, request, *args, **kwargs):
        try:
            lo_id = self.kwargs.get("pk")
            self.logistics_operator = BranchOperator.objects.get(
                id=lo_id, logistics_branch=self.branch)
        except Exception as e:
            messages.error(request, "Delivery person does not exist.")
            return redirect("accapp:logisticsdeliverypersonlist")
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["logistics_operator"] = self.logistics_operator
        return context


class LogisticsOperatorUpdateView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/user/logisticsoperatorupdate.html"

    def get(self, request, *args, **kwargs):
        try:
            lo_id = self.kwargs.get("pk")
            self.logistics_operator = BranchOperator.objects.get(
                id=lo_id, logistics_branch=self.branch)
        except Exception as e:
            messages.error(request, "Delivery person does not exist.")
            return redirect("accapp:logisticsdeliverypersonlist")
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["logistics_operator"] = self.logistics_operator
        context["logistics_operator_form"] = LogisticsOperatorUpdateForm(
            instance=self.logistics_operator)
        return context

    def post(self, request, *args, **kwargs):
        dp_id = self.kwargs.get("pk")
        try:
            logistics_operator = BranchOperator.objects.get(
                id=dp_id, logistics_branch=self.branch)
            logistics_operator_form = LogisticsOperatorUpdateForm(
                request.POST, request.FILES, instance=logistics_operator)
            if logistics_operator_form.is_valid():
                logistics_operator_form.save()
                messages.success(
                    request, "Information updated successfully")
            else:
                print(logistics_operator_form.errors)
                messages.error(
                    request, "Ops! Something went wrong. Please try again in a while.")
                return redirect("accapp:logisticsoperatorlist")

        except Exception as e:
            print(e)
            messages.error(request, "Logistics operator does not exist.")
            return redirect("accapp:logisticsoperatorlist")
        return redirect("accapp:logisticsoperatorlist")


class LogisticsOperatorPasswordChangeView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/user/logisticsoperatorpasswordchange.html"

    def get(self, request, *args, **kwargs):
        try:
            lo_id = self.kwargs.get("pk")
            self.logistics_operator = BranchOperator.objects.get(
                id=lo_id, logistics_branch=self.branch)
        except Exception as e:
            messages.error(request, "Logistics Operator does not exist.")
            return redirect("accapp:logisticsoperatorlist")
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["password_change_form"] = LogisticsPasswordChangeForm
        context["logistics_operator"] = self.logistics_operator
        return context

    def post(self, request, *args, **kwargs):
        try:
            dp_id = self.kwargs.get("pk")
            logistics_operator = BranchOperator.objects.get(
                id=dp_id, logistics_branch=self.branch)
            password_form = LogisticsPasswordChangeForm(request.POST)
            if password_form.is_valid():
                password = password_form.cleaned_data.get("password")
                logistics_operator.user.set_password(password)
                logistics_operator.user.save()
                messages.success(request, "Password changed successfully.")
            else:
                return render(request, self.template_name, {"logistics_operator": logistics_operator, "password_change_form": password_form, "error": "Ops. something went wrong. "})
        except Exception as e:
            messages.error(request, "Logistics Operator does not exist.")
            return redirect("accapp:logisticsoperatorlist")

        return redirect("accapp:logisticsoperatorlist")

# delivery person management


class LogisticsDeliveryPersonListView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/user/logisticsdeliverypersonlist.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["delivery_persons"] = DeliveryPerson.objects.filter(
            logistics_branch=self.branch)
        return context


class LogisticsDeliveryPersonCreateView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/user/logisticsdeliverypersoncreate.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["delivery_person_form"] = LogisticsDeliveryPersonCreateForm
        return context

    def post(self, request, *args, **kwargs):
        delivery_person_form = LogisticsDeliveryPersonCreateForm(
            request.POST, request.FILES)
        if delivery_person_form.is_valid():
            email = delivery_person_form.cleaned_data.get("email")
            password = delivery_person_form.cleaned_data.get("password")
            user = User.objects.create_user(
                username=email, email=email, password=password)
            delivery_person_form.instance.status = "Active"
            delivery_person_form.instance.user = user
            delivery_person_form.instance.logistics_branch = self.branch
            dp = delivery_person_form.save()
        else:
            return render(request, self.template_name, {"delivery_person_form": delivery_person_form})

        return redirect("accapp:logisticsdeliverypersonlist")


class LogisticsDeliveryPersonDetailView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/user/logisticsdeliverypersondetail.html"

    def get(self, request, *args, **kwargs):
        try:
            dp_id = self.kwargs.get("pk")
            self.delivery_person = DeliveryPerson.objects.get(
                id=dp_id, logistics_branch=self.branch)
        except Exception as e:
            messages.error(request, "Delivery person does not exist.")
            return redirect("lbcapp:logisticsshippingzonelist")
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        delivery_person = self.delivery_person
        context["delivery_person"] = self.delivery_person
        return context


class LogisticsDeliveryPersonUpdateView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/user/logisticsdeliverypersonupdate.html"

    def get(self, request, *args, **kwargs):
        try:
            dp_id = self.kwargs.get("pk")
            self.delivery_person = DeliveryPerson.objects.get(
                id=dp_id, logistics_branch=self.branch)
        except Exception as e:
            messages.error(request, "Delivery person does not exist.")
            return redirect("lbcapp:logisticsshippingzonelist")
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        delivery_person = self.delivery_person
        context["delivery_person_form"] = LogisticsDeliveryPersonUpdateForm(
            instance=delivery_person)
        return context

    def post(self, request, *args, **kwargs):
        dp_id = self.kwargs.get("pk")
        try:
            delivery_person = DeliveryPerson.objects.get(
                id=dp_id, logistics_branch=self.branch)
            delivery_person_form = LogisticsDeliveryPersonUpdateForm(
                request.POST, request.FILES, instance=delivery_person)
            if delivery_person_form.is_valid():
                delivery_person_form.save()
                messages.success(
                    request, "Information updated successfully")
            else:
                print(delivery_person_form.errors)
                messages.error(
                    request, "Ops! Something went wrong. Please try again in a while.")
                return redirect("accapp:logisticsdeliverypersonlist")

        except Exception as e:
            print(e)
            messages.error(request, "Delivery person does not exist.")
            return redirect("accapp:logisticsdeliverypersonlist")
        return redirect("accapp:logisticsdeliverypersonlist")


class LogisticsDeliveryPersonPasswordChangeView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/user/logisticsdeliverypersonpasswordchange.html"

    def get(self, request, *args, **kwargs):
        try:
            dp_id = self.kwargs.get("pk")
            self.delivery_person = DeliveryPerson.objects.get(
                id=dp_id, logistics_branch=self.branch)
        except Exception as e:
            messages.error(request, "Delivery person does not exist.")
            return redirect("accapp:logisticsdeliverypersonlist")
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["password_change_form"] = LogisticsPasswordChangeForm
        context["delivery_person"] = self.delivery_person
        return context

    def post(self, request, *args, **kwargs):
        try:
            dp_id = self.kwargs.get("pk")
            delivery_person = DeliveryPerson.objects.get(
                id=dp_id, logistics_branch=self.branch)
            password_form = LogisticsPasswordChangeForm(request.POST)
            if password_form.is_valid():
                password = password_form.cleaned_data.get("password")
                delivery_person.user.set_password(password)
                delivery_person.user.save()
                messages.success(request, "Password changed successfully.")
            else:
                return render(request, self.template_name, {"delivery_person": delivery_person, "password_change_form": password_form, "error": "Ops. something went wrong. "})
        except Exception as e:
            messages.error(request, "Delivery person does not exist.")
            return redirect("accapp:logisticsdeliverypersonlist")

        return redirect("accapp:logisticsdeliverypersonlist")


class LogisticsDeliveryPersonAttendanceView(LogisticsRequiredMixin, ListView):
    template_name = "logisticstemplates/profile/logisticsdeliverypersonattendance.html"
    model = DeliveryPersonCheckin
    context_object_name = "deliveryperson_attendances"

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(delivery_person__logistics_branch=self.branch)
        return queryset.order_by("-id")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["checkin_form"] = DeliveryPersonCheckinForm
        context["checkout_form"] = DeliveryPersonCheckoutForm
        return context

    def post(self, request, *args, **kwargs):
        att_pk = request.POST.get("att_pk")
        if att_pk:
            obj = DeliveryPersonCheckin.objects.get(id=att_pk)
            checkout_form = DeliveryPersonCheckoutForm(request.POST, request.FILES)
            if checkout_form.is_valid():
                obj.checkout_time = timezone.localtime(timezone.now())
                obj.checkout_km_record = checkout_form.cleaned_data.get("checkout_km_record")
                obj.checkout_km_image = checkout_form.cleaned_data.get("checkout_km_image")
                obj.remarks = checkout_form.cleaned_data.get("remarks")
                obj.save()
                
        else:
            checkin_form = DeliveryPersonCheckinForm(request.POST, request.FILES)

            if checkin_form.is_valid():
                checkin = checkin_form.save()
            else:
                messages.error(request, "Something went wrong.")
        return redirect("accapp:logisticsdeliverypersonattendance")



class LogisticsDeliveryPersonCheckinView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/profile/logisticsdpcheckin.html"

    def post(self, request, *args, **kwargs):
        title = request.POST.get("title")
        content = request.POST.get("content")
        print(title, content)
        return redirect("accapp:logisticsdeliverypersoncheckin")


# customer management

class LogisticsCustomerListView(PermissionRequiredMixin, LogisticsRequiredMixin, ListView):
    template_name = "logisticstemplates/user/logisticscustomerlist.html"
    context_object_name = "customers"
    paginate_by = 100
    permission_required = 'lmsapp.process_shipment'

    def get_queryset(self):
        queryset = LogisticsCustomer.objects.filter(
            connected_logistics=self.branch)
        if 'search' in self.request.GET:
            search = self.request.GET.get('search')
            queryset = queryset.filter(Q(full_name__icontains=search) | Q(
                company_name__icontains=search) | Q(contact_number__icontains=search))
        return queryset.order_by("-id")


class LogisticsCustomerDetailView(LogisticsRequiredMixin, TemplateView):
    template_name = "logisticstemplates/user/logisticscustomerdetail.html"

    def get(self, request, *args, **kwargs):
        try:
            self.customer = LogisticsCustomer.objects.get(
                id=self.kwargs.get("pk"), connected_logistics=self.branch)
        except Exception as e:
            messages.error(request, "Customer not found")
            return redirect("accapp:logisticscustomerlist")
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["logistics_customer"] = self.customer
        try:
            context["connected_on"] = LogisticsCustomerContract.objects.get(
                first_party=self.branch, second_party=self.customer).created_at
        except Exception as e:
            print('LogisticsCustomerDetailView', e)
        return context
