from django.db.models import Q

STATUS = (
    ("Pending", "Pending"),
    ("Active", "Active"),
    ("Disabled", "Disabled"),
)


SYSTEM_OPERATOR_TYPE = (
    ("SuperAdmin", "SuperAdmin"),  
    ("Staff", "Staff"),
)


OPERATOR_TYPE = (
    ("SuperAdmin", "SuperAdmin"),  
    ("Admin", "Admin"),
    ("Staff", "Staff"),
)
# there must be only one super admin in a logistic company

ATTEMPT_STATUS = (
    ("Success", "Success"),
    ("Failure", "Failure"),
)

SHIPMENT_STATUS = (
    # Origin Logistics  
    ("Shipment Requested", "Shipment Requested"),
    ("Shipment Canceled", "Shipment Canceled"),
    ("Rider Assigned for Pickup", "Rider Assigned for Pickup"), 
    ("Rider Picked up the Shipment", "Rider Picked up the Shipment"), 
    ("Shipment Arrived at Warehouse", "Shipment Arrived at Warehouse"), 
    ("Sender at Logistics with Shipment", "Sender at Logistics with Shipment"),
    ("Rider Assigned for Dispatch to the Receiver", "Rider Assigned for Dispatch to the Receiver"),
    ("Rider Picked up the Shipment for Dispatch", "Rider Picked up the Shipment for Dispatch"),
    ("Shipment Delivered to the Receiver", "Shipment Delivered to the Receiver"),
    ("Shipment Rejected", "Shipment Rejected"),
    ("Ready to Return to Sender", "Ready to Return to Sender"),
    ("Rider Assigned for Returning to the Sender", "Rider Assigned for Returning to the Sender"),
    ("Rider Picked up the Shipment for Return", "Rider Picked up the Shipment for Return"),
    ("Shipment Returned to the Sender", "Shipment Returned to the Sender"), 

    ("Rider Assigned for Dispatch to DC", "Rider Assigned for Dispatch to DC"),
    ("Rider Picked up the Shipment for Dispatch to DC", "Rider Picked up the Shipment for Dispatch to DC"),
    ("Rider Delivered the Shipment to Cargo for DC", "Rider Delivered the Shipment to Cargo for DC"),


    # Distribution Center
    ("Rider Assigned for Pickup in DC", "Rider Assigned for Pickup in DC"),
    ("Rider Picked up the Shipment in DC", "Rider Picked up the Shipment in DC"),
    ("Shipment Arrived at the DC", "Shipment Arrived at the DC"),
    ("Shipment Delivered to the DC", "Shipment Delivered to the DC"),
    ("Rider Assigned for Dispatch to the Receiver in DC", "Rider Assigned for Dispatch to the Receiver in DC"),
    ("Rider Picked up the Shipment for Dispatch in DC", "Rider Picked up the Shipment for Dispatch in DC"),
    ("Shipment Delivered to the Receiver in DC", "Shipment Delivered to the Receiver in DC"),
    ("Shipment Rejected in DC", "Shipment Rejected in DC"),
    ("Shipment Ready to Return by DC", "Shipment Ready to Return by DC"),
    ("Rider Assigned for Return to Logistics by DC", "Rider Assigned for Return to Logistics by DC"),
    ("Rider Picked up the Shipment to Return to Logistics by DC", "Rider Picked up the Shipment to Return to Logistics by DC"),
    ("Shipment Returned to the Logistics by DC", "Shipment Returned to the Logistics by DC"),
)

SHIPMENT_BUNDLE_SHIPMENT_STATUS = (
    ("Rider Assigned for Pickup in DC", "Rider Assigned for Pickup in DC"),
    ("Rider Picked up the Shipment in DC", "Rider Picked up the Shipment in DC"),
    ("Shipment Arrived at the DC", "Shipment Arrived at the DC"),
    ("Shipment Delivered to the DC", "Shipment Delivered to the DC"),
    ("Rider Assigned for Dispatch to the Receiver in DC", "Rider Assigned for Dispatch to the Receiver in DC"),
    ("Rider Picked up the Shipment for Dispatch in DC", "Rider Picked up the Shipment for Dispatch in DC"),
    ("Shipment Delivered to the Receiver in DC", "Shipment Delivered to the Receiver in DC"),
    ("Shipment Rejected in DC", "Shipment Rejected in DC"),
    ("Shipment Ready to Return by DC", "Shipment Ready to Return by DC"),
    ("Rider Assigned for Return to Logistics by DC", "Rider Assigned for Return to Logistics by DC"),
    ("Rider Picked up the Shipment to Return to Logistics by DC", "Rider Picked up the Shipment to Return to Logistics by DC"),
    ("Shipment Returned to the Logistics by DC", "Shipment Returned to the Logistics by DC"),
)

# Shipment Status grouping for Customer
customer_pending_sl = ["Shipment Requested", "Rider Assigned for Pickup"]
customer_processing_sl = [
    "Rider Picked up the Shipment", "Shipment Arrived at Warehouse", "Sender at Logistics with Shipment", "Rider Assigned for Dispatch to the Receiver", "Rider Assigned for Dispatch to DC"]
customer_dispatched__sl = [
    "Rider Picked up the Shipment for Dispatch", "Rider Picked up the Shipment for Dispatch to DC", "Rider Delivered the Shipment to Cargo for DC", "Rider Assigned for Pickup in DC", "Rider Picked up the Shipment in DC", "Shipment Arrived at the DC", "Shipment Delivered to the DC", "Rider Assigned for Dispatch to the Receiver in DC", "Rider Picked up the Shipment for Dispatch in DC"]
customer_just_rejected_sl = [
    "Shipment Rejected", "Shipment Rejected in DC"]
customer_returning_sl = [
    "Ready to Return to Sender", "Rider Assigned for Returning to the Sender", "Rider Picked up the Shipment for Return", "Shipment Ready to Return by DC", "Rider Assigned for Return to Logistics by DC", "Rider Picked up the Shipment to Return to Logistics by DC", "Shipment Returned to the Logistics by DC"]
customer_rejected_sl = customer_just_rejected_sl + customer_returning_sl
customer_delivered_sl = ["Shipment Delivered to the Receiver",
                         "Shipment Delivered to the Receiver in DC"]
customer_returned_sl = ["Shipment Returned to the Sender"]
customer_canceled_sl = ["Shipment Canceled"]
# for payments
# useful for cod value and cod charge calculation
customer_completed_cod_sl = customer_delivered_sl
# useful for shipping charge calculation
customer_completed_charge_sl = customer_returning_sl + \
    customer_returned_sl + customer_delivered_sl
# useful for rejection charge calculation
customer_failed_charge_sl = customer_returning_sl + customer_returned_sl
# useful for incomplete cod and charge calculation
customer_incomplete_cod_and_charge_sl = customer_processing_sl + customer_dispatched__sl

# exchange processing status list
customer_exchange_processing_sl = customer_delivered_sl + customer_rejected_sl

# end of status grouping

SHIPMENT_REMARKS_BY = Q(app_label="accapp", model="logisticsbranch") | Q(app_label="accapp", model="deliveryperson") | Q(app_label="accapp", model="logisticscustomer")

COD_INFO = (
    ("Not a Business", "Not a Business"),
    ("Already Paid", "Already Paid"),
    ("Cash on Delivery", "Cash on Delivery"),
)

WEIGHT_UNIT = (
    ("Gram", "Gram"),
    ("Kilogram", "Kilogram"),
    ("Quintal", "Quintal"),
    ("Ton", "Ton"),
)

LENGTH_UNIT = (
    ("Cm", "Cm"),
    ("Inches", "Inches"),
    ("Feet", "Feet"),
    ("Meter", "Meter"),
)

CUSTOMER_TYPE = (
    ("Individual Customer", "Individual Customer"),
    ("Business Customer", "Business Customer"),
)

TICKET_STATUS = (
    ("Open", "Open"),
    ("Reviewing", "Reviewing"),
    ("Solved", "Solved"),
)

DP_TASK_TYPE = (
    ("Pick Up", "Pick Up"),
    ("Drop Off", "Drop Off"),
    ("Return", "Return"),
    ("Bundle Pick Up", "Bundle Pick Up"),
    ("Bundle Drop Off", "Bundle Drop Off"),
    ("Bundle Return", "Bundle Return"),
)

DP_TASK_STATUS = (
    ("Canceled", "Canceled"),
    ("Assigned", "Assigned"),
    ("Started", "Started"),  # not used
    ("Doing", "Doing"),
    ("Completed Success", "Completed Success"),
    ("Completed Fail", "Completed Fail"),
)


TRANSACTION_TYPE = (
    ("Cash In", "Cash In"),
    ("Cash Out", "Cash Out"),
)

TRANSACTION_CATEGORY = (
    ("COD Collection", "COD Collection"),
    ("Expense", "Expense"),
    ("COD Handover", "COD Handover"),
    ("Loss Bearing", "Loss Bearing"),
    ("Service Charge", "Service Charge"),
    ("Others", "Others"),
)


CTICKET_FOLLOWUP_BY = Q(app_label="accapp", model="branchoperator") | Q(app_label="accapp", model="logisticscustomer")
LTICKET_FOLLOWUP_BY = Q(app_label="accapp", model="branchoperator") | Q(app_label="accapp", model="logisticscustomer")

# rejection can be done if shipments are under these status in a logistics branch.
possible_rejection_conditions = ["Shipment Arrived at Warehouse", "Sender at Logistics with Shipment", "Rider Assigned for Dispatch to the Receiver", "Rider Picked up the Shipment for Dispatch"]

PARCEL_TYPE = (
    ("Clothings", "Clothings"),
    ("Books", "Books"),
    ("Electronics", "Electronics"),
    ("Documents", "Documents"),
    ("Foods", "Foods"),
    ("Fragile", "Fragile"),
    ("Others", "Others"),
)