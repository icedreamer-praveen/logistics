# Generated by Django 3.2.7 on 2021-10-09 17:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accapp', '0002_auto_20210913_1309'),
    ]

    operations = [
        migrations.AddField(
            model_name='logisticsbranch',
            name='disable_shipment_create',
            field=models.BooleanField(blank=True, default=False, null=True),
        ),
    ]
