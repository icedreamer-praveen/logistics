from django.contrib import admin
from .models import *


class CityAdmin(admin.ModelAdmin):
    search_fields = ["name", "slug", "id"]


admin.site.register(City, CityAdmin)

admin.site.register([Country, Province, District, SystemOperator, LogisticsAccountType, LogisticsCompany, LogisticsBranch, BranchOperator, DeliveryPerson, LogisticsCustomer, SignInAttempt, DeliveryPersonCheckin])