from django import forms
from .models import *


# customer forms --------------------------------------------------------------
class CustomerProfileForm(forms.ModelForm):
    class Meta:
        model = LogisticsCustomer
        fields = ["full_name", "profile_image", "company_name", "contact_number",
                  "alternative_contact_number", "city", "address", "document1", "document2", "document3"]
        widgets = {
            "full_name": forms.TextInput(attrs={"class": "form-control"}),
            "profile_image": forms.ClearableFileInput(attrs={"class": "form-control"}),
            "company_name": forms.TextInput(attrs={"class": "form-control"}),
            "contact_number": forms.NumberInput(attrs={"class": "form-control"}),
            "alternative_contact_number": forms.NumberInput(attrs={"class": "form-control"}),
            "city": forms.Select(attrs={"class": "form-control select2"}),
            "address": forms.TextInput(attrs={"class": "form-control"}),
            "document1": forms.ClearableFileInput(attrs={"class": "form-control"}),
            "document2": forms.ClearableFileInput(attrs={"class": "form-control"}),
            "document3": forms.ClearableFileInput(attrs={"class": "form-control"}),
        }
class CustomerPasswordChangeForm(forms.Form):
    password = forms.CharField(
        widget=forms.PasswordInput(attrs={"class": "form-control"}))
    confirm_password = forms.CharField(
        widget=forms.PasswordInput(attrs={"class": "form-control"}))

    def clean_confirm_password(self):
        password = self.cleaned_data.get("password")
        confirm_password = self.cleaned_data.get("confirm_password")
        if password != confirm_password:
            raise forms.ValidationError("Passwords did not match.")
        return confirm_password


# branch forms ---------------------------------------------------------------------

class LogisticsPasswordChangeForm(forms.Form):
    password = forms.CharField(
        widget=forms.PasswordInput(attrs={"class": "form-control"}))
    confirm_password = forms.CharField(
        widget=forms.PasswordInput(attrs={"class": "form-control"}))

    def clean_confirm_password(self):
        password = self.cleaned_data.get("password")
        confirm_password = self.cleaned_data.get("confirm_password")
        if password != confirm_password:
            raise forms.ValidationError("Passwords did not match.")
        return confirm_password


class LogisticsBranchCreateForm(forms.ModelForm):
    login_email = forms.CharField(widget=forms.EmailInput(
        attrs={"class": "form-control", "pattern": "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"}))
    login_password = forms.CharField(
        widget=forms.PasswordInput(attrs={"class": "form-control"}))
    full_name = forms.CharField(widget=forms.TextInput(attrs={"class": "form-control"}))
    mobile = forms.CharField(widget=forms.NumberInput(attrs={"class": "form-control"}))

    class Meta:
        model = LogisticsBranch
        fields = ["branch_city", "branch_address", "branch_contact", "branch_email", "branch_coverage"]
        widgets = {
            "branch_city": forms.Select(attrs={"class": "form-control select2"}),
            "branch_email": forms.EmailInput(attrs={"class": "form-control"}),
            "branch_address": forms.TextInput(attrs={"class": "form-control"}),
            "branch_contact": forms.NumberInput(attrs={"class": "form-control"}),
            "branch_coverage": forms.SelectMultiple(attrs={"class": "form-control select2"}),
        }

    def clean_login_email(self):
        login_email = self.cleaned_data.get("login_email")
        if User.objects.filter(username=login_email).exists():
            raise forms.ValidationError("This email is already used, please use different email.")
        return login_email


class LogisticsDeliveryPersonCreateForm(forms.ModelForm):
    email = forms.EmailField(widget=forms.EmailInput(attrs={
                             "class": "form-control", "pattern": '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$',
                             "placeholder": "Enter email..."}))
    password = forms.CharField(widget=forms.PasswordInput(
        attrs={"class": "form-control", "placeholder": "Enter password..."}))

    class Meta:
        model = DeliveryPerson
        fields = ["email", "password", "full_name", "mobile", "image",
                  "alt_mobile", "document1", "document2", "document3"]
        widgets = {
            "full_name": forms.TextInput(attrs={"class": "form-control", "placeholder": "Full name..."}),
            "mobile": forms.NumberInput(attrs={"class": "form-control", "placeholder": "Mobile number..."}),
            "alt_mobile": forms.NumberInput(attrs={"class": "form-control", "placeholder": "Alternative mobile..."}),
            "image": forms.ClearableFileInput(attrs={"class": "form-control"}),
            "document1": forms.ClearableFileInput(attrs={"class": "form-control"}),
            "document2": forms.ClearableFileInput(attrs={"class": "form-control"}),
            "document3": forms.ClearableFileInput(attrs={"class": "form-control"}),
        }

    def clean_email(self):
        email = self.cleaned_data.get("email")
        if User.objects.filter(username=email).exists():
            raise forms.ValidationError(
                "User with this email already exists. Please use different email.")
        return email


class LogisticsDeliveryPersonUpdateForm(forms.ModelForm):

    class Meta:
        model = DeliveryPerson
        fields = ["full_name", "mobile", "image", "alt_mobile",
                  "document1", "document2", "document3"]
        widgets = {
            "full_name": forms.TextInput(attrs={"class": "form-control", "placeholder": "Full name..."}),
            "mobile": forms.NumberInput(attrs={"class": "form-control", "placeholder": "Mobile number..."}),
            "alt_mobile": forms.NumberInput(attrs={"class": "form-control", "placeholder": "Alternative mobile..."}),
            "image": forms.ClearableFileInput(attrs={"class": "form-control"}),
            "document1": forms.ClearableFileInput(attrs={"class": "form-control"}),
            "document2": forms.ClearableFileInput(attrs={"class": "form-control"}),
            "document3": forms.ClearableFileInput(attrs={"class": "form-control"}),
        }


class DeliveryPersonCheckinForm(forms.ModelForm):
    class Meta:
        model = DeliveryPersonCheckin
        fields = ["delivery_person", "checkin_km_record", "checkin_km_image"]
        widgets = {
            "delivery_person": forms.Select(attrs={"class": "form-control"}),
            "checkin_km_record": forms.NumberInput(attrs={"class": "form-control"}),
            "checkin_km_image": forms.ClearableFileInput(attrs={"class": "form-control"}),
        }


class DeliveryPersonCheckoutForm(forms.ModelForm):
    class Meta:
        model = DeliveryPersonCheckin
        fields = ["checkout_km_record", "checkout_km_image", "remarks"]
        widgets = {
            "checkout_km_record": forms.NumberInput(attrs={"class": "form-control", "required": "required"}),
            "checkout_km_image": forms.ClearableFileInput(attrs={"class": "form-control"}),
            "remarks": forms.Textarea(attrs={"class": "form-control", "rows": 3}),
        }




class LogisticsOperatorCreateForm(forms.ModelForm):
    email = forms.EmailField(widget=forms.EmailInput(attrs={
                             "class": "form-control", "pattern": '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$',
                             "placeholder": "Enter email..."}))
    password = forms.CharField(widget=forms.PasswordInput(
        attrs={"class": "form-control", "placeholder": "Enter password..."}))

    class Meta:
        model = BranchOperator
        fields = ["email", "password", "operator_type", "full_name", "image", "mobile",
                  "alt_mobile", "address_city", "document1", "document2", "document3"]
        widgets = {
            "operator_type": forms.Select(attrs={"class": "form-control"}),
            "full_name": forms.TextInput(attrs={"class": "form-control", "placeholder": "Full name..."}),
            "mobile": forms.NumberInput(attrs={"class": "form-control", "placeholder": "Mobile number..."}),
            "alt_mobile": forms.NumberInput(attrs={"class": "form-control", "placeholder": "Alternative mobile..."}),
            "address_city": forms.Select(attrs={"class": "form-control select2"}),
            "image": forms.ClearableFileInput(attrs={"class": "form-control"}),
            "document1": forms.ClearableFileInput(attrs={"class": "form-control"}),
            "document2": forms.ClearableFileInput(attrs={"class": "form-control"}),
            "document3": forms.ClearableFileInput(attrs={"class": "form-control"}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["operator_type"].choices = (
            ("Admin", "Admin"), ("Staff", "Staff"))


class LogisticsOperatorUpdateForm(forms.ModelForm):
    class Meta:
        model = BranchOperator
        fields = ["operator_type", "full_name", "image", "mobile",
                  "alt_mobile", "address_city", "document1", "document2", "document3"]
        widgets = {
            "operator_type": forms.Select(attrs={"class": "form-control"}),
            "full_name": forms.TextInput(attrs={"class": "form-control", "placeholder": "Full name..."}),
            "mobile": forms.NumberInput(attrs={"class": "form-control", "placeholder": "Mobile number..."}),
            "alt_mobile": forms.NumberInput(attrs={"class": "form-control", "placeholder": "Alternative mobile..."}),
            "address_city": forms.Select(attrs={"class": "form-control select2"}),
            "image": forms.ClearableFileInput(attrs={"class": "form-control"}),
            "document1": forms.ClearableFileInput(attrs={"class": "form-control"}),
            "document2": forms.ClearableFileInput(attrs={"class": "form-control"}),
            "document3": forms.ClearableFileInput(attrs={"class": "form-control"}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["operator_type"].choices = (
            ("Admin", "Admin"), ("Staff", "Staff"))
